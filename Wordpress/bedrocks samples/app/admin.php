<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);

 // Social Section
  $wp_customize->add_section('social', [
    'title'          => __('Social', 'extend_admin'),
    'description'    => sprintf( __('Social media urls', 'extend_admin')),
    'priority'       =>140,
  ]);

  // Facebook URL Setting
  $wp_customize->add_setting('facebook_url', [
    'default'              => _x('http://www.facebook.com', 'extend_admin'),
    'type'                 => 'theme_mod'
  ]);

  // Facebook URL Control
  $wp_customize->add_control( 'facebook_url', [
    'label'    => __('Facebook URL', 'extend_admin'),
    'section'  => 'social',
    'priority' => 1,
  ]);

  // Twitter URL Setting
  $wp_customize->add_setting('twitter_url', [
    'default'              => _x('http://www.twitter.com', 'extend_admin'),
    'type'                 => 'theme_mod'
  ]);

  // Twitter URL Control
  $wp_customize->add_control( 'twitter_url', [
    'label'    => __('Twitter URL', 'extend_admin'),
    'section'  => 'social',
    'priority' =>2,
  ]);

  // Linkedin URL Setting
  $wp_customize->add_setting('linkedin_url', [
    'default'              => _x('http://www.linkedin.com', 'extend_admin'),
    'type'                 => 'theme_mod'
  ]);

  // Linkedin URL Control
  $wp_customize->add_control( 'linkedin_url', [
    'label'    => __('LinkedIn URL', 'extend_admin'),
    'section'  => 'social',
    'priority' =>2,
  ]);

   // Utility links
  $wp_customize->add_section('Utility_links', [
    'title'          => __('Utility links', 'extend_admin'),
    'description'    => sprintf( __('Utility links', 'extend_admin')),
    'priority'       =>150,
  ]);

  // Donate URL setting
  $wp_customize->add_setting('donate_url', [
    'type'                 => 'theme_mod'
  ]);

  // Donate URL Control
  $wp_customize->add_control( 'donate_url', [
    'label'    => __('Donate', 'extend_admin'),
    'section'  => 'Utility_links',
    'priority' =>1,
  ]);

   // Featured posts for catagories and home page
  $wp_customize->add_section('Featured_posts', [
    'title'          => __('Featured posts', 'extend_admin'),
    'description'    => sprintf( __('Featured posts', 'extend_admin')),
    'priority'       =>150,
  ]);

  // Home page catagory settings setting
  $wp_customize->add_setting('home_catagory_single', [
    'type'                 => 'theme_mod'

  ]);

  $wp_customize->add_control( 'home_catagory_single', [
    'label'    => __('Home page feature catagory', 'extend_admin'),
    'section'  => 'Featured_posts',
    'priority' =>1,
  ]);

  // Research page catagory settings setting
  $wp_customize->add_setting('work_catagory', [
    'type'                 => 'theme_mod'

  ]);

  $wp_customize->add_control( 'work_catagory', [
    'label'    => __('Work page feature catagory', 'extend_admin'),
    'section'  => 'Featured_posts',
    'priority' =>2,
  ]);

  // Featured subscrbe banner for posts
  $wp_customize->add_section('subscribe', [
    'title'          => __('Subscribe post banner', 'extend_admin'),
    'description'    => sprintf( __('Subscribe banner copy', 'extend_admin')),
    'priority'       =>150,
  ]);

  $wp_customize->add_setting('subscribe_title', [
    'default'              => _x('Keep up with developments in drug science', 'extend_admin'),
    'type'                 => 'theme_mod'
  ]);

  $wp_customize->add_control( 'subscribe_title', [
    'label'    => __('Subscribe title copy', 'extend_admin'),
    'section'  => 'subscribe',
    'priority' =>1,
  ]);

  $wp_customize->add_setting('subscribe_copy', [
    'default'              => _x('Reading, engaging with, and sharing our publications, papers and commentary gives evidence-based science and policy the audience it needs and deserves.', 'extend_admin'),
    'type'                 => 'theme_mod'
  ]);

  $wp_customize->add_control( 'subscribe_copy', [
    'label'    => __('Subscribe body copy', 'extend_admin'),
    'section'  => 'subscribe',
    'priority' =>2,
  ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});
