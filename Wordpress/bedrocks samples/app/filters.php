<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/**
 * Remove 'catagory' from listings
 */
add_filter( 'get_the_archive_title', function ( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
});

/**
 * Filter the excerpt "read more" string.
 */
add_filter( 'excerpt_more', function ( $more ) {
    return '&nbsp; &hellip;';
} );



/*
* Update MCE Editro to include style select
*/
add_filter('mce_buttons_2', function ($buttons) {
array_unshift($buttons, 'styleselect');
return $buttons;
});

/*
* Callback function to filter the MCE settings

/*
* Each array child is a format with it's own settings
* Notice that each array has title, block, classes, and wrapper arguments
* Title is the label which will be visible in Formats menu
* Block defines whether it is a span, div, selector, or inline style
* Classes allows you to define CSS classes
* Wrapper whether or not to add a new block-level element around any selected elements
*/


// Callback function to filter the MCE settings

// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', function ( $init_array ) {
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
    array(
        'title' => 'Pseudo Headings',
        'items' => array(
          array(
            'title' => 'h1',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'h1'
          ),
          array(
            'title' => 'h2',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'h2'
          ),
          array(
            'title' => 'h3',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'h3'
          ),
          array(
            'title' => 'h4',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'h4'
          ),
          array(
            'title' => 'h5',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'h5'
          ),
          array(
            'title' => 'h6',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'h6'
          ),
        ),
      ),

      array(
        'title' => 'Display headings',
        'items' => array(
          array(
            'title' => 'Display 1',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'inline' => 'span',
            'classes' => 'display-1'
          ),
          array(
            'title' => 'Display 2',
            'inline' => 'span',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'display-2'
          ),
          array(
            'title' => 'Display 3',
            'inline' => 'span',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'display-3'
          ),
          array(
            'title' => 'Display 4',
            'inline' => 'span',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'display-4'
          ),
          array(
            'title' => 'Display 5',
            'inline' => 'span',
            'selector' => 'h1, h2, h3, h4, h5, h6, p, a, span',
            'classes' => 'display-5'
          ),
          array(
            'title' => 'Display link',
            'inline' => 'span',
            'selector' => 'a',
            'classes' => 'display-link'
          ),
        ),
      ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

} );



