@extends('layouts.app')
@section('content')
@include('partials.navbar')
@include('partials.page-breadcrumb')

<div id="content" class="container my-4">
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php the_post() @endphp
    @include('partials.content-'.get_post_type())
  @endwhile
  @include('partials.page-pagination')
</div>
@endsection
