{{--
  Template Name: About - Visually hidden title
--}}
@extends('layouts.app')
@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-breadcrumb')
    <div id="content">
	    @include('partials.page-title-sr-only')
	    <div  class="section container mb-4">
	      @include('partials.content-page')
	    </div>
    </div>
  @endwhile
@endsection
