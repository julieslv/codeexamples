{{--
  Template Name: Custom Template
--}}

@extends('layouts.app')
<div id="content">
@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <section class="section container mb-4 mt-5">
      @include('partials.content-page')
    </section>
    @include('partials.sidebar')
  @endwhile
</div>
@endsection
