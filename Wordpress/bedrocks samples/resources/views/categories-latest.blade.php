{{--
  Template Name: Categories Latest
--}}

@extends('layouts.app')
@section('content')
@include('partials.page-breadcrumb')
<div id="content">
  @include('partials.page-header')
  @include('partials.promoted-latest-blog')
  <section class="section container my-4">
     <div class="row no-gutters align-items-start">
      <div class="col-md-2 py-2">
        <b>Browse all categories:</b>
      </div>
      <div class="col-md-10 scroll">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item"><a class="nav-link active" id="latest-tab" data-toggle="tab" href="#latest" role="tab" aria-controls="latest" aria-selected="true">See All</a></li>
          <li class="nav-item"><a class="nav-link" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="false">News</a></li>
          <li class="nav-item"><a class="nav-link" id="comment-tab" data-toggle="tab" href="#comment" role="tab" aria-controls="comment" aria-selected="false">Comment</a> </li>
          <li class="nav-item"><a class="nav-link" id="impact-tab" data-toggle="tab" href="#impact" role="tab" aria-controls="impact" aria-selected="false">Impact</a></li>
        </ul>
      </div>
    </div>
    <div class="tab-content mt-4">
      <div class="row justify-content-end align-items-start">
            <div class="col-sm-12 col-lg-10 mb-3">
        <hr>
      </div>
      </div>
      <div class="tab-pane active" id="latest" role="tabpanel" aria-labelledby="latest-tab">
        @foreach($all_latest_posts as $latest_post)
        @php
          App::setupPost($latest_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        {!! get_the_posts_navigation($latest_post) !!}
      </div>
      <div class="tab-pane" id="news" role="tabpanel" aria-labelledby="news-tab">
        @foreach($news_posts as $news_post)
        @php
          App::setupPost($news_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        {!! get_the_posts_navigation($news_post) !!}
      </div>
      <div class="tab-pane" id="comment" role="tabpanel" aria-labelledby="comment-tab">
        @foreach($comment_posts as $comment_post)
        @php
          App::setupPost($comment_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        @include('partials.page-pagination')
      </div>
      <div class="tab-pane" id="impact" role="tabpanel" aria-labelledby="impact-tab">
        @foreach($impact_posts as $impact_post)
        @php
          App::setupPost($impact_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        @include('partials.page-pagination')
      </div>
    </section>
</div>
@endsection
