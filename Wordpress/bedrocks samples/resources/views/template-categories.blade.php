@extends('layouts.app')
@section('content')
@include('partials.page-breadcrumb')
{{-- @include('partials.page-header') --}}
{{-- TODO - add if statement to bring in or pass a variable to state what root/main category we are in --}}
{{-- @include('partials.promoted-latest') --}}

<div id="content" class="section container my-4">
   <div class="row align-items-center">
    <div class="col">
      <ul class="nav nav-tabs" role="tablist">
        @php
        // This general variable to get all the categories, but we only need the categories with the current slug/parent category of i.e. *Reaserch* and *Latest*
        $categories = get_categories();
        // This needs to be check to see if we have children, else we redefine this.
        @endphp
        @if ( $categories ) {{-- check to see we have a returned value so the page does not fail --}}
          @foreach($categories as $category) {{-- enter the loop --}}
          @php
          $dataCategory =  strtolower($category->name);
          $dataCategory =  str_replace(' ', '_', strtolower($dataCategory)); //define a lowercase no space underscore name to use programatically
          @endphp
          {{-- On each navigation tab element use the category term to to trigger each show and hide in the main *tab-content* set the attributes for the first visible tabed content as per bootstrap see https://getbootstrap.com/docs/4.3/components/navs/#tabs  --}}
          <li class="nav-item">
            <a class="nav-link @if($loop->first)active @endif" id="{{$dataCategory}}-tab" data-toggle="tab" href="#{{$dataCategory}}" role="tab" aria-controls="{{ $category->name }}" aria-selected="@if($loop->first)true @else false @endif">{{$category->name }}</a>
          </li>
          @endforeach
        @endif
        @php wp_reset_query(); @endphp
      </ul>
    </div>
  </div>
  <div class="tab-content row">
  @if ( $categories )
    @foreach($categories as $category)
    @php
      $dataCategory =  strtolower($category->name);
      $dataCategory =  str_replace(' ', '_', strtolower($dataCategory)); //define a lowercase no space underscore name to use programatically
    @endphp
    <div class="tab-pane fade col @if($loop->first)active show @endif" id="{{$dataCategory}}" role="tabpanel" aria-labelledby="{{$dataCategory}}-tab">
      <h5>{{$dataCategory}}</h5>
      {{-- TODO BUG : list is not coming trough as it does in the loop NOT WORKING--}}
      @php echo the_post() @endphp
    </div>
    @endforeach
  @endif
  </div>
  {{-- Duplicate below to show original loop working --}}
  <div class="row align-items-center">
    <div class="col">
      @if (!have_posts())
        <div class="row justify-content-end align-items-start bt-1">
          <div class="col-sm-12 col-md-9 col-lg-7">
            {{ __('Sorry, no results were found.', 'sage') }}
            {!! get_search_form(false) !!}
          </div>
        </div>
      @endif
      @while (have_posts()) @php the_post() @endphp
      @include('partials.content-'.get_post_type())
      @endwhile
    </div>
  </div>

  <div class="row align-items-center">
    <div class="col-auto">
      {{-- TODO Get navigation only brings in the before and after no pagination --}}
      {!! get_the_posts_navigation() !!}
    </div>
  </div>
</div>
@endsection
