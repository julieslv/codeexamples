{{--
  Template Name: Categories Research
--}}

@extends('layouts.app')
@section('content')
@include('partials.page-breadcrumb')
<div id="content">
  @include('partials.page-header')
  @include('partials.promoted-research')
  <section class="section container my-4">
     <div class="row no-gutters align-items-start">
      <div class="col-md-2 py-2">
        <b>Browse all categories:</b>
      </div>
      <div class="col-md-10 scroll">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item"><a class="nav-link active" id="research-tab" data-toggle="tab" href="#research" role="tab" aria-controls="research" aria-selected="true">See All</a></li>
          <li class="nav-item"><a class="nav-link" id="policy-tab" data-toggle="tab" href="#policy" role="tab" aria-controls="policy" aria-selected="false">Policy Statements</a></li>
          <li class="nav-item"><a class="nav-link" id="papers-tab" data-toggle="tab" href="#papers" role="tab" aria-controls="papers" aria-selected="false">Papers</a> </li>
          <li class="nav-item"><a class="nav-link" id="journal-tab" data-toggle="tab" href="#journal" role="tab" aria-controls="journal" aria-selected="false">Drug Journal</a></li>
          <li class="nav-item"><a class="nav-link" id="resources-tab" data-toggle="tab" href="#resources" role="tab" aria-controls="journal" aria-selected="false">Resources</a></li>
        </ul>
      </div>
    </div>
    <div class="tab-content mt-4">
      <div class="row justify-content-end align-items-start">
        <div class="col-sm-12 col-lg-10 mb-3">
          <hr>
        </div>
      </div>
      <div class="tab-pane active" id="research" role="tabpanel" aria-labelledby="research-tab">
        @foreach($all_research_posts as $research_post)
        @php
          App::setupPost($research_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        @include('partials.page-pagination')
      </div>
      <div class="tab-pane" id="policy" role="tabpanel" aria-labelledby="policy-tab">
        @foreach($policy_posts as $research_post)
        @php
          App::setupPost($research_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        @include('partials.page-pagination')
      </div>
      <div class="tab-pane" id="papers" role="tabpanel" aria-labelledby="papers-tab">
        @foreach($papers_posts as $research_post)
        @php
          App::setupPost($research_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        @include('partials.page-pagination')
      </div>
      <div class="tab-pane" id="journal" role="tabpanel" aria-labelledby="journal-tab">
        @foreach($papers_posts as $research_post)
        @php
          App::setupPost($research_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        @include('partials.page-pagination')
      </div>
      <div class="tab-pane" id="resources" role="tabpanel" aria-labelledby="resources-tab">
        @foreach($resources_posts as $research_post)
        @php
          App::setupPost($research_post);
        @endphp
        @include('partials.content-search')
        @endforeach
        @include('partials.page-pagination')
      </div>
    </section>
</div>
@endsection
