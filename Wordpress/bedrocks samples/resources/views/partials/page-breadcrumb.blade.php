<?php

//  to include in functions.php
function the_breadcrumb() {

    if (!is_front_page()) {

  // Start the breadcrumb with a link to your homepage
        echo '<ol class="breadcrumb">';
        echo '<li class="breadcrumb-item"><a href="';
        echo get_option('home');
        echo '">Home</a></li>';

  // Check if the current page is a category, an archive or a single page. If so show the category or archive name.
        if (is_category() || is_single() ){
            echo '<li class="breadcrumb-item">';
            $category = get_the_category();
            echo '<a href="'.get_category_link($category[0]->cat_ID).'"> ' . $category[0]->cat_name . '</a> </li>';
        } elseif (is_archive() || is_single()){
            if ( is_day() ) {
                printf( __( '%s', 'text_domain' ), get_the_date() );
            } elseif ( is_month() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
            } elseif ( is_year() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
            } else {
                echo '<li class="breadcrumb-item">';
                _e( 'Blog Archives', 'text_domain' );
                echo '</li>';
            }
        }

  // If the current page is a single post, show its title with the separator
        if (is_single()) {
            echo '<li class="breadcrumb-item">';
            the_title();
            echo '</li>';
        }

  // If the current page is a static page, show its title.
        if (is_page()) {
            echo '<li class="breadcrumb-item">';
            echo the_title();
            echo '</li>';
        }

  // if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
        if (is_home()){
            global $post;
            echo '<li class="breadcrumb-item">';
            $page_for_posts_id = get_option('page_for_posts');
            echo '</li>';
            if ( $page_for_posts_id ) {
                echo '<li class="breadcrumb-item">';
                $post = get_page($page_for_posts_id);
                setup_postdata($post);
                the_title();
                rewind_posts();
                echo '</li>';
            }
        }

        echo '</ol>';
    }
}
/*
* Credit: http://www.thatweblook.co.uk/blog/tutorials/tutorial-wordpress-breadcrumb-function/
*/

?>
<div class="container">
    <div class="row mb-4">
        <div class="col-12 col-md-11  offset-md-1">
            <nav aria-label="breadcrumb">
                <?php the_breadcrumb(); ?>
            </nav>
        </div>
    </div>
</div>
