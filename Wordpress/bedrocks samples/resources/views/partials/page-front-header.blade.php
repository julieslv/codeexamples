<div class="page-header page-front">
	<div class="container">
		<div class="row pt-5">
      <div class="col-12 offset-md-1 col-md-9 col-lg-8">
        <h1>{!! App::title() !!}</h1>
        @php dynamic_sidebar('sidebar-front') @endphp
       </div>
     </div>
   </div>
 </div>

