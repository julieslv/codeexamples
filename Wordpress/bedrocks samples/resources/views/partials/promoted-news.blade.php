
<section class="cards-wrap container my-4">
	<div class="row align-items-stretch pb-md-4">
		<div class="col-12 col-md-4">
			<div class="card">
			 	<span class="badge badge-pill badge-secondary mr-1 mb-1 addiction">News</span>
				<img src="@asset('images/news-1.jpg')"alt="[Get image Alt Text]">
				<div class="card-body">
					<h2 class="card-title">Lethal toxicity of new psychoactive substances.</h2>
					<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
					<hr>
					<p class="card-meta"><span class="author">Admin</span>| <span class="time">February 13, 2019</span> </p>
				</div>
				<a href="" class="card-link"><span class="sr-only">Read more</span></a>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="card">
			 	<span class="badge badge-pill badge-secondary mr-1 mb-1 addiction">News</span>
				<img src="@asset('images/news-2.jpg')"alt="[Get image Alt Text]">
				<div class="card-body">
					<h2 class="card-title">Analysis of alcohol and cannabis, what’s the harm?</h2>
					<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
					<hr>
					<p class="card-meta"><span class="author">Admin</span>| <span class="time">February 13, 2019</span> </p>
				</div>
				<a href="" class="card-link"><span class="sr-only">Read more</span></a>
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="card">
				<span class="badge badge-pill badge-secondary mr-1 mb-1 addiction">News</span>
				<img src="@asset('images/news-3.jpg')"alt="[Get image Alt Text]">
				<div class="card-body">
					<h2 class="card-title">Lethal toxicity of new psychoactive substances.</h2>
					<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
					<hr>
					<p class="card-meta"><span class="author">Admin</span> | <span class="time">February 13, 2019</span> </p>
				</div>
				<a href="" class="card-link"><span class="sr-only">Read more</span></a>
			</div>
		</div>
	</div>
</section>

