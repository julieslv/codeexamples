<!-- single listing-->
<article class="row justify-content-end align-items-start row-100" @php post_class() @endphp>
	<div class="col-sm-12 col-md-9 col-lg-7">
		<header>
			<h2 class="entry-title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
		</header>
		<div class="entry-summary">
			@php the_excerpt() @endphp
		</div>
	</div>
	<div class="col-sm-12 col-md-3 col-lg-3 entry-meta">
		@include('partials/entry-meta-tags-time')
	</div>
	<div class="col-sm-12 col-lg-10 mb-3">
		<hr>
	</div>
</article>
