@php
@endphp
<header class="navbar navbar-expand-lg fixed-top">
  <a class="sr-only" href="#content">Skip to content</a>
  <div class="container">
      @php
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        if ( has_custom_logo() ) {
                echo '<a class="navbar-brand mr-auto mr-lg-0" href="'. get_bloginfo( 'url' ) .'"><img src="'. esc_url( $logo[0] ) .'"></a>';
        } else {
                echo '<h1><a class="navbar-brand mr-auto mr-lg-0" href="'. get_bloginfo( 'url' ) .'">'. get_bloginfo( 'name' ) .'</a></h1>';
        }
      @endphp
      <button type="button" class="btn btn-toggle-search d-sm-block d-lg-none" data-toggle="collapse" href="#siteSearch" role="button" aria-expanded="false" aria-controls="siteSearch" >Search</button>

      <button class="navbar-toggler" type="button" data-toggle="offcanvas" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-box">
                <span class="navbar-toggler-icon"></span>
            </span>
      </button>

      <div class="navbar-collapse offcanvas-collapse">
        <svg version="1.1" baseProfile="tiny" class="drugScience-logo d-lg-none"xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="90px" height="32px"viewBox="0 0 90 32" xml:space="preserve"> <g> <path fill="#FFFFFF" d="M87.8,27.8c-0.5,0.5-1.1,0.8-1.9,0.8c-0.8,0-1.4-0.3-1.8-0.8c-0.4-0.6-0.6-1.3-0.6-2.2H90 c0-0.9,0-1.6-0.1-2c-0.1-0.6-0.3-1.2-0.6-1.6c-0.3-0.5-0.8-0.9-1.4-1.2c-0.6-0.3-1.2-0.5-1.8-0.5c-1.2,0-2.3,0.5-3,1.4 s-1.2,2.1-1.2,3.6c0,1.5,0.4,2.6,1.2,3.5c0.8,0.8,1.7,1.2,2.8,1.2c0.5,0,0.9,0,1.2-0.1c0.7-0.2,1.2-0.5,1.7-0.9 c0.3-0.3,0.5-0.6,0.7-1c0.2-0.4,0.3-0.8,0.4-1.1h-1.5C88.3,27.1,88.1,27.5,87.8,27.8z M84.3,22.3c0.5-0.5,1-0.8,1.7-0.8 c1,0,1.6,0.4,2,1.1c0.2,0.4,0.4,0.9,0.4,1.6h-4.9C83.6,23.5,83.8,22.8,84.3,22.3z"/> <path fill="#FFFFFF" d="M39.7,23.5c-0.4-0.3-1.1-0.6-1.9-0.8l-1.7-0.4c-1.1-0.3-1.9-0.5-2.2-0.7c-0.5-0.3-0.7-0.7-0.7-1.3 c0-0.6,0.2-1,0.6-1.5s1.1-0.6,2.1-0.6c1.2,0,2.1,0.3,2.6,1c0.3,0.4,0.4,0.9,0.5,1.5h1.5c0-1.4-0.4-2.4-1.3-3c-0.9-0.6-2-1-3.3-1 c-1.4,0-2.5,0.4-3.2,1.1c-0.8,0.7-1.1,1.6-1.1,2.8c0,1,0.4,1.8,1.2,2.3c0.4,0.3,1.2,0.6,2.3,0.8l1.6,0.4c0.9,0.2,1.6,0.5,2,0.7 c0.4,0.3,0.6,0.8,0.6,1.5c0,0.9-0.5,1.5-1.5,1.9c-0.5,0.2-1.1,0.3-1.7,0.3c-1.4,0-2.3-0.4-2.9-1.2c-0.3-0.4-0.5-1-0.5-1.7h-1.5 c0,1.4,0.4,2.4,1.3,3.2c0.9,0.8,2,1.2,3.6,1.2c1.3,0,2.4-0.3,3.4-0.9c1-0.6,1.5-1.6,1.5-3C40.9,24.9,40.5,24.1,39.7,23.5z"/> <path fill="#FFFFFF" d="M36.7,0.7h-4.9v12.5h4.9c2.1,0,3.6-0.9,4.4-2.8c0.5-1.1,0.7-2.3,0.7-3.6c0-1.7-0.4-3.1-1.2-4.2 C39.7,1.3,38.4,0.7,36.7,0.7z M40,8.4c-0.1,0.8-0.4,1.4-0.7,1.9c-0.4,0.6-0.9,1.1-1.6,1.3c-0.4,0.1-0.8,0.2-1.4,0.2h-2.9V2.2h2.9 c1.3,0,2.3,0.4,2.9,1.3s0.9,2.1,0.9,3.6C40.1,7.5,40.1,7.9,40,8.4z"/> <path fill="#FFFFFF" d="M45.3,8c0-0.6,0.2-1.2,0.6-1.7c0.4-0.5,1-0.7,1.7-0.7c0.1,0,0.2,0,0.2,0c0.1,0,0.2,0,0.2,0V3.9 c-0.2,0-0.3,0-0.3,0s-0.1,0-0.1,0c-0.6,0-1.1,0.2-1.6,0.7c-0.5,0.4-0.7,0.8-0.9,1.1V4.1h-1.4v9.1h1.5V8z"/> <path fill="#FFFFFF" d="M61.8,13.3c0.7,0,1.2-0.1,1.7-0.3c0.4-0.2,0.8-0.6,1.1-1c0,1.3-0.1,2.1-0.3,2.6c-0.4,0.8-1.1,1.2-2.2,1.2 c-0.7,0-1.3-0.2-1.6-0.5c-0.2-0.2-0.4-0.5-0.4-0.9h-1.5c0.1,0.9,0.4,1.6,1.1,2.1c0.7,0.4,1.5,0.7,2.4,0.7c1.7,0,2.9-0.6,3.5-1.9 c0.3-0.7,0.5-1.6,0.5-2.7V4.2h-1.4v1.2C64.3,5,64,4.7,63.8,4.5c-0.5-0.4-1.1-0.5-1.8-0.5c-1,0-1.9,0.4-2.6,1.3 c-0.7,0.9-1.1,2.1-1.1,3.6c0,1.6,0.4,2.8,1.1,3.5C60,13,60.9,13.3,61.8,13.3z M60.1,6.8c0.4-1,1.1-1.5,2.2-1.5 c0.7,0,1.3,0.3,1.7,0.8c0.5,0.5,0.7,1.4,0.7,2.5c0,0.7-0.1,1.4-0.4,2c-0.4,1-1.2,1.5-2.3,1.5c-0.7,0-1.2-0.3-1.6-0.8 s-0.6-1.3-0.6-2.3C59.8,8.1,59.9,7.4,60.1,6.8z"/> <rect x="51.3" y="20.5" fill="#FFFFFF" width="1.5" height="9.1"/> <path fill="#FFFFFF" d="M62.5,23.5c-0.1-0.6-0.3-1.2-0.6-1.6c-0.3-0.5-0.8-0.9-1.4-1.2c-0.6-0.3-1.2-0.5-1.8-0.5 c-1.2,0-2.3,0.5-3,1.4s-1.2,2.1-1.2,3.6c0,1.5,0.4,2.6,1.2,3.5c0.8,0.8,1.7,1.2,2.8,1.2c0.5,0,0.9,0,1.2-0.1 c0.7-0.2,1.2-0.5,1.7-0.9c0.3-0.3,0.5-0.6,0.7-1c0.2-0.4,0.3-0.8,0.4-1.1h-1.5c-0.1,0.4-0.3,0.8-0.6,1.1c-0.5,0.5-1.1,0.8-1.9,0.8 c-0.8,0-1.4-0.3-1.8-0.8c-0.4-0.6-0.6-1.3-0.6-2.2h6.5C62.6,24.6,62.6,23.9,62.5,23.5z M56.2,24.3c0-0.8,0.3-1.4,0.7-1.9 c0.5-0.5,1-0.8,1.7-0.8c1,0,1.6,0.4,2,1.1c0.2,0.4,0.4,0.9,0.4,1.6H56.2z"/> <path fill="#FFFFFF" d="M46.2,21.5c0.7,0,1.2,0.2,1.5,0.6c0.3,0.4,0.5,0.8,0.6,1.4h1.4c-0.1-1.2-0.5-2.1-1.1-2.6 c-0.6-0.5-1.4-0.7-2.4-0.7c-1.2,0-2.1,0.4-2.9,1.3c-0.8,0.9-1.1,2.1-1.1,3.7c0,1.3,0.3,2.4,1,3.3c0.7,0.9,1.6,1.3,2.8,1.3 c1,0,1.9-0.3,2.5-0.8c0.7-0.6,1.1-1.4,1.2-2.7h-1.4c-0.1,0.7-0.4,1.2-0.7,1.6c-0.4,0.4-0.9,0.6-1.5,0.6c-0.8,0-1.4-0.3-1.8-0.9 c-0.4-0.6-0.6-1.4-0.6-2.3c0-0.9,0.1-1.7,0.4-2.3C44.6,22,45.3,21.5,46.2,21.5z"/> <path fill="#FFFFFF" d="M68.8,20.2c-0.6,0-1.1,0.1-1.6,0.3c-0.5,0.2-0.9,0.6-1.3,1.2v-1.3h-1.4v9.1H66v-4.8c0-0.6,0-1.1,0.1-1.4 c0.1-0.3,0.2-0.6,0.5-0.9c0.3-0.4,0.7-0.6,1.1-0.7c0.2-0.1,0.5-0.1,0.9-0.1c0.7,0,1.2,0.3,1.4,0.9c0.2,0.3,0.2,0.8,0.2,1.3v5.8h1.5 v-5.9c0-0.9-0.1-1.6-0.4-2.1C70.9,20.7,70.1,20.2,68.8,20.2z"/> <path fill="#FFFFFF" d="M55.1,13.2h1.4V4.1H55v4.5c0,0.9-0.1,1.6-0.3,2.1c-0.4,1-1.1,1.4-2.1,1.4c-0.7,0-1.2-0.3-1.5-0.8 c-0.1-0.3-0.2-0.7-0.2-1.1V4.1h-1.5v6.2c0,0.8,0.1,1.4,0.4,1.9c0.5,0.9,1.3,1.3,2.6,1.3c0.8,0,1.5-0.2,2.1-0.7 c0.3-0.2,0.5-0.5,0.7-0.9L55.1,13.2z"/> <rect x="51.3" y="17" fill="#FFFFFF" width="1.5" height="1.7"/> <path fill="#FFFFFF" d="M77.3,21.5c0.7,0,1.2,0.2,1.5,0.6c0.3,0.4,0.5,0.8,0.6,1.4h1.4c-0.1-1.2-0.5-2.1-1.1-2.6 c-0.6-0.5-1.4-0.7-2.4-0.7c-1.2,0-2.1,0.4-2.9,1.3s-1.1,2.1-1.1,3.7c0,1.3,0.3,2.4,1,3.3c0.7,0.9,1.6,1.3,2.8,1.3 c1,0,1.9-0.3,2.5-0.8c0.7-0.6,1.1-1.4,1.2-2.7h-1.4c-0.1,0.7-0.4,1.2-0.7,1.6s-0.9,0.6-1.5,0.6c-0.8,0-1.4-0.3-1.8-0.9 c-0.4-0.6-0.6-1.4-0.6-2.3c0-0.9,0.1-1.7,0.4-2.3C75.7,22,76.4,21.5,77.3,21.5z"/> <ellipse fill="#FFFFFF" cx="2.3" cy="2.3" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="2.3" cy="9.3" rx="1.4" ry="1.4"/> <ellipse fill="#FFFFFF" cx="2.3" cy="16.2" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="9" cy="16.2" rx="1.4" ry="1.4"/> <ellipse fill="#FFFFFF" cx="15.8" cy="16.2" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="22.5" cy="16.2" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="22.5" cy="23.2" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="15.8" cy="23.2" rx="1.4" ry="1.4"/> <ellipse fill="#FFFFFF" cx="15.8" cy="29.7" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="9" cy="29.7" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="2.3" cy="29.7" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="9" cy="2.3" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="15.8" cy="2.3" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="9" cy="9.3" rx="1.8" ry="1.9"/> <ellipse fill="#FFFFFF" cx="15.8" cy="9.3" rx="1.4" ry="1.4"/> <ellipse fill="#FFFFFF" cx="22.5" cy="9.3" rx="2.3" ry="2.3"/> <ellipse fill="#FFFFFF" cx="9" cy="23.2" rx="1.8" ry="1.9"/> <ellipse fill="#FFFFFF" cx="2.3" cy="23.2" rx="1.4" ry="1.4"/> </g> </svg> @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu([
                  'theme_location' => 'primary_navigation', 'menu_class' => 'nav-primary navbar-nav mr-auto'
              ])
          !!}
        @endif

        <nav class="nav nav-utility row flex-md-nowrap justify-content-center align-items-md-end align-itmes-spread">
          <div class="col-md-auto col-sm-12 px-md-2">
              <button type="button" class="btn btn-link btn-toggle-site-signup" data-toggle="modal" data-target="#subscribeToDrugScience">Get Our Emails</button>
          </div>
          <div class="col-md-auto col-sm-12 px-md-2 d-lg-block d-sm-none">
              <button type="button" class="btn btn-toggle-search collapsed" data-toggle="collapse" href="#siteSearch" role="button" aria-expanded="false" aria-controls="siteSearch" >Search</button>
          </div>
          <div class="col-md-auto col-sm-12 px-md-2">
          <?php if(get_theme_mod('donate_url', '') != '') : ?>
              <a class="btn btn-primary" href="<?php echo get_theme_mod('donate_url',''); ?>" target="_blank">Donate</a>
          <?php endif; ?>
          </div>
        </nav>
    </div>
  </div>
</header>


<div class="site-search collapse" id="siteSearch">
  <div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-md-10">
            <div class="form-inline my-2">
              @php get_search_form() @endphp
            </div>
        </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="subscribeToDrugScience" tabindex="-1" role="dialog" aria-labelledby="subscribeToDrugScienceTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g transform="translate(-1296.000000, -3055.000000)" fill="#000000"> <path d="M1301.65685,3062.07107 L1296,3056.41421 L1297.41421,3055 L1303.07107,3060.65685 L1308.72792,3055 L1310.14214,3056.41421 L1304.48528,3062.07107 L1310.14214,3067.72792 L1308.72792,3069.14214 L1303.07107,3063.48528 L1297.41421,3069.14214 L1296,3067.72792 L1301.65685,3062.07107 Z" id="Icon"></path> </g> </g> </svg>
        </button>
      </div>
      <div class="modal-body mt-4 mb-5">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-12 col-md-5">
              <h2 class="h3">Keep up with developments in drug science</h2>
              <p>Reading, engaging with, and sharing our publications, papers and commentary gives evidence-based science and policy the audience it needs and deserves.</p>
            </div>
            <div class="col-md-6 offset-md-1 ml-auto">
               @php dynamic_sidebar('sidebar-chimpform') @endphp
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
