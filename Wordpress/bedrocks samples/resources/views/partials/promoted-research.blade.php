@php
    // Get the category defined by admin in customizer
	if(get_theme_mod('work_catagory', '') != '') :
         $namedCatagory = get_theme_mod('work_catagory','');
 	endif;

    // Get the ID of a given category
    $category_id = get_cat_ID( $namedCatagory);

    // Get the URL of this category
    $category_link = get_category_link( $category_id );

    // Query the db
	$the_query = new WP_Query( 'showposts=1&cat='. $category_id );

	global $_wp_additional_image_sizes;
	// Output width
	echo $_wp_additional_image_sizes['post_thumbnail']['width'];
	// Output height
	echo $_wp_additional_image_sizes['post_thumbnail']['height'];


@endphp


@while(have_posts())
@php the_post() @endphp
@endwhile


@php while ($the_query->have_posts()) : $the_query->the_post(); @endphp

<section class="section container mb-4">
	<div class="catagory-single my-4">
		<div class="row justify-content-start">
			<div class="col-12 col-md-11 col-lg-7 title">
			@php
				// <!-- Get tags-->
				$posttags = get_the_tags();
				$count=0;
				if ($posttags) {
				  foreach($posttags as $tag) {
				    $count++;
				    if (1 == $count) {
				    	$tag_link = get_tag_link( $tag->term_id );
						$badge_tag = "<a href='{$tag_link}' title='{$tag->name} Tag' class='badge badge-pill badge-primary mr-1 mb-1 {$tag->slug}'>";
						$badge_tag .= "{$tag->name}</a>";
				      	echo $badge_tag;
				    }
				  }
				}
			@endphp
			<h2 class="h3 mt-3"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
			<div class="display-3"> {{ __('By', 'sage') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn"> {{ get_the_author() }}</a><span>|</span><time class="updated text-uppercase my-2" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></div>
		</div>
		</div>
		<div class="row justify-content-end image">
			<div class="col-12 col-md-11">
    			@php
	    			if ( has_post_thumbnail()) :
	    				the_post_thumbnail();
	    			endif;
    			@endphp
			</div>
		</div>
	</div>
</section>
@php
wp_reset_query();
endwhile;
@endphp
