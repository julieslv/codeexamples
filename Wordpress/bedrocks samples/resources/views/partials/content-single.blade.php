<article  @php post_class() @endphp>
    @include('partials/page-breadcrumb')
    @include('partials/post-header')
    <div class="container">
        <div class="entry-content row mb-4">
            <div class="col-12 col-md-8 offset-md-2">
                @php the_content() @endphp
            </div>
        </div>
    </div>
    @include('partials/post-email')
    <footer>
        {!! wp_link_pages([
            'echo' => 0,
            'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'),
            'after' => '</p></nav>'
            ]) !!}
        </footer>
        @php comments_template('/partials/comments.blade.php') @endphp
    </article>
