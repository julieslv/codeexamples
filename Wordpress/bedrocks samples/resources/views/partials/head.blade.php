<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  @php wp_head() @endphp
  <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:400,500,600|Montserrat:300,300i,400i,500,500i,600,600i,700,700i" rel="stylesheet">
</head>
