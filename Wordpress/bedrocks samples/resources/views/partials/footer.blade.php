@php
function auto_copyright($year = 'auto'){
	if(intval($year) == 'auto'){ $year = date('Y'); }
	if(intval($year) == date('Y')){ echo intval($year); }
	if(intval($year) < date('Y')){ echo intval($year) . ' - ' . date('Y'); }
	if(intval($year) > date('Y')){ echo date('Y'); }
}
@endphp

<footer class="footer">
	<div class="container">
		<div class="row align-items-center justify-content-between py-4 border-bottom no-gutters">
			<div class="col-md-auto pr-4">
				@php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				if ( has_custom_logo() ) {
					echo '<a class="navbar-brand mr-auto mr-lg-0 mb-sm-4 mb-md-0" href="'. get_bloginfo( 'url' ) .'"><img src="'. esc_url( $logo[0] ) .'"></a>';
				} else {
					echo '<h1><a class="navbar-brand mr-auto mr-lg-0 mb-sm-4 mb-md-0" href="'. get_bloginfo( 'url' ) .'">'. get_bloginfo( 'name' ) .'</a></h1>';
				}
				@endphp
			</div>
			<div class="col-md-auto">
				<button type="button" class="btn btn-link btn-toggle-site-signup" data-toggle="modal" data-target="#subscribeToDrugScience">Get Our Emails</button>
			</div>
			<nav class="col nav-footer">
				@if (has_nav_menu('footer_navigation'))
				{!! wp_nav_menu([
					'theme_location' => 'footer_navigation',
					'menu_class' => 'nav',
					'fallback_cb' => 'false',
					'depth' => '1',
					'container' => '',
					'menu_id' => ''
					]) !!}
					@endif
				</nav>
				<nav class="nav col-md-auto nav-social">
					<?php if(get_theme_mod('facebook_url', 'http://facebook.com') != '') : ?>
					<a class="btn btn-default btn-sml" href="<?php echo get_theme_mod('facebook_url','http://facebook.com'); ?>" target="_blank"><i class="fab fa-facebook"></i>
						<span class="sr-only ">Facebook</span></a>
						<?php endif; ?>

						<?php if(get_theme_mod('twitter_url', 'http://twitter.com') != '') : ?>
						<a class="btn btn-default btn-sml" href="<?php echo get_theme_mod('twitter_url','http://twitter.com'); ?>" target="_blank"><i class="fab fa-twitter"></i>
							<span class="sr-only ">Twitter</span></a>
							<?php endif; ?>

							<?php if(get_theme_mod('linkedin_url', 'http://linkedin.com') != '') : ?>
							<a class="btn btn-default btn-sml" href="<?php echo get_theme_mod('linkedin_url','http://linkedin.com'); ?>" target="_blank"><i class="fab fa-linkedin"></i>
								<span class="sr-only ">Linkedin</span></a>
								<?php endif; ?>
							</nav>
						</div>
						<div class="row py-4">
							<div class="col-sm">
								<p class="copyright">&copy; @php auto_copyright(); @endphp Drug Science.  All rights reserved</p>
								@php dynamic_sidebar('sidebar-footer') @endphp
							</div>
						</div>
					</div>
				</footer>
