@php
    // Get the category defined by admin in customizer end the if at the end of file, we don't want to display anything if this varibale is not defined.
	if(get_theme_mod('home_catagory_single', '') != '') :
         $namedCatagory = get_theme_mod('home_catagory_single','');


    // Get the ID of a given category
    $category_id = get_cat_ID( $namedCatagory);

    // Get the URL of this category
    $category_link = get_category_link( $category_id );

    // Query the db
	$the_query = new WP_Query( 'showposts=1&cat='. $category_id );

	global $_wp_additional_image_sizes;
	// Output width
	echo $_wp_additional_image_sizes['post_thumbnail']['width'];
	// Output height
	echo $_wp_additional_image_sizes['post_thumbnail']['height'];


@endphp


@while(have_posts())
@php the_post() @endphp
@endwhile

@php while ($the_query->have_posts()) : $the_query->the_post(); @endphp

<section class="section container mb-4">
	  <div class="row justify-content-between pb-md-4">
	    <div class="col-auto">
	      <h2 class="display-2">@php echo $namedCatagory @endphp</h2>
	    </div>
	    <div class="col-auto">
	      <a href="@php echo esc_url( $category_link ); @endphp" class="display-link" title="@php $namedCatagory @endphp">View more @php echo $namedCatagory @endphp</a>
	  </div>
	</div>

	<div class="catagory-single my-4">
		<div class="row justify-content-start">
			<div class="col-12 col-md-11 col-lg-7 title">
			@php
				// <!-- Get tags-->
				$posttags = get_the_tags();
				$count=0;
				if ($posttags) {
				  foreach($posttags as $tag) {
				    $count++;
				    if (1 == $count) {
				    	$tag_link = get_tag_link( $tag->term_id );
						$badge_tag = "<a href='{$tag_link}' title='{$tag->name} Tag' class='badge badge-pill badge-primary mr-1 mb-1 {$tag->slug}'>";
						$badge_tag .= "{$tag->name}</a>";
				      	echo $badge_tag;
				    }
				  }
				}
			@endphp
			<h2 class="h3 mt-3"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
		</div>
		</div>
		<div class="row justify-content-end image">
			<div class="col-12 col-md-9 col-lg-7">
    			@php
	    			if ( has_post_thumbnail()) :
	    				the_post_thumbnail();
	    			endif;
    			@endphp
			</div>
		</div>
	</div>
</section>
@php
wp_reset_query();
endwhile;
endif;
@endphp
