<header>
	<div class="container">
		<div class="row mb-4mt-md-5">
			<div class="col-12 col-md-8 offset-md-1">
				@php
				$tags = get_tags();
				$html = '<div class="post_tags">';
				foreach ( $tags as $tag ) {
					$tag_link = get_tag_link( $tag->term_id );

					$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='badge badge-pill badge-primary mr-1 mb-1 {$tag->slug}'>";
					$html .= "{$tag->name}</a>";
				}
				$html .= '</div>';
				echo $html;
				@endphp
				<h1 class="entry-title">{{ get_the_title() }}</h1>
				<time class="updated text-uppercase my-4 display-3 d-block" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
			</div>
		</div>
	</div>
		@php
		if ( has_post_thumbnail()) :
			echo '<div class="feature-image">';
			the_post_thumbnail();
			echo '</div>';
		endif;
		@endphp
	<div class="container">
		<div class="row my-4">
			<div class="col-12 col-md-8 offset-md-2">
				<div class="byline author vcard display-2">
					{{ __('By', 'sage') }}
					<a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
						{{ get_the_author() }}
					</a>
				</div>
			</div>
		</div>
	</div>
</header>
