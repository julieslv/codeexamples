@extends('layouts.app')
@section('content')
  @include('partials.page-breadcrumb')
  <div id="content" class="container my-4">
   <div class="row no-gutters align-items-start">
    <div class="col-md-2 py-2">
      <b>See All:</b>
    </div>
    <div class="col-md-10">
      <ul class="nav nav-tabs mb-4">
       <li class="nav-item">
          <a class="nav-link active" href="/categories/research" role="tab" aria-controls="research" aria-selected="true">Research</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/categories/latest" role="tab" aria-controls="latest" aria-selected="false">Latest</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/drug-information" aria-selected="false">Drug Information</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/events" aria-selected="false">Events</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="row no-gutters">
    @if (!have_posts())
    <div class="row align-items-start">
      <div class="col-sm-12 col-md-9 col-lg-7">
        <div class="alert alert-warning">
          {{ __('Sorry, no results were found.', 'sage') }}
        </div>
      </div>
    </div>
    <div class="row justify-content-between align-items-center">
      <div class="col-12">
        <div class="site-search">
          <div class="form-inline my-2 my-lg-0">
            {!! get_search_form(false) !!}
          </div>
        </div>
      </div>
    </div>
    @endif
    @while(have_posts()) @php the_post() @endphp
      @include('partials.content-search')
    @endwhile
  </div>
  <div class="row justify-content-end align-items-start">
    <div class="col-md-12 col-lg-10">
      {{-- TODO Get navigation only brings in the before and after no pagination --}}
      {!! get_the_posts_navigation() !!}
    </div>
  </div>
</div>
@endsection
