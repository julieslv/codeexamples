{{--
  Template Name: Drug Info - Secondary
--}}
@extends('layouts.app')
@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-breadcrumb')
    <div id="content">
	    @include('partials.page-header')
	    <div class="section container my-4">
	      @include('partials.content-page')
	    </div>
	</div>
  @endwhile
@endsection
