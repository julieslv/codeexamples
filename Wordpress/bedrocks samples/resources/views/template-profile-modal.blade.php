{{--
  Template Name: Profile modal
--}}
@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-breadcrumb')
    <div id="content">
        @include('partials.page-title-sr-only')
        <!-- Modal -->
        <div class="modal fade" id="openModalContent" tabindex="-1" role="dialog" aria-labelledby="openModalContent" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <svg width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g transform="translate(-1296.000000, -3055.000000)" fill="#000000"> <path d="M1301.65685,3062.07107 L1296,3056.41421 L1297.41421,3055 L1303.07107,3060.65685 L1308.72792,3055 L1310.14214,3056.41421 L1304.48528,3062.07107 L1310.14214,3067.72792 L1308.72792,3069.14214 L1303.07107,3063.48528 L1297.41421,3069.14214 L1296,3067.72792 L1301.65685,3062.07107 Z" id="Icon"></path> </g> </g> </svg>
                </button>
              </div>
              <div class="modal-body mt-4 mb-5">
                <div class="container-fluid">
                  <div class="row justify-content-center">
                    <div class="col-12 col-lg-4 modal-bio-title">
                    </div>
                    <div class="col-12 col-lg-8 modal-bio-body">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
              </div>
            </div>
          </div>
        </div>

        <div  class="section container mb-4">
          @include('partials.content-page')
        </div>
    </div>
  @endwhile
@endsection
