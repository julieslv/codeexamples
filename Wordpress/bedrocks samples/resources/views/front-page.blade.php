@extends('layouts.app')
@section('content')
<div id="content">
@include('partials.page-front-header')
<section class="pb-4">
	@include('partials.promoted-research-blog')
</section>
<section class="section container mb-4 mt-5">
  @include('partials.content-page')
</section>
</div>
@endsection
