'use strict';
var namespace = namespace || {};

namespace.bgMatch = {
    init: function () {
        if ($('.bgMatch').length) {
            Þ.window.stack('resize', function () {
                namespace.bgMatch.on.resize();
            });
        }
    },
    on: {
        resize: function () {
            var $stripsWrap = $('.bgMatch .strips');
            var $strips = $stripsWrap.find('a');
            var stripsCount = $strips.length;
            var stripsWidth = $stripsWrap.width() / stripsCount + 'px';
            var stripsHeight = $stripsWrap.width() / stripsCount + 'px';
            $strips.each(function (eq, me) {
                //if window with> 768
                var $me = $(me);
                if ($(window).width() > 768) {
                    $me.width(stripsWidth);
                    // don't chain - need the width to be set before offset is calculated
                    $me.find('.color').css({
                        'background-position': sprintf('-%spx 0', $me.offset().left)
                    }).width(stripsWidth);

                } else {
                    var childPos = $(me).offset().top;
                    var parentPos = $('.bgMatch').offset().top;
                    var childOffset = parentPos - childPos;


                    $me.removeAttr('style').find('.color').removeAttr('style').css(
                    {
                        'background-position': sprintf('0 %spx', childOffset)
                    });


                }
            });
        }
    }
};



namespace.init = function () {
    this.bgMatch.init();
};


$(document).ready(function (e) {
    namespace.init();
});




