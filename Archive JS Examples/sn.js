﻿S//'use strict';
var stolt = stolt || {};



$.isIE8 = document.all && !document.addEventListener; //nifty bit of code to detect for IE8 stolen from Browserhacks.com
$.isSafari = /constructor/i.test(window.HTMLElement);
//Check for smart devices use [if(isSmartDevice(){//do something}]
function isSmartDevice() {
    var ua = navigator.userAgent || navigator.vendor || window.opera;
    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge|maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i).test(ua) || (/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i).test(ua.substr(0, 4));
}

stolt.init = function () {
    this.checkRunOnce.init();
    this.navigatorUserAgent.init();
    this.navMenu.init();
    this.cookie.init();
    this.spotNav.init();
    this.goTop.init();
    this.knob.init();
    this.selectric.init();
    this.maps.init(); // Written by Lead developer Francois DuPlessis
    this.searchInput.searchActive();
    this.searchInput.searchReveal();
    this.bxslider.init();
    this.offcanvas.init();
    this.selectchoice.init();
    this.contacts.init();
    this.bodAccordion.init();
    this.moveHeroCopy.init();
    this.scrollToID.init();
    this.feed.init();
    this.scrollToID.show();
    this.menuTouchExpand.init();
    this.sitemapmasonary.init();
    this.clearWidow.init();
    this.responsiveTableTitleAtr.init();
    this.stats.init();
    this.tabs.init();
    this.manipulate.init();
    this.businessFilter.init();
    Þ.window.stack('breakpoint', { minWidth: 480 }, function () {
        if (Þ.browser.is.IE9()) {
            stolt.equalheight.init('.eh');
        }

    });

    if (!$.isIE8) {
        //alert('this is not IE8');
        this.countup.init();
    }
};
stolt.businessFilter = {
    init: function () {
        if ($('#businessChooser').length > 0) {
            $('#businessChooser').change(function () {
                window.location = $('#businessChooser').val();
            });
        }
    }
},
    stolt.selectric = {
        init: function () {
            if ($('select').length > 0 && (!$('select').hasClass('non-selectric'))) {
                $('select').selectric();
            }
        }
    },
    stolt.tabs = {
        init: function () {
            $("#tabs").tabs({
                beforeLoad: function (event, ui) {
                    ui.jqXHR.fail(function () {
                        ui.panel.html(
                            "Couldn't load this tab. We'll try to fix this as soon as possible.");
                    });
                }
            });
        }
    },
    stolt.manipulate = {
        init: function () {
            Þ.window.stack('breakpoint', { maxWidth: 768 }, function () {
                $('.figure-glance hr').remove();
                $('.biz-snip .tile:nth-child(2n)').after('<hr class="colpad_12"/>');
            });

            Þ.window.stack('breakpoint', { minWidth: 768 }, function () {
                //console.log('min-width 768');
                $('.map-overlay-controls li.mod ').addClass('active').removeClass('mod');
                $('.figure-glance hr').remove();
                if ($('.biz-snip .tile').length > 4) {
                    $('.biz-snip').after('<hr class="colpad_12"/>');
                    $('.biz-snip .tile:nth-child(4n)').after('<hr class="colpad_12"/>');
                } else if ($('.biz-snip .tile').length < 4) {
                    $('.biz-snip').after('<hr class="colpad_12"/>');
                }
            });
        }
    },

    stolt.navigatorUserAgent = {
        init: function () {
            //console.log('checking navigator user agent.');
            $('html').addClass(Þ.browser.is.IE() && Þ.browser.version() > 8 ? 'ltIE8' : '');
            $('html').addClass(Þ.browser.is.IE() && Þ.browser.version() < 10 ? 'IE9' : '');
        }
    },
    stolt.checkRunOnce = {
        runOnce: false,
        init: function () {
            Þ.window.stack('resize', function () {
                stolt.checkRunOnce.runOnce = true;
                //console.log('run once = ' + stolt.checkRunOnce.runOnce);
            });
        }
    };
stolt.knob = {
    init: function () {
        $(".knob").knob();
    }
};

stolt.goTop = {
    init: function () {
        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
    }
};
stolt.scrollToID = {
    init: function () {
        //for each list item get the id and scroll to it's corresponding element
        $('.mdl-fleetlist a').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            if ($($(this).attr('href')).length > 0) {
                $('body, html').scrollTo($($(this).attr('href')), 0.5e3, {
                    offset: -50
                });
            }
        });
        $('.scrollToFleetNav').click(function (e) {
            e.preventDefault();
            if ($($(this).attr('href')).length > 0) {
                $('body, html').scrollTo($($('#fleetNav')), 0.5e3);
            }
        });

    },
    show: function () {
        $('.mdl-fleet-detail h3').waypoint(function (direction) {
            if (direction === 'down')
                $(this).find('.scrollToFleetNav').addClass('active');
            else {
                $(this).find('.scrollToFleetNav').removeClass('active');
            }
        }, {
            offset: '25%'
        });
    }

};
stolt.barchart = {
    init: function () {
        //get the full width of a bar
        $('.barchart').each(function (eqChart, meChart) {
            var maxBarWidth = $(meChart).width(),
                maxValue = $(meChart).find('.bar .fig').sort(
                    function (a, b) {
                        return ($(b).data('fig')) < ($(a).data('fig')) ? 1 : -1;
                    }).last().data('fig');
            $(meChart).find('.bar').each(function (eq, me) {
                var ratio = ($(me).find('.fig').data('fig') / maxValue),
                    newWidth = (maxBarWidth - 135) * ratio + 135 - 30,
                    newWidth = Math.round(newWidth);
                $(me).width(newWidth).css({
                    'backgroundColor': barchart.makeBgHex(ratio)
                });
            });
        });
    },
    makeBgHex: function (ratio) {
        if ($('.barchart').parents().hasClass('mod-biz')) {
            return ratio >= 0.75 ? '#ea5b26' :
                (ratio < 0.95 && ratio >= 0.85) ? '#eb6b3d' :
                    (ratio < 0.85 && ratio >= 0.75) ? '#eb7d54' :
                        (ratio < 0.75 && ratio >= 0.65) ? '#eb8e6c' :
                            (ratio < 0.65 && ratio >= 0.55) ? '#eb9f83' :
                                ratio < 0.55 ? '#ebb09b' : '#ea5b26';
        } else {

            return ratio >= 0.75 ? '#041c41' :
                (ratio < 0.95 && ratio >= 0.85) ? '#10294a' :
                    (ratio < 0.85 && ratio >= 0.75) ? '#1d3254' :
                        (ratio < 0.75 && ratio >= 0.65) ? '#364967' :
                            (ratio < 0.65 && ratio >= 0.55) ? '#68778d' :
                                ratio < 0.55 ? '#9ba4b3' : '#041c41';
        }
    }
};
stolt.selectchoice = {
    init: function () {
        if ($('.selectFastfact').length > 0) {
            this.selectinput($('.selectFastfact'), $('.mdl-select'));
        } else if ($('.selectContact').length > 0) {
            this.selectinput($('.selectContact'), $('.vcard'));

        }
    },
    selectinput: function (selectEl, revealEl) {
        selectEl.on('change', function (e) {
            revealEl.hide(0, function () {
                $('.' + selectEl.val()).show(0);
            });
        });
        selectEl.trigger('change');
        //selectchoice.appendMap(selectEL); // must be jQuery obj
    },
    termimalLocale: function (data) {
        if ($('.mdl-termimal-locale').length > 0 && typeof data !== 'undefined') {
            var $selectRegion = $('.selectRegion');
            var $selectFacilityWrap = $('.selectFacilityWrap');

            $selectRegion.prepend('<option>Please select a region</option>');

            for (var region in data.regions) {
                $selectRegion.append('<option value="' + data.regions[region].websafeId + '">' + data.regions[region].name + '</option>');
                var $selectFacility = $('<select class="selectFacility ' + data.regions[region].websafeId + ' " ></select>');

                $selectFacility.prepend('<option>Please select a facility</option>');

                for (var terminal in data.regions[region].terminals) {
                    $selectFacility.append('<option value="' + data.regions[region].terminals[terminal].url + '">' + data.regions[region].terminals[terminal].name + '</option>');
                }

                $selectFacility.appendTo($selectFacilityWrap).change(function () {
                    window.location.href = $(this).val();
                });
            }

            $selectRegion.on('change', function (event) {
                //console.log('yey ', event);
                if ($('.selectFacilityWrap').children('.selectricWrapper').length > 0) {
                    $('.selectric-selectFacility').hide();
                    $('.selectFacilityWrap .selectFacility').hide(0, function () {
                        $('.selectric-' + $selectRegion.val()).show(0);
                    });
                } else {
                    $('.selectFacilityWrap .selectFacility').hide(0, function () {
                        $('.' + $selectRegion.val()).show(0);
                    });
                }
            });


        }
    },
    appendMap: function ($srcObj) // $srcObj is the LI that has the long and lat data attrs (in jquery each loop $(me))
    {
        if (typeof $srcObj !== 'undefined' && $srcObj.data('latitude') && $srcObj.data('longitude')) {
            // do google maps stuff here
            var zoomLevel = 15; // or add another data attr
            //var bubbleContent = "<strong>blah</strong>Blah blah";
            var geoLong = $srcObj.data('longitude');
            var geoLat = $srcObj.data('latitude');
            var mapWrapper = $('<div class="google-maps-instance"></div>').appendTo($srcObj);
        }
    }
};
stolt.contacts = {
    init: function () {
        if ($('select#contactSelector').length > 0) {
            this.loadData($('select#contactSelector').val());
            $('select#contactSelector').on('change', function (e) {
                stolt.contacts.loadData(this.value);
            });
        }
    },
    loadData: function(value) {
        $.get('/umbraco/Surface/ContactUs/ShowContactItems?parentId=' + value, function (data) {
            stolt.contacts.setData(data);
        });
    },
    setData: function (data) {
        $("div#contacts").html(data);
    }
};
// Add currentZoomLevel to global scope
var currentZoomLevel;
stolt.maps = {
    debug: false,
    markerInstances: [],
    overlayInstances: {},
    preInit: function () {
        this.imageLayer = function (bounds, image, map, visible) {
            // Initialize all properties.
            this.bounds_ = bounds;
            this.image_ = image;
            this.map_ = map;
            this.visible_ = visible;
            // Define a property to hold the image's div. We'll
            // actually create this div upon receipt of the onAdd()
            // method so we'll leave it null for now.
            this.div_ = null;
            // Explicitly call setMap on this overlay.
            this.setMap(map);
        };
        this.imageLayer.prototype = new google.maps.OverlayView();
        this.imageLayer.prototype.draw = function () {
            // We use the south-west and north-east
            // coordinates of the overlay to peg it to the correct position and size.
            // To do this, we need to retrieve the projection from the overlay.
            var overlayProjection = this.getProjection();
            // Retrieve the south-west and north-east coordinates of this overlay
            // in LatLngs and convert them to pixel coordinates.
            // We'll use these coordinates to resize the div.
            var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
            var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());
            // Resize the image's div to fit the indicated dimensions.
            var div = this.div_;

            var newZoomLevel = stolt.maps.stoltmap.getZoom();
            newWidth = ne.x - sw.x,
                newHeight = sw.y - ne.y;
            currentWidth = parseInt(div.style.width, 10);

            if (newZoomLevel < currentZoomLevel) {
                newWidth = (newWidth < 0) ? currentWidth / 2 : newWidth;
            }
            else if (newZoomLevel > currentZoomLevel) {
                newWidth = (newWidth < 0) ? currentWidth * 2 : newWidth;
            }

            currentZoomLevel = newZoomLevel;

            div.style.left = sw.x + 'px';
            div.style.top = ne.y + 'px';
            div.style.width = newWidth + 'px';
            div.style.height = newHeight + 'px';
        };
        this.imageLayer.prototype.onAdd = function () {
            var div = document.createElement('div'),
                thisName = this.name,
                thisClassName = 'map-overlay overlay-' + thisName;

            div.className = thisClassName;

            div.style.visibility = this.visible_ ? 'visible' : 'hidden';
            // Create the img element and attach it to the div.
            var img = document.createElement('img');
            img.src = this.image_;
            div.appendChild(img);
            this.div_ = div;
            // Add the element to the "overlayLayer" pane.
            var panes = this.getPanes();
            panes.overlayLayer.appendChild(div);
        };
        this.imageLayer.prototype.show = function () {
            if (this.div_) {
                this.div_.style.visibility = 'visible';
            }
        };
        this.imageLayer.prototype.onRemove = function () {
            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        };
        this.imageLayer.prototype.hide = function () {
            if (this.div_) {
                // The visibility property must be a string enclosed in quotes.
                this.div_.style.visibility = 'hidden';
            }
        };
        this.imageLayer.prototype.toggle = function () {
            if (this.div_) {
                if (this.div_.style.visibility === 'hidden') {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };
        this.imageLayer.prototype.setVisible = function (boolean) {
            if (this.div_) {
                if (boolean) {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };
        if (this.debug) {
            console.info('maps preInit done');
        }
    },

    init: function () {
        if ($('#stolt-network').length > 0) {
            // extend default Map functionality
            stolt.maps.preInit();

            stolt.maps.stoltmap = new google.maps.Map($('#stolt-network').get(0), { //.get(0) returns the native DOM object
                center: {
                    lat: 0,
                    lng: 0
                },
                scrollwheel: false,
                zoom: 2,
                disableDefaultUI: true,
                zoomControl: true,
                styles: [{ "featureType": "all", "elementType": "all", "stylers": [{ "hue": "#ff0000" }] }, { "featureType": "all", "elementType": "geometry", "stylers": [{ "visibility": "on" }] }, { "featureType": "all", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }, { "hue": "#ff0000" }] }, { "featureType": "administrative", "elementType": "all", "stylers": [{ "visibility": "off" }, { "lightness": 33 }] }, { "featureType": "administrative", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.country", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.country", "elementType": "geometry.fill", "stylers": [{ "color": "#999999" }, { "visibility": "on" }] }, { "featureType": "administrative.country", "elementType": "geometry.stroke", "stylers": [{ "visibility": "on" }, { "color": "#f2f2f2" }] }, { "featureType": "administrative.country", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.province", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.locality", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.neighborhood", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.land_parcel", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#dcddde" }, { "visibility": "on" }] }, { "featureType": "landscape", "elementType": "geometry.fill", "stylers": [{ "color": "#999999" }] }, { "featureType": "landscape", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.man_made", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.attraction", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.park", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#c5dac6" }] }, { "featureType": "poi.park", "elementType": "labels", "stylers": [{ "visibility": "off" }, { "lightness": 20 }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "lightness": 20 }, { "visibility": "off" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "geometry", "stylers": [{ "color": "#c5c6c6" }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#e4d7c6" }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#fbfaf7" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "visibility": "on" }, { "color": "#f2f2f2" }] }, { "featureType": "water", "elementType": "labels.text", "stylers": [{ "color": "#acacac" }, { "weight": "0.01" }, { "visibility": "off" }] }]
            });
            this.setMarkers();
            stolt.maps.setOverlays();

            // Events for toggling pins/overlays
            $('.map-overlay-controls li.marker').each(function (eq, me) {

                $(me).click(function () {
                    var $currentActive = $('.map-overlay-controls .marker.active'),
                        $currentActiveData = $currentActive.data('type');

                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        stolt.maps.toggleMarkers($(this).data('type'), false);
                    } else {
                        $(this).addClass('active');
                        //$currentActive.removeClass('active').removeClass('current');
                        //stolt.maps.toggleMarkers($currentActiveData, false);
                        stolt.maps.toggleMarkers($(this).data('type'), true);
                    }
                });

            });
            $('.map-overlay-controls li.active').each(function () {
                $(this).addClass('current');
            });

            $('.map-overlay-controls li.overlay').each(function (eq, me) {
                $(me).click(function () {
                    if ($(this).hasClass('active')) {
                        stolt.maps.toggleOverlays('hideAll');
                        $('.map-overlay-controls li.overlay').removeClass('active');
                    } else {
                        stolt.maps.toggleOverlays('showAll');
                        $('.map-overlay-controls li.overlay').removeClass('active');
                        $(this).addClass('active');
                    }
                });
            });

            $('.map-zoom-controls li').each(function (eq, me) {
                $(me).click(function () {
                    if ($(this).hasClass('active')) {
                        //console.log($(this).data('region'))
                        stolt.maps.setRegion('home');
                        $('.map-zoom-controls li').removeClass('active hidepsudeo');
                        stolt.maps.toggleOverlays('showAll');
                    } else {
                        stolt.maps.setRegion($(this).data('region'));
                        $('.map-zoom-controls li').removeClass('active hidepsudeo');
                        $(this).addClass('active').next('li').addClass('hidepsudeo');
                        stolt.maps.toggleOverlays('hideAll');
                    }
                }).on('mouseenter', function () {
                    $(me).next().addClass('hidepsudeo');
                }).on('mouseleave', function () {
                    $(me).next().removeClass('hidepsudeo');

                });

            });

            $('.reset-map-zoom').click(function () {
                stolt.maps.setRegion('home');
            });

            Þ.window.stack('breakpoint', { maxWidth: 768 }, function () {
                stolt.maps.mobileRedirect();
                $('.figure-glance hr').remove();
                $('.biz-snip .tile:nth-child(2n)').after('<hr class="colpad_12"/>');
            });

            // Set initial zoomlevel var
            currentZoomLevel = stolt.maps.stoltmap.getZoom();

        } // end if
    }, // end init
    setMarkers: function () {
        // Check we have the data
        if (typeof stolt.maps.data === 'undefined' || stolt.maps.data.length === 0) return;
        if (typeof stolt.maps.data.stoltpins === 'undefined' || stolt.maps.data.stoltpins.length === 0) return;

        function makeMarkup(pin) {
            var heading;
            if (pin.url > '') {
                heading = $('<a/>').attr('href', pin.url).attr('class', 'read-more').attr('title', pin.title).html(pin.title);
            } else {
                heading = $('<p/>').attr('class', 'popup-header').html(pin.title);
            }
            var location = $('<p/>').attr('class', location).html(pin.location);
            var descriptionText = pin.description

            if (pin.telephone) {
                descriptionText += '<br/>Tel: ' + pin.telephone;
            }

            if (pin.emailAddress) {
                descriptionText += '<br/>Email: <a href="mailto:' + pin.emailAddress + '">' + pin.emailAddress + '</a>';
            }

            var description = $('<p/>').html(descriptionText);

            return heading.prop('outerHTML') + location.prop('outerHTML') + description.prop('outerHTML');
        }

        for (var pinTypeCtr in stolt.maps.data.stoltpins) {
            var pinType = stolt.maps.data.stoltpins[pinTypeCtr];
            for (var pinCtr in pinType.pins) {
                var pin = pinType.pins[pinCtr];
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pin.lat, pin.lng),
                    //anchor:  new google.maps.Marker({pinType.offsetX, pinType.offsetY}),
                    map: stolt.maps.stoltmap,
                    animation: google.maps.Animation.DROP,
                    icon: pinType.icon,
                    title: pin.title,
                    zIndex: pinType.zindex,
                    type: pinType.type,
                    visible: pinType.visible
                });
                var markup = makeMarkup(pin);
                google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                    //console.info(infowindow);
                    return function () {
                        if (typeof stolt.maps.infowindow !== 'undefined') {
                            stolt.maps.infowindow.close();
                        }
                        infowindow.setContent(content);
                        infowindow.open(stolt.maps.stoltmap, marker);
                        stolt.maps.infowindow = infowindow;
                    };
                })(marker, markup, new google.maps.InfoWindow({
                    maxWidth: 500
                })));
                // closure for context scope.
                stolt.maps.markerInstances.push(marker);
                if (stolt.maps.debug) {
                //console.log('Marker data: ', pinType);
                }
            } // end for each (pin)
        } // end for each (pinType)
    },
    toggleMarkers: function (type, visible) {
        type = type || 'showAll';
        if (stolt.maps.debug) {
            console.info('marker type: ', type);
        }
        for (var marker in stolt.maps.markerInstances) {
            if (stolt.maps.debug) {
                //console.log('marker in loop: ', stolt.maps.markerInstances[marker].title, marker);
                //console.log('marker setVisible: ', stolt.maps.markerInstances[marker].type === type);
            }
            if (type === 'showAll' || stolt.maps.markerInstances[marker].type === type) {
                stolt.maps.markerInstances[marker].setVisible(visible);
            }
        } // end for each
        if (typeof stolt.maps.infowindow !== 'undefined') {
            stolt.maps.infowindow.close();
        }
    },
    setOverlays: function () {
        // Check we have the data
        if (typeof stolt.maps.data === 'undefined' || stolt.maps.data.length === 0) {
            return;
        }
        if (typeof stolt.maps.data.overlays === 'undefined' || stolt.maps.data.overlays.length === 0) {
            return;
        }

        for (var overlay in stolt.maps.data.overlays) {
            var overlaydata = stolt.maps.data.overlays[overlay];
            this.overlayInstances[overlaydata.name] = new this.imageLayer(
                // overlay bounds
                new google.maps.LatLngBounds(
                    new google.maps.LatLng(overlaydata.bottom, overlaydata.left), // bottom left corner
                    new google.maps.LatLng(overlaydata.top, overlaydata.right) // top right corner
                ),
                // image src
                overlaydata.image,
                // map instance
                stolt.maps.stoltmap,
                stolt.maps.data.overlaysVisible
            );

            stolt.maps.overlayInstances[overlaydata.name].name = overlaydata.name || 'overlay_' + new Date().valueOf();
            stolt.maps.overlayInstances[overlaydata.name].setMap(stolt.maps.stoltmap);
            if (stolt.maps.debug) {
                //console.log('Overlay data: ', overlaydata);
            }
        } // end for each
    },
    toggleOverlays: function (name) {
        name = name || 'showAll';
        if (stolt.maps.debug) {
            console.info('overlay name: ', name);
        }
        for (var overlay in stolt.maps.overlayInstances) {
            if (stolt.maps.debug) {
                //console.log('overlay in loop: ', overlay);
                //console.log('overlay show/hide: ', stolt.maps.overlayInstances[overlay].name === name);
            }
            stolt.maps.overlayInstances[overlay].setVisible(name === 'showAll' ? true : false);
        }
        if (typeof stolt.maps.infowindow !== 'undefined') {
            stolt.maps.infowindow.close();
        }
    },
    setRegion: function (region) {
        region = region || 'home';

        // Find the region
        var regionToShow = {
            lat: 0,
            lng: 0,
            zoom: 2
        };
        for (var i = 0, len = stolt.maps.data.regions.length; i < len; i++) {
            if (stolt.maps.data.regions[i].title === region) {
                regionToShow = stolt.maps.data.regions[i];
                break;
            }
        }

        stolt.maps.stoltmap.setZoom(regionToShow.zoom);
        stolt.maps.stoltmap.setCenter(new google.maps.LatLng(regionToShow.lat, regionToShow.lng));
        if (typeof stolt.maps.infowindow !== 'undefined') {
            stolt.maps.infowindow.close();
        }
    },
    mobileRedirect: function () {
        $('.map-overlay-controls li.active ').removeClass('active').addClass('mod');
        $('.map-overlay-controls li a ').click(function () {

        });
    }

}; // end maps
// Terminal selector
stolt.terminalselector = {}; // end terminal selector

/*use this on empty a tags for screenreaders*/
stolt.anchortext = {
    init: function () {
        $('a').each(function (eq, me) {
            if (typeof $(me).attr('title') !== 'undefined') {
                $(me).attr({
                    'aria-label': $(me).attr('title')
                });
            }
        });
    }
};
stolt.calcIframeHeight = {
    init: function () {
        this.resize = function () {
            $('#dynIframe')[0].contentWindow.postMessage('Info', '*');

            if (window.addEventListener) {
                window.addEventListener('message', receiver, false);
            } else {
                $('#dynIframe').height(1600);
            }
            function receiver(e) {
                $('.loading').remove();
                $('#dynIframe').height(e.data);
            }
        };
    }
};
stolt.countup = {
    mc_animateChart: function (duration) {
        $('.knob').each(function () {
            var elm = $(this);
            var color = elm.attr("data-fgColor");
            var perc = elm.attr("value");
            elm.knob({
                'value': 0,
                'min': 0,
                'max': 100,
                "readOnly": true,
                'dynamicDraw': true,
                "displayInput": false
            });
            $({
                value: 0
            }).animate({
                value: perc
            }, {
                duration: duration,
                easing: 'swing',
                progress: function () {
                    elm.val(Math.ceil(this.value)).trigger('change');
                }
            });
            //circular progress bar color
            $(this).append(function () {
                elm.parent().parent().find('.circular-bar-content').css('color', color);
                elm.parent().parent().find('.circular-bar-content label').text(perc + '%');
            });
        });
    },
    init: function () {
        if ($('.mdl-fast-facts').length > 0) {
            $('.countup').counterUp({
                delay: 10,
                time: 2000
            });
            $('.mdl-fast-facts').waypoint(function (direction) {
                stolt.countup.mc_animateChart(2000);
            }, {
                offset: 500
            });
        } else {
            $('.countup').counterUp({
                delay: 10,
                time: 1000
            });
            $('.mod-biz, .mdl-performance').waypoint(function (direction) {
                //console.log('waypoint.');
                stolt.countup.mc_animateChart(1000);
            }, {
                offset: '100%'
            });
        }
    }
};
stolt.bodAccordion = {
    init: function () {
        $('.mdl-bod article').each(function (eq, me) {
            $(me).find('.control').click(function () {
                $(this).toggleClass('open');
                $(me).find('.block-wht').slideToggle("slow").toggleClass('reveal');
            });
        });
    }
};
/*move hero on homepage up.*/
stolt.moveHeroCopy = {
    init: function () {
        var videoPlaying = false;
        $('.bgvideo').on('play', function (e) {
            var videoPlaying = true;
            $('.video-wrap, .copyvid').removeClass('isPoster');
            $(this).parent().addClass('isPlaying');
            stolt.moveHeroCopy.moving();
        });
        if (videoPlaying === false) {
            $('.video-wrap, .copyvid').addClass('isPoster');
            Þ.window.stack('breakpoint', { maxWidth: 768 }, function () {
                $('.video-wrap.isPoster').each(function (eq, me) {
                    var poster = $(me).find('video').attr('poster');
                    $(me).css({
                        'background-image': 'url(' + poster + ')'
                    }).addClass('bgcover');
                });
            });
        }
    },
    moving: function () {

        if (Modernizr.video && $('.bgvideo').length > 0 && $('.mdl-hero-latest').length > 0) {
            $('.video-wrap').css({
                'top': $('.mdl-hero-latest').height() + 72
            });

        }
    }
};
stolt.sitemapmasonary = {
    init: function () {
        if ($('.masonry').length > 0) {
            var container = document.querySelector('.masonry');
            var msnry = new Masonry(container, {
                // options...
                itemSelector: 'section',
                percentPosition: true,
                columnWidth: '.item'
            });
        }
    }

};
stolt.menuTouchExpand = {
    init: function () {
        Þ.window.stack('breakpoint', { minWidth: 768 }, function () {
            var $listItem = $('#nav div>ul>li a'),
                $toggleSubNav = $('.toggleopenclose');
            $toggleSubNav.click(function (e) {
                if ($(this).parent().hasClass('open')) {
                    $(this).parent().removeClass('open');
                } else {
                    e.preventDefault();
                    $(this).parent().addClass('open');
                }
            });
        });
    }
};
stolt.bxslider = {
    doresize: false,
    directionControls: function () {
        if (this.doresize) {
            $('.bx-wrapper').each(function (eq, me) {
                $(me).find('.bx-prev').addClass('bx-apended-control').prependTo($(me).find('.bx-pager'));
                $(me).find('.bx-next').addClass('bx-apended-control').appendTo($(me).find('.bx-pager'));
            });
        }
    },
    init: function () {
        var bxsettings = {},
            runbx = false;
        if ($('.mdl-timeline-scroll').length > 0 /*&& $(window).width() > 480*/) {
            $('.mdl-timeline-scroll').tinycarousel({
                axis: "y"
            });
            runbx = true;
        } else if ($('.gallery').length > 0) {
            //initialise colorbox for the gallery
            $(".galleryitem").colorbox({
                rel: 'gallery',
                transition: "fade",
                width: "95%",
                height: "95%"
            });
            runbx = true;
        } else if ($('.mdl-homeStarRunner').length > 0) {
            runbx = true;
        }
        if (runbx) {
            stolt.bxslider.instances = [];
            $('.bxslider').each(function (eq, me) {
                stolt.bxslider.instances.push($(me).bxSlider(stolt.bxslider.get.variant($(me).attr('data-variant'))));
            });

        }
        $('.mdl-timeline-scroll .bx-wrapper img').each(function (eq, me) {
            $(me).parent().addClass('has-image');
        });
        $(window).resize(function () {
            stolt.bxslider.directionControls();
        });
    }
    ,
    get: {

        //set the setings for each bx variant... needs data att value on bxslider
        variant: function (variant) {
            switch (variant) {
                case 'homestarrunner':
                    return {
                        mode: $(window).width() < 768 ? 'horizontal' : 'vertical',
                        pager: true,
                        auto: true,
                        pause: 5000,
                        adaptiveHeight: false,
                        pagerCustom: '#bx-pager',
                        onSliderLoad: function (currentIndex) {
                            Modernizr.testProp('touch');
                            Þ.window.stack('breakpoint', { minWidth: 768 }, function () {
                                $('.mdl-homeStarRunner').find('video').each(function (eq, me) {
                                    //set the video attributes via JS to initialise it in the slider... does not work otherwise. we do not need fitVids as we are not using an iframe vedeo source e.g. youTube or Vimeo
                                    $(me)[0].play();
                                    $(me)[0].loop = true;

                                });
                            });
                        }
                    };
                case 'gallery':
                    return {
                        mode: 'horizontal',
                        pager: true,
                        adaptiveHeight: true
                    };
                case 'timeline':
                    return {
                        mode: 'horizontal',
                        pager: true,
                        adaptiveHeight: false,
                        onSliderLoad: function () {
                            stolt.bxslider.directionControls();
                        }
                    };
                default:
                    return {};
            }
        }
    }
};
stolt.cookie = {
    scope: "sn_",
    create: function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toUTCString();
        } else {
            expires = "";
        }
        document.cookie = this.scope + name + "=" + value + expires + "; path=/";
        return true;
    },

    read: function (name) {
        var nameEQ = this.scope + name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
        }

        return null;

    },

    erase: function (name) {
        document.cookie = this.scope + name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
        return true;
    },
    init: function () {
        if (!this.read('notification')) {
            $('.mdl-notification').addClass('show');
            $('.acceptCookie').click(function () {
                stolt.cookie.sn_accept();
            });
        } else {
            $('.mdl-notification').remove();

        }
    },
    sn_accept: function () {
        this.create('notification', 'true', 365);
        $('.mdl-notification').slideUp(500, function () {
            $(this).remove();
        });

    }
};
stolt.searchInput = {
    searchActive: function () {
        var searchBox = $('.searchInput');
        searchBox.addClass('idleField');
        searchBox.focus(function () {
            $(this).removeClass('idleField').addClass('focusField');
            if (this.value === this.defaultValue) {
                this.value = '';
            }
            if (this.value !== this.defaultValue) {
                this.select();
            }
        });
        searchBox.blur(function () {
            $(this).removeClass('focusField').addClass('idleField');
            if ($.trim(this.value) === '') {
                this.value = (this.defaultValue ? this.defaultValue : '');
            }
        });
    },
    searchReveal: function () {
        $('.btn-open-search').click(function () {
            $('.mdl-hearder-search').toggleClass('open').slideToggle(500);
            if (!$.isIE8) {
                $('.searchInput').focus();
            }
            $(this).toggleClass("open");

            var attr = $('.mdl-stolt-login').attr('style');
            if (typeof attr !== typeof undefined && attr == 'display: block;') {
                $('.login').trigger('click');
            }
        });
        //check and set the cookie
        $('.mdl-secure-login .remebercheck').click(function () {
            if ($(this).prop('checked')) {
                stolt.cookie.create('hasvisited', 1, 1);
            } else {
                stolt.cookie.erase('hasvistied');
            }

        });

        //pass a cookie to not have to show the login again, so if the cookie is active, then hide the entry screen.
        if (stolt.cookie.read('hasvisited') !== null) {
            // hide the thing
            $('.mdl-secure-login .signin, .mdl-secure-login .checkwrap').hide().addClass('hide');
            $('.mdl-secure-login .submit').show(0).animate({
                opacity: 1
            }, 250, function () {
                $(this).addClass('show');
            });
            $('.mdl-secure-login .copy').slideUp(25, function () {
                $(this).addClass('show');
            });
        }
    }
};
stolt.navMenu = {
    openLogin: function () {
        //make some stuff happen here to show the login screen.
        $('.login').click(function () {
            $('.mdl-stolt-login').slideToggle(500, function () { }).toggleClass('open');
            $(this).toggleClass('open');
            stolt.equalheight.init('.mdl-stolt-login .eh-view');

            var attr = $('.mdl-hearder-search').attr('style');
            if (typeof attr !== typeof undefined && attr == 'display: block;') {
                $('.btn-open-search').trigger('click');
            }
        });

    },

    moveIndicator: function () {
        //add a caret element to primary navigation and position dynamically
        //as due to design parent cannot be set to relative.
        $('.nav ul.i>li').each(function (eq, me) {

            $(me).mouseenter(function () {
                //on enter
                if ($(this).find('.ii').length > 0 && $(this).find('.indicator').length === 0) {
                    $('<i class="indicator"></i>').appendTo(this).css({
                        'left': $(this).position().left + ($(this).width() / 2) + 7
                    });
                }
            });
        });
    },
    apendWrapper: function () {
        if ($('.mdl-nav').length === 0) {
            $('.nav').wrap('<div class="mdl-nav nav-target content"></div>');
        }

        $('.innerwrap').addClass('dt');
        if ($('.header-outerwrap').length === 0) {
            $('div.header').prependTo('.innerwrap');
            $('.nav-target').wrapAll('<div class="header-outerwrap" style="width:100%"></div>');
            //Some sneaky work for orientation change. where the wrapper is not getting the full widht of the viewport, by cahnging and resetting the display of the offending block, we force the page to redraw with out having to call a refresh.

        }

    },
    removeWrapper: function () {
        if ($('.header-outerwrap').length > 0) {
            $('.innerwrap').removeClass('dt');
            $('div.header').prependTo('.outerwrap');

            $('.mdl-hearder-search').unwrap('<div class="header-outerwrap"></div>');
        }
        if ($('.mdl-nav').length > 0) {
            $('.nav').unwrap('<div class="mdl-nav content"></div>');
        }
    },
    subMenuOpen: function () {
        //stolt.navMenu.removeWrapper();
        //append markers to the menu for mobile
        $('.nav ul').each(function (eq, me) {
            if ($(me).parent('li').length > 0) {
                $(this).before('<i class="more"></i>');
            }
        });
        //open sub navigation on burger menu
        $('.nav .more').click(function () {
            var $subUL = $(this).next('ul'),
                $ulTitle = $(this).prev('a').text();

            if ($subUL.hasClass('ii')) {
                $(this).toggleClass('less').next('ul').toggleClass('open').slideToggle(500);
            } else if ($subUL.hasClass('iii')) {
                $('.level-iii-mob').html($subUL.clone()).addClass('show').slideDown(500,
                    function () {
                        $("html, body").animate({
                            scrollTop: 0
                        }, 300);
                    });
                $('.nav ul.i').addClass('hide').slideUp(500);

                $('<div class="iiiTitle"><i class="back"></i>Back</div>').prependTo('.level-iii-mob').click(function () {
                    $('.level-iii-mob').removeClass('show').slideUp(500, function () {
                        $(this).empty();
                    });
                    $('.nav ul.i').removeClass('hide').slideDown(500);
                });
            }
        });

    },
    init: function () {
        stolt.navMenu.openLogin();

        Þ.window.stack('breakpoint', { minWidth: 992 }, function () {
            stolt.navMenu.apendWrapper();
            stolt.navMenu.moveIndicator();
            stolt.apendSubNav.init();

        });

        Þ.window.stack('breakpoint', { maxWidth: 993 }, function () {
            stolt.navMenu.removeWrapper()
            stolt.navMenu.subMenuOpen();
        });



    }
};
stolt.apendSubNav = {
    fadeinNavItems: function () {
        var timeoutDelay = 150;
        $('.level-ii-dt.open ul.ii .group>li').each(function (eq, me) {
            setTimeout(function () {
                $(me).animate({
                    opacity: 1
                });
            }, timeoutDelay * (eq + 1));
        });
    },
    wrapGroupItems: function () {
        //wrap them in groups of 6
        var groupitems = $('.level-ii-dt .ii>li');
        for (var i = 0; i < groupitems.length; i += 6) {
            groupitems.slice(i, i + 6).wrapAll("<div class='group'></div>");
        }
    },
    animateContainerHeight: function () {
        var containerHeight = $('.level-ii-dt .ii').height();
        $('.level-ii-dt').animate({
            height: containerHeight
        }, 150);
    },
    cloneOpenNav: function (params) {
        var srcObj = params.src,
            direction = typeof params != 'undefined' && typeof params.dir != 'undefined' ? params.dir : null,
            $ulSubNav = srcObj.find('.ii'),
            landingpageItem = srcObj.html();
        //if the subNav exists and destination is empty
        //clone it
        //append it
        //add flag is cloned to the thing you are entering
        if (($ulSubNav.length > 0) && $('.level-ii-dt ul').length < 1) {
            $('.level-ii-dt .content').remove();
            $ulSubNav.clone().appendTo('.level-ii-dt');
            $('.level-ii-dt .ii').addClass('col_12').wrap('<div class="content"></div>');
            stolt.apendSubNav.wrapGroupItems();
            $('.level-ii-dt').stop().clearQueue().slideDown(300, function () {
                stolt.apendSubNav.fadeinNavItems();
            }).addClass('open');
            srcObj.addClass('isCloned');
            this.animateContainerHeight();
        }
        //if the container already has a subnav in it and the thing you hovered on does not have the flag
        //remove what is there
        //clone the current subnav into the container
        else if (($ulSubNav.hasClass('isCloned') === false) && ($('.level-ii-dt').hasClass('open') === true)) {
            $('.level-ii-dt .ii').remove();
            $ulSubNav.clone().appendTo('.level-ii-dt .content');

            stolt.apendSubNav.wrapGroupItems();
            stolt.apendSubNav.fadeinNavItems();
            $('.nav ul.i>li').removeClass('isCloned');
            srcObj.addClass('isCloned');
            this.animateContainerHeight();

        } else if ($ulSubNav.length === 0) {
        }
    },
    destroyNavItems: function () {
        $('.nav li').unbind();
        $('.nav ul.i>li>*').unbind();
        $('.level-ii-dt').empty().hide();

    },
    lavaNav: function () {
        if ($('.nav').find('.lava').length < 1) {
            $('.nav').prepend('<i class="lava"></i>');
        }
        var $activeNavItem = $('.nav ul.i>li.current>*:first-child'),
            activeNavItemWidth = $activeNavItem.width(),
            activeNavItemPosition = $activeNavItem.position().left,
            $lava = $('.lava');
        $lava.css({
            width: 0,
            left: activeNavItemPosition
        });

        //console.log($activeNavItem);

        $('.nav .i>li').mouseenter(function () {
            var navItemWidth = $(this).children().width();
            $activeNavItem.addClass('haslava');
            $lava.stop().animate({
                left: $(this).children().position().left,
                width: navItemWidth
            }, {
                duration: 300
            }).addClass('animated');
        });
    },
    init: function () {


        function bindActionIn(el) {
            el.preventDefault();
            $(el.target).addClass('hover');
            stolt.apendSubNav.cloneOpenNav({
                src: $(el.target).parent(),
                status: 'enter'
            });
        }

        function bindActionOut(el) {
            $(el.target).removeClass('hover');
        }

        function showSubmenu(event) {
            $(event.target).addClass('hover');
        }

        function hideSubmenu(event) {
            $(event.target).removeClass('hover');
            //if the element hovered has the flag
            //close and remove contents from the container
            $('.level-ii-dt').slideUp(300, function () {
                $('.level-ii-dt .ii').remove();
            }).removeClass('open');
            $('.nav li').removeClass('isCloned');
        }

        //append the sub navigation to a new substructure
        if ($('.nav').length > 0) {
            $('.nav ul.i>li>*').mouseenter(bindActionIn).mouseleave(bindActionOut);
            $('.nav ul.i>li>*').on('touchstart', bindActionIn);

            $('.level-ii-dt').mouseleave(function () {
                //console.log('mouse out subnav');
                stolt.apendSubNav.lavaNav();
                $('.nav ul.i>li.current>*').removeClass('haslava')
            });

            $(document).click(function (event) {
                if ($(event.target).closest('.nav').length === 0) {
                    bindActionOut(event);
                    hideSubmenu(event);
                }
            });


            //Hide and empty when the mouse leaves
            $('.level-ii-dt').mouseenter(showSubmenu).mouseleave(hideSubmenu);
            $('.level-ii-dt').on('touchstart', showSubmenu)

            //prevent the immediate a tag from linking to the new page.
            $('.nav li').each(function (eq, me) {
                var $ulSubNav = $(me).find('ul.ii');
            });
            this.lavaNav();
        }//end if
    }
};


stolt.spotNav = {
    runOnce: false,
    init: function () {
        //resize event run on first load and on resize, but we would like to be able to make sure that if the user opens a small window that if the window is made larger, the event fires.
        Þ.window.stack('resize', function () {
            //fire the event
            stolt.spotNav.build();
        });
    },
    //for large desktop devices
    build: function () {
        var itemCount = $('.mdl-spotnav .item').length;
        $('.mdl-spotnav .item').each(function (eq, me) {
            if ($(window).width() > 992) {
                $(me).css({
                    'width': 100 / itemCount + '%'
                });
            } else {
                $(me).removeAttr('style');
            }
            $(me).append('<i>').mouseenter(function () {
                $(me).addClass('hover');
            }).mouseleave(function () {
                $(me).removeClass('hover');
            });
        });
        //set the runOnce variable to true!!! :) - happy squanch days!
        stolt.spotNav.runOnce = true;
    }

};

stolt.offcanvas = {
    init: function () {
        /*! * *  Copyright (c) David Bushell | http://dbushell.com/ * */
        (function (window, document, undefined) {
            // helper functions
            var trim = function (str) {
                return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, '');
            };
            var hasClass = function (el, cn) {
                return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
            };
            var addClass = function (el, cn) {
                if (!hasClass(el, cn)) {
                    el.className = (el.className === '') ? cn : el.className + ' ' + cn;
                }
            };
            var removeClass = function (el, cn) {
                el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
            };
            var hasParent = function (el, id) {
                if (el) {
                    do {
                        if (el.id === id) {
                            return true;
                        }
                        if (el.nodeType === 9) {
                            break;
                        }
                    }
                    while ((el = el.parentNode));
                }
                return false;
            };
            // normalize vendor prefixes
            var doc = document.documentElement;
            var transform_prop = window.Modernizr.prefixed('transform'),
                transition_prop = window.Modernizr.prefixed('transition'),
                transition_end = (function () {
                    var props = {
                        'WebkitTransition': 'webkitTransitionEnd',
                        'MozTransition': 'transitionend',
                        'OTransition': 'oTransitionEnd otransitionend',
                        'msTransition': 'MSTransitionEnd',
                        'transition': 'transitionend'
                    };
                    return props.hasOwnProperty(transition_prop) ? props[transition_prop] : false;
                })();
            window.App = (function () {
                var _init = false,
                    app = {};
                var inner = document.getElementById('innerwrap'),
                    nav_open = false,
                    nav_class = 'js-nav';
                app.init = function () {
                    if (_init) {
                        return;
                    }
                    _init = true;
                    var closeNavEnd = function (e) {
                        if (e && e.target === inner) {
                            document.removeEventListener(transition_end, closeNavEnd, false);
                        }
                        nav_open = false;
                    };
                    app.closeNav = function () {
                        if (nav_open) {
                            // close navigation after transition or immediately
                            var duration = (transition_end && transition_prop) ? parseFloat(window.getComputedStyle(inner, '')[transition_prop + 'Duration']) : 0;
                            if (duration > 0) {
                                document.addEventListener(transition_end, closeNavEnd, false);
                            } else {
                                closeNavEnd(null);
                            }
                        }
                        removeClass(doc, nav_class);
                    };
                    app.openNav = function () {
                        if (nav_open) {
                            return;
                        }
                        addClass(doc, nav_class);
                        nav_open = true;
                    };
                    app.toggleNav = function (e) {
                        if (nav_open && hasClass(doc, nav_class)) {
                            app.closeNav();
                        } else {
                            app.openNav();
                        }
                        if (e) {
                            e.preventDefault();
                        }
                    };
                    // open nav with main "nav" button
                    try {
                        document.getElementById('nav-open-btn').addEventListener('click', app.toggleNav, false);
                        // close nav with main "close" button
                        document.getElementById('nav-close-btn').addEventListener('click', app.toggleNav, false);
                        // close nav by touching the partial off-screen content
                        document.addEventListener('click', function (e) {
                                if (nav_open && !hasParent(e.target, 'nav')) {
                                    e.preventDefault();
                                    app.closeNav();
                                }
                            },
                            true);
                    } catch (e) { }
                    addClass(doc, 'js-ready');
                };
                return app;
            })();
            if (window.addEventListener) {
                window.addEventListener('DOMContentLoaded', window.App.init, false);
            }
        })(window, window.document);
    }
};
//ensuring no widows
stolt.clearWidow = {
    init: function () {
        $(".clearwidow").each(function () {
            var wordArray = $(this).text().split(" ");
            if (wordArray.length > 1) {
                wordArray[wordArray.length - 2] += "&nbsp;" + wordArray[wordArray.length - 1];
                wordArray.pop();
                $(this).html(wordArray.join(" "));
            }
        });
    }
};
stolt.equalheight = {
    init: function (selector) {
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = [],
            $el = 0,
            topPosition = 0,
            $elements = $(selector);
        for (var containerInLoop = 0, cLoops = $elements.length; containerInLoop < cLoops; containerInLoop++) {
            $elements.each(function () {
                $el = $(this);
                $($el).height('auto');
                var topPostion = $el.position().top;
                if (currentRowStart !== topPostion) {
                    rowDivs.length = 0; // empty the array
                    currentRowStart = topPostion;
                    currentTallest = $el.height();
                    rowDivs.push($el);
                } else {
                    rowDivs.push($el);
                    currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
                }
                for (var currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                    if ($el.find('.block-wht') && $el.find($('.cta'))) {
                        var headingHeight = $el.children('h2').outerHeight(true);
                        rowDivs[currentDiv].children('.block-wht').height(currentTallest - headingHeight - 120 - 60);
                    }
                }
            });
        }
    }
};

stolt.responsiveTableTitleAtr = {
    init: function () {
        if ($('main table thead').length > 0) {
            $('main table').each(function (eq, me) {
                var tableHeadings = [];
                $(me).find('th').each(function () {
                    tableHeadings.push($(this).text());
                    //console.log($(this).attr("title"));
                });
                $(me).find('tbody tr').each(function (tEq, tMe) {
                    for (var i = 0, iLoops = tableHeadings.length; i < iLoops; i++) {
                        $(tMe).find('td:eq(' + i + ')').attr({
                            'title': tableHeadings[i]
                        });
                    }
                });
            });
        }
    }
};
stolt.feed = {
    settings: {
        timeout: 5e3 * 60 // 5 minutes
    },
    init: function () {
        this.getFeed();
        this.timer = setInterval(function () {
            stolt.feed.getFeed();
        }, this.settings.timeout);
    },
    getFeed: function () {
        var feedData = $.ajax({ // get and parse
            url: 'https://ir.asp.manamind.com/products/xml/snapshot.do?key=stoltnielsen_std&lang=en',
            type: "GET",
            crossDomain: true,
            dataType: 'xml',
            success: function (d) {
                var xmlObj = $.xml2json(d);
                //console.log(xmlObj);
                typeof xmlObj.snapshot !== 'undefined' ? stolt.feed.drawData(xmlObj.snapshot) : stolt.feed.fail();
            },
            fail: function () {
                stolt.feed.fail();
            }
        });
    },
    drawData: function (xmlObj) {
        var date = new Date(parseInt(xmlObj.time, 10)),
            isoTimestamp = date.getFullYear() + '-' + this.pad2digit(date.getMonth() + 1) + '-' + this.pad2digit(date.getDate()) + ' ' + this.pad2digit(date.getHours()) + ':' + this.pad2digit(date.getMinutes());
        $('.timestamp').html(isoTimestamp);
        $('.share-price .fig').html(xmlObj.last);
        $('.share-price .change .point').html(xmlObj.change);
        $('.share-price .change .percent').html(parseFloat(xmlObj.changePercent).toFixed(2) + '%');
        $('.shareprice-details .bid').html(xmlObj.bid);
        $('.shareprice-details .high').html(xmlObj.high);
        $('.shareprice-details .low').html(xmlObj.low);
        $('.shareprice-details .close').html(xmlObj.close);
        $('.shareprice-details .ask').html(xmlObj.ask);
        $('.shareprice-details .volume').html(xmlObj.volume);
        $('.share-price .change').addClass(parseFloat(xmlObj.change) > 0 ? 'pos' : 'neg');
    },
    pad2digit: function (input) {
        return String(input).length === 1 ? '0' + input : input;
    },
    fail: function () {
        // fallback draw data on ajax fail
        $('.mdl-manamind-feed').html('Shareprice currently unavailable');
    }
};

stolt.windowResizeEvents = {
    init: function () {
        $(window).bind('resizeEnd', function (event) {
            width = $(this).width();
            windowResizeEvents.eventsList(width);
            moveHeroCopy.moving();
            barchart.init();

        });
    },
    //This function delays the action on window resize, 
    //prevents events cueing with every pixel changed
    preventRepeat: function () {
        $(window).resize(function () {
            if (this.resizeTO) {
                clearTimeout(this.resizeTO);
            }
            this.resizeTO = setTimeout(function () {
                $(this).trigger('resizeEnd');
            }, 500);
        });
    },
    eventsList: function (width) {
        var wHeight = $(window).height(),
            wWidth = $(window).width();
        if (wWidth > 992) {

            // add way point for CSS animations
            $('.tml-home .porthole-wrap').waypoint(function (direction) {
                if (direction === 'down') {
                    $('.innerwrap').addClass('animate');
                }
                else {
                    $('.innerwrap').removeClass('animate');
                }
            }, {
                offset: '25%'
            });


        }
        if (wWidth < 991) {
            //navMenu.subMenuOpen();
        }
        if (wWidth > 768) {
            $('.map-overlay-controls li.mod ').addClass('active').removeClass('mod');
            maps.init();
            //console.log('> 1024 && wWidth > 768');
            //only add the horizontal when needed
            $('.figure-glance hr').remove();
            if ($('.biz-snip .tile').length > 4) {
                $('.biz-snip').after('<hr class="colpad_12"/>');
                $('.biz-snip .tile:nth-child(4n)').after('<hr class="colpad_12"/>');
            } else if ($('.biz-snip .tile').length < 4) {
                $('.biz-snip').after('<hr class="colpad_12"/>');
            }
        }
        if (wWidth > 768 && wWidth < 1024) {
            //console.log('> 1024 && wWidth > 768');
        }


        //safari stuff here

    }
};

stolt.loginHandler = {
    login: {
        handleAjaxAntiForgeryToken: function (xhr) {

            // Add anti forgery token to header so it works with AJAX http://richiban.wordpress.com/2013/02/06/validating-net-mvc-4-anti-forgery-tokens-in-ajax-requests/
            var securityToken = $('[name=__RequestVerificationToken]').val();
            xhr.setRequestHeader('__RequestVerificationToken', securityToken);
        },

        changeUrlOnSuccess: function (data) {

            // If data Key is true then redirect to URL in Value
            if (data.Key !== undefined && data.Key === true) {
                window.location = data.Value;
            }
        }
    }
};
window.onload = function () {
    if (Þ.browser.is.IE9()) {
        stolt.equalheight.init('.eh');
    }

};
stolt.stats = {
    init: function () {
        //NOTE
        //uncomment the line before go live
        // Þ.stats.GA.attach.downloadClickEvent('a');

    }
};

$(document).ready(function () {
    stolt.init();
    //TO DO
    //make sticky footer dynamic
});