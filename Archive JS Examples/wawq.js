/// <reference path="/js/libraries/jquery.min.js" />
/// <reference path="/js/libraries/foundation.js" />
/// <reference path="/js/libraries/thorn.min.js" />
var weAreWorldQuant = weAreWorldQuant || {};
weAreWorldQuant.config = {
    delays: {
        uiEasingDelay: 0.7e3
    }
    //breakpoints: {
    //    small: 0,
    //    medium: 640,
    //    large: 1024,
    //    xlarge: 1200,
    //    xxlarge: 1440
    //}
};


weAreWorldQuant.assets = {
    svg: {
        dropdownArrowRed: '<svg version="1.1" class="svg_arrow-down" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 40 22" enable-background="new 0 0 40 22" xml:space="preserve" width="40" height="22"> <polyline fill="none" points="0,0 20,22 40,0 " stroke="#E7412B" stroke-width="1" stroke-linecap="square" stroke-linejoin="miter" /></svg>'
        ,
        videoPlayButton: '<svg version="1.1" class="svg_play-button" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="240px" height="240px" viewBox="0 0 240 240" enable-background="new 0 0 240 240" xml:space="preserve"> <circle fill="none" cx="120" cy="120" r="118" stroke="#FFFFFF" stroke-width="2px" /> <polyline fill="none" points="102.3,78.3 150.1,126.1 101.6,174.5 " stroke="#FFFFFF" stroke-width="2px" /></svg>'
        ,
        arrowDown: '<svg version="1.2" baseProfile="tiny" class="icon-arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" width="39px" height="25px" viewBox="0 0 39 25" xml:space="preserve"><path fill="#f69a20" d="M0,5.2L5.1,0l14.4,14.6L33.9,0L39,5.2L19.5,25L0,5.2z"/></svg>'
        ,
        arrowRight: '<svg version="1.1" baseProfile="tiny" class="icon-arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="39px" viewBox="0 0 25 39" xml:space="preserve"> <path fill="#F69A20" d="M5.2,39L0,33.9l14.6-14.4L0,5.1L5.2,0L25,19.5L5.2,39z"/> </svg>'
        ,
        arrowRightSml: '<svg version="1.1" baseProfile="tiny" class="icon-arrow-right lulu" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" width="14.7px" height="23px" viewBox="0 0 14.7 23" overflow="scroll" xml:space="preserve"> <path fill="#F69A20" d="M3.1,23L0,20l8.6-8.5L0,3l3.1-3l11.7,11.5L3.1,23z"/> </svg>'
        ,

        arrowLeft: '<svg version="1.1" baseProfile="tiny" class="icon-arrow-left" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="25px" height="39px" viewBox="0 0 25 39" xml:space="preserve"> <path fill="#F69A20" d="M19.8,0L25,5.1L10.4,19.5L25,33.9L19.8,39L0,19.5L19.8,0z"/></svg>'
        ,
        arrowDownRound: '<svg version="1.2" baseProfile="tiny" class="icon-arrow-round" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="38px" height="38px" viewBox="0 0 38 38" xml:space="preserve"> <path fill="#F69A20" class="circle" d="M20.9,0.6C9.2-0.6-0.6,9.2,0.6,20.9c0.9,8.7,7.9,15.7,16.5,16.5c11.7,1.2,21.5-8.6,20.3-20.3 C36.5,8.5,29.5,1.5,20.9,0.6z M20.8,36.4C9.7,37.5,0.5,28.3,1.6,17.2C2.4,9,9,2.4,17.2,1.6c11.1-1.1,20.3,8.1,19.2,19.2 C35.6,29,29,35.6,20.8,36.4z"/> <path fill="#F69A20" d="M11.6,16h14.9L19,23L11.6,16z"/> </svg> '
        ,
        phone: '<svg version="1.1" baseProfile="tiny" class="icon-phone" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="33.1px" height="46px" viewBox="0 0 33.1 46"  <g> <path fill="#ffffff" d="M15.4,29.3c0,0-5-8.5-5-8.6c-1-2.3-1.9-4.6-0.1-5.6L3.1,2.8C1.2,4.2-4,11.8,5.8,29.2 c10.4,18.3,20.3,17.5,22.2,16.3l-7-12C19.4,34.5,18,32.8,15.4,29.3z M32.9,40.8L32.9,40.8c0,0-5.5-9.3-5.5-9.3 c-0.4-0.7-1.4-1-2.1-0.5l-3.3,2l7,12c0,0,3.4-2,3.3-2l0,0C33.1,42.5,33.3,41.5,32.9,40.8z M14.5,12.5L14.5,12.5 c0.8-0.5,1-1.4,0.5-2.1l0,0c0,0-5.6-9.6-5.6-9.6C9,0,8.1-0.2,7.4,0.2L4,2.2l7.2,12.3C11.2,14.5,14.5,12.5,14.5,12.5z"/> </g> </svg> '
        ,
        mail: '<svg version="1.1" baseProfile="tiny" class="icon-mail" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="33.9px" height="31.6px" viewBox="0 0 33.9 31.6" xml:space="preserve"> <g> <path fill="#FFFFFF" d="M1.4,12.1c-0.7,0.3-1.2,0.9-1.4,1.7c-0.1,0.8,0.2,1.5,0.9,2l6.9,5L32.4,1.5L10.6,22.9l9.5,6.9 c0.6,0.4,1.4,0.6,2.1,0.4c0.7-0.2,1.3-0.8,1.5-1.5L33.9,1c0.1-0.3,0-0.6-0.2-0.8c-0.2-0.2-0.5-0.3-0.8-0.2L1.4,12.1z"/><path fill="#FFFFFF" d="M6.2,22.5l0.1,0.3l1.3,7.3c0.1,0.5,0.5,1,1,1.2c0.5,0.2,1.1,0.2,1.6-0.1c1.9-1.2,4.3-2.8,4.2-2.9L6.2,22.5 z"/></g></svg> '
        ,
        mapPin: '<svg version="1.1" baseProfile="tiny" class="icon-map-pin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="34px" height="50px" viewBox="0 0 34 50" xml:space="preserve"> <path fill-rule="evenodd" fill="#ffffff" d="M7,15.8c0-5.5,4.5-10,10-10c5.5,0,10,4.5,10,10c0,5.5-4.5,10-10,10 C11.5,25.8,7,21.3,7,15.8z M0,17c0,4.2,5.1,13,5.1,13L17,50l11.2-20c0,0,5.8-8.9,5.8-13c0-9.4-7.6-17-17-17C7.6,0,0,7.6,0,17z"/> </svg> '
        ,
        iconPlay: '<svg version="1.1" baseProfile="tiny" class="icon-play" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50"  <g> <path fill-rule="evenodd" fill="#F69A20" d="M25,0c13.8,0,25,11.2,25,25S38.8,50,25,50S0,38.8,0,25S11.2,0,25,0z"/> </g> <g> <path fill-rule="evenodd" fill="#FFFFFF" d="M32.5,25l-15-10.6v21.2L32.5,25"/> </g> </svg> '
    }
};
weAreWorldQuant.fallbacks = {
    ie9onLoad: function () {
        if (Þ.browser.is.IE9()) {
            $.subscribe('fireEqualHeight', function () {
                weAreWorldQuant.equalheight.init('.eh-large:visible');
            });
        }
    }
};
weAreWorldQuant.googleMaps = {
    debug: false,
    markerInstances: [],
    overlayInstances: {},
    preInit: function () {
        this.imageLayer = function (bounds, image, map, visible) {
            // Initialize all properties.
            this.bounds_ = bounds;
            this.image_ = image;
            this.map_ = map;
            this.visible_ = visible;
            // Define a property to hold the image's div. We'll
            // actually create this div upon receipt of the onAdd()
            // method so we'll leave it null for now.
            this.div_ = null;
            // Explicitly call setMap on this overlay.
            this.setMap(map);
        };
        this.imageLayer.prototype = new google.maps.OverlayView();
        this.imageLayer.prototype.draw = function () {
            // We use the south-west and north-east
            // coordinates of the overlay to peg it to the correct position and size.
            // To do this, we need to retrieve the projection from the overlay.
            var overlayProjection = this.getProjection();
            // Retrieve the south-west and north-east coordinates of this overlay
            // in LatLngs and convert them to pixel coordinates.
            // We'll use these coordinates to resize the div.
            var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
            var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());
            // Resize the image's div to fit the indicated dimensions.
            var div = this.div_;
            div.style.left = sw.x + 'px';
            div.style.top = ne.y + 'px';
            div.style.width = (ne.x - sw.x) + 'px';
            div.style.height = (sw.y - ne.y) + 'px';
        };
        this.imageLayer.prototype.onAdd = function () {
            var div = document.createElement('div');
            div.style.borderStyle = 'none';
            div.style.borderWidth = '0px';
            div.style.position = 'absolute';
            div.style.visibility = this.visible_ ? 'visible' : 'hidden';
            // Create the img element and attach it to the div.
            var img = document.createElement('img');
            img.src = this.image_;
            img.style.width = '100%';
            img.style.height = '100%';
            img.style.position = 'absolute';
            div.appendChild(img);
            this.div_ = div;
            // Add the element to the "overlayLayer" pane.
            var panes = this.getPanes();
            panes.overlayLayer.appendChild(div);
        };
        this.imageLayer.prototype.show = function () {
            if (this.div_) {
                this.div_.style.visibility = 'visible';
            }
        };
        this.imageLayer.prototype.onRemove = function () {
            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        };
        this.imageLayer.prototype.hide = function () {
            if (this.div_) {
                // The visibility property must be a string enclosed in quotes.
                this.div_.style.visibility = 'hidden';
            }
        };
        this.imageLayer.prototype.toggle = function () {
            if (this.div_) {
                if (this.div_.style.visibility === 'hidden') {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };
        this.imageLayer.prototype.setVisible = function (boolean) {
            if (this.div_) {
                if (boolean) {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };
        if (this.debug) {
            console.info('maps preInit done');
        }
    },
    init: function () {
        var svg = {
            path: {
                pinActive: 'M10.5,0C4.7,0,0,4.7,0,10.5c0,2.2,0.7,4.3,1.8,5.9l8.7,14.7l8.7-14.7c1.2-1.7,1.8-3.7,1.8-5.9 C21,4.7,16.3,0,10.5,0z M15.4,8.9l-7.7,7.7l-3.6-4c-0.5-0.5-0.5-1.3,0.1-1.7C4.7,10.4,5.5,10.5,6,11 l1.9,2.1l5.9-5.9c0.5-0.5,1.3-0.5,1.8,0S15.9,8.5,15.4,8.9z',
                pin: 'M10.5,0C4.7,0,0,4.7,0,10.5c0,2.2,0.7,4.3,1.8,5.9l8.7,14.7l8.7-14.7c1.2-1.7,1.8-3.7,1.8-5.9 C21,4.7,16.3,0,10.5,0z M10.5,16.2c-3.2,0-5.7-2.6-5.7-5.7s2.6-5.7,5.7-5.7s5.7,2.6,5.7,5.7S13.7,16.2,10.5,16.2z'
            }
        };
        $('.mdl-contact-list .accordion-content').each(function (i, me) {
            if ($(me).find('.button').length > 0) {
                $(me).addClass('hassbutton');
            }

        });
        if ($('#map').length > 0) {
            // extend default Map functionality
            this.preInit();
            this.mcMap = new google.maps.Map($('#map').get(0), { //.get(0) returns the native DOM object
                center: {
                    lat: 40,
                    lng: 0
                },
                zoom: 2,
                //maxZoom: 20,
                minZoom: 2,
                scrollwheel: true,
                disableDefaultUI: false,
                styles: [
                    { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] },
                    { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] },
                    { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] },
                    { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] },
                    { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] },
                    { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] },
                    { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] },
                    { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] },
                    { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] },
                    { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }
                ]
            });
            this.setMarkers();
            this.setOverlays();
            $('.setNewLocation input').change(function () {
                //console.time('on change');
                //console.debug($(this).data('region'));
                var thisDataRegion = $(this).data('region');


                weAreWorldQuant.googleMaps.setRegion(thisDataRegion, function () {
                    $('.mdl-contact-list').find('li.accordion-item').each(function (eq, me) {
                        //console.time('onchage each');
                        //console.debug(eq);
                        //console.debug($(me).data('region'));
                        //console.debug(thisDataRegion, ': thisDataRegion');
                        if (thisDataRegion === 'home') {
                            $('li.accordion-item').show(0, function () {
                                // console.log('this data region is home');
                            });
                        } else {
                            $(me).show(0);
                        }

                        if ($(me).data('region') != thisDataRegion || $(me).data('region') === 'undefined') {
                            $(me).hide(0);
                        }

                        //console.timeEnd('onchage each');
                    });
                });
                weAreWorldQuant.equalheight.init('.eh-large:visible');
                //console.timeEnd('on change');
            });
            //turn it all on
            $('.setNewFilter input').prop("checked", true);
            $('.setNewFilter').on('click', function (e) {
                var types = [];
                var isShowAll = false;
                if ($(e.target).val() === 'showAll') {
                    isShowAll = true;
                    $(e.target).val('showNone').html('Clear all');
                } else if ($(e.target).val() === 'showNone') {
                    types = [];
                    $('.setNewFilter input').prop('checked', false);
                    $('.mdl-contact-list > div > ul > li').addClass('hide');
                    $(e.target).val('showAll').html('Show all');
                    weAreWorldQuant.googleMaps.toggleMarkers(types);
                    return;
                }
                var $input = $('.setNewFilter input');
                $input.each(function (eq, me) {
                    var $contactList = $('.mdl-contact-list').find("[data-type='" + $(me).val() + "']"),
                        contactListVal = $('.mdl-contact-list li.accordion-item').data('type');
                    $('.mdl-contact-list > div > ul > li').addClass('hide');
                    if (isShowAll) {
                        $(me).prop('checked', true);
                        types.push('is Show all', $(me).val());
                        $contactList.removeClass('hide');
                    } else if ($(me).prop('checked')) {
                        types.push($(me).val());
                    }
                });
                for (var i = 0; i < types.length; i++) {
                    $('.mdl-contact-list').find("li[data-type='" + types[i] + "']").removeClass('hide');
                }
                if (this.debug) {
                    //     console.log(types);
                }
                weAreWorldQuant.googleMaps.toggleMarkers(types, function () {
                    if ($input.length === types.length) {
                        $('.setNewFilter').find('button').val('showNone').html('Clear all');
                    } else if (types.length === 0) {
                        $('.setNewFilter').find('button').val('showAll').html('Show all');
                    }
                });

                $.publish('fireEqualHeight');
            });
        } // end if
    }, // end init
    setMarkers: function () {
        // Check we have the data
        if (typeof weAreWorldQuant.googleMaps.data === 'undefined' || weAreWorldQuant.googleMaps.data.length === 0) return;
        if (typeof weAreWorldQuant.googleMaps.data.mcPins === 'undefined' || weAreWorldQuant.googleMaps.data.mcPins.length === 0) return;
        function makeMarkup(pin) {
            var heading;
            if (pin.url > '') {
                heading = $('<a/>').attr('href', pin.url).attr('class', 'read-more').attr('title', pin.title).html(pin.title);
            } else {
                heading = $('<p/>').attr('class', 'popup-header').html(pin.title);
            }
            var address1 = $('<p/>').html(pin.address1);
            var address2 = $('<p/>').html(pin.address2);
            var contact1 = $('<p/>').html(sprintf('<a href="tel:%s">%s</a>', pin.contact1, pin.contact1));
            var contact2 = $('<p/>').html(sprintf('<a href="fax:%s">%s</a>', pin.contact2, pin.contact2));
            return heading.prop('outerHTML') + address1.prop('outerHTML') + address2.prop('outerHTML') + contact1.prop('outerHTML') + contact2.prop('outerHTML');
        }
        for (var pinTypeCtr in weAreWorldQuant.googleMaps.data.mcPins) {
            var pinType = weAreWorldQuant.googleMaps.data.mcPins[pinTypeCtr];
            for (var pinCtr in pinType.pins) {
                var pin = pinType.pins[pinCtr];
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pin.lat, pin.lng),
                    //anchor:  new google.maps.Marker({pinType.offsetX, pinType.offsetY}),
                    map: weAreWorldQuant.googleMaps.mcMap,
                    icon: pinType.icon,
                    title: pin.title,
                    animation: google.maps.Animation.DROP,
                    zIndex: pinType.zindex,
                    type: pinType.type,
                    visible: pinType.visible
                });
                var markup = makeMarkup(pin);
                //open infowindow on mouseover
                google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                    //console.info(infowindow);
                    return function () {
                        if (typeof weAreWorldQuant.googleMaps.infowindow !== 'undefined') {
                            weAreWorldQuant.googleMaps.infowindow.close();
                        }
                        infowindow.setContent(content);
                        infowindow.open(weAreWorldQuant.googleMaps.mcMap, marker);
                        weAreWorldQuant.googleMaps.infowindow = infowindow;
                        google.maps.event.addListener(infowindow, 'domready', function () {
                            $('.gm-style-iw').parent().addClass('iw-styled');
                        });
                    };
                })(marker, markup, new google.maps.InfoWindow({
                    maxWidth: 600
                })));
                // closure for context scope.
                weAreWorldQuant.googleMaps.markerInstances.push(marker);
                if (weAreWorldQuant.googleMaps.debug) {
                    //console.log('Marker data: ', pinType);
                }
            } // end for each (pin)
        } // end for each (pinType)
    },
    toggleMarkers: function (types, callback) {
        types = types || ['showAll'];
        if (weAreWorldQuant.googleMaps.debug) {
            console.info('marker type: ', types);
        }
        for (var marker in weAreWorldQuant.googleMaps.markerInstances) {
            if (weAreWorldQuant.googleMaps.debug) {
                console.log('marker in loop: ', weAreWorldQuant.googleMaps.markerInstances[marker].title, marker);
                console.log('marker setVisible: ', types.indexOf(weAreWorldQuant.googleMaps.markerInstances[marker].type) > -1);
            }
            if (types[0] === 'showAll' || types.indexOf(weAreWorldQuant.googleMaps.markerInstances[marker].type) > -1) {
                weAreWorldQuant.googleMaps.markerInstances[marker].setVisible(true);
            } else {
                weAreWorldQuant.googleMaps.markerInstances[marker].setVisible(false);
            }
        } // end for each
        if (typeof weAreWorldQuant.googleMaps.infowindow !== 'undefined') {
            weAreWorldQuant.googleMaps.infowindow.close();
        }
        if (typeof callback === 'function') {
            callback.call();
        }
    },
    setOverlays: function () {
        // Check we have the data
        if (typeof weAreWorldQuant.googleMaps.data === 'undefined' || weAreWorldQuant.googleMaps.data.length === 0) {
            return;
        }
        if (typeof weAreWorldQuant.googleMaps.data.overlays === 'undefined' || weAreWorldQuant.googleMaps.data.overlays.length === 0) {
            return;
        }
        for (var overlay in weAreWorldQuant.googleMaps.data.overlays) {
            var overlaydata = weAreWorldQuant.googleMaps.data.overlays[overlay];
            this.overlayInstances[overlaydata.name] = new this.imageLayer(
                // overlay bounds
                new google.maps.LatLngBounds(
                    new google.maps.LatLng(overlaydata.bottom, overlaydata.left), // bottom left corner
                    new google.maps.LatLng(overlaydata.top, overlaydata.right) // top right corner
                ),
                // image src
                overlaydata.image,
                // map instance
                weAreWorldQuant.googleMaps.mcMap,
                weAreWorldQuant.googleMaps.data.overlaysVisible
            );
            weAreWorldQuant.googleMaps.overlayInstances[overlaydata.name].name = overlaydata.name || 'overlay_' + new Date().valueOf();
            weAreWorldQuant.googleMaps.overlayInstances[overlaydata.name].setMap(weAreWorldQuant.googleMaps.mcMap);
            if (weAreWorldQuant.googleMaps.debug) {
                console.log('Overlay data: ', overlaydata);
            }
        } // end for each
    },
    toggleOverlays: function (name) {
        name = name || 'showAll';
        if (weAreWorldQuant.googleMaps.debug) {
            console.info('overlay name: ', name);
        }
        for (var overlay in weAreWorldQuant.googleMaps.overlayInstances) {
            if (weAreWorldQuant.googleMaps.debug) {
                console.log('overlay in loop: ', overlay);
                console.log('overlay show/hide: ', weAreWorldQuant.googleMaps.overlayInstances[overlay].name === name);
            }
            weAreWorldQuant.googleMaps.overlayInstances[overlay].setVisible(name === 'showAll' ? true : false);
        }
        if (typeof weAreWorldQuant.googleMaps.infowindow !== 'undefined') {
            weAreWorldQuant.googleMaps.infowindow.close();
        }
    },
    setRegion: function (region, callback) {
        region = region || 'home';
        // Find the region
        var regionToShow = {
            lat: 0,
            lng: 0,
            zoom: 2
        };
        for (var i = 0, len = weAreWorldQuant.googleMaps.data.regions.length; i < len; i++) {
            if (weAreWorldQuant.googleMaps.data.regions[i].title === region) {
                regionToShow = weAreWorldQuant.googleMaps.data.regions[i];
                break;
            }
        }
        weAreWorldQuant.googleMaps.mcMap.setZoom(regionToShow.zoom);
        weAreWorldQuant.googleMaps.mcMap.setCenter(new google.maps.LatLng(regionToShow.lat, regionToShow.lng));
        if (typeof weAreWorldQuant.googleMaps.infowindow !== 'undefined') {
            weAreWorldQuant.googleMaps.infowindow.close();
        }

        if (typeof callback === 'function') {
            callback.call();
        }
    },
    mobileRedirect: function () {
        $('.map-overlay-controls li.active ').removeClass('active').addClass('mod');
    }
}; // end maps
weAreWorldQuant.specification = {
    init: function () {
        Þ.window.stack('breakpoint', { minWidth: 769 }, function () {
            if (!$('.spec-col').length > 0) {
                $('.mdl-specification').prepend('<div class="col-8 spec-col spec-col-1"></div><div class="col-8 spec-col spec-col-2"></div>');
                $('.mdl-product-detail dt').each(function (eq, me) {
                    var title = $(me).html() || '',
                        descrption = $(me).next('dd').html() || '';
                    if (!eq % 2) {
                        $('.spec-col-1').append(
                               sprintf('<div class="h3">%s</div><div></div>%s</div>', title, descrption)
                           );
                    } else {;
                        $('.spec-col-2').append(
                           sprintf('<div class="h3">%s</div><div></div>%s</div>', title, descrption)
                       );
                    }
                });
            }
        });
    }
};
weAreWorldQuant.notifications = {
    init: function () {
        if (!Modernizr.svg) { alertify.warning('This site requires a SVG capabile browser.'); }
        outdatedBrowser({
            color: '#000000',
            lowerThan: 'IE11',
            languagePath: '/scripts/libraries/outdatedbrowser/lang/en.html'
        });
    }
};
weAreWorldQuant.counter = {
    init: function () {
        if ($('.figure').length > 0) {
            $('.figure b').counterUp({
                delay: 10,
                time: 1000
            });
        }
    }
};
weAreWorldQuant.addIconToButton = {
    init: function () {
        $('.button').each(function (eq, me) {
            if (!$(this).hasClass('link')) {
                $(me).append(weAreWorldQuant.assets.svg.arrowRightSml);
            }
        });
    }
};
weAreWorldQuant.selectric = {
    init: function () {
        if ($('select').length) {
            $('select').selectric();
        }
    }
};
weAreWorldQuant.calloutMargin = {
    init: function () {
        document.fonts.ready.then(function () {
            //alert('All fonts in use by visible text have loaded.');
            //alert('DinCondenced loaded? ' + document.fonts.check('1em DinCondenced'));  // true
        });

        document.fonts.onloadingdone = function (fontFaceSetEvent) {
            weAreWorldQuant.calloutMargin.offset();
            //alert('onloadingdone we have ' + fontFaceSetEvent.fontfaces.length + ' font faces loaded');
        };

    },
    offset: function () {
        if ($('.calloutMargin').length > 0) {
            var shiftValue = $('.calloutMargin').find('.copy h2').height();
            console.info(shiftValue);
            $('.calloutMargin').find('.callout > div').css({
                'margin-top': shiftValue + 'px'
            });
        }
    }
};
weAreWorldQuant.equalheight = {
    init: function (selector) {
        //console.log('eh');
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = [],
            $el = 0,
            topPosition = 0,
            $elements = $(selector);
        for (var containerInLoop = 0, cLoops = $elements.length; containerInLoop < cLoops; containerInLoop++) {
            $elements.each(function () {
                $el = $(this);
                $($el).height('auto');
                topPostion = $el.position().top;
                if (currentRowStart !== topPostion) {
                    rowDivs.length = 0; // empty the array
                    currentRowStart = topPostion;
                    currentTallest = $el.height();
                    rowDivs.push($el);
                } else {
                    rowDivs.push($el);
                    currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
                }
                for (var currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
            });
        }
    }
};

weAreWorldQuant.initFoundationComponants = {
    init: function () {
        $('.mdl-modal').foundation();
        weAreWorldQuant.initFoundationComponants.fzMagellan === $('.horizontal.menu').foundation();
        weAreWorldQuant.initFoundationComponants.fzMagellan === $('.mdl-pageNav').foundation();
        weAreWorldQuant.initFoundationComponants.fzAccordion = $('.accordion').foundation();
        if (typeof weAreWorldQuant.initFoundationComponants.fzAccordion === 'undefined') {
        };
        Þ.window.stack('breakpoint', { maxWidth: 768 }, function () {
            if (typeof weAreWorldQuant.initFoundationComponants.fzAccordion === 'undefined') {
                weAreWorldQuant.initFoundationComponants.fzAccordion = $('.accordionSml').foundation();
            };
        }, function () {
            ///769 breakpoint
            if (typeof weAreWorldQuant.initFoundationComponants.fzAccordion !== 'undefined' && $('.accordionSml').length > 0) {
                $('.accordionSml').foundation('destroy');
                delete weAreWorldQuant.initFoundationComponants.fzAccordion;
            };
        });

        ///641 breakpoint
        Þ.window.stack('breakpoint', { minWidth: 641 }, function () {
        });

        Þ.window.stack('breakpoint', { minWidth: 769 }, function () {
            if (typeof weAreWorldQuant.initFoundationComponants.fzDropdown === 'undefined') {
                weAreWorldQuant.initFoundationComponants.fzDropdown = $('.dropdown').foundation();
            };
            if (typeof weAreWorldQuant.initFoundationComponants.fzOffcanvas !== 'undefined') {
                $('.off-canvas').foundation('destroy');
                delete weAreWorldQuant.initFoundationComponants.fzOffcanvas;
            };

        }, function () {
            //else if not min width 768 then do other stuff
            if (typeof weAreWorldQuant.initFoundationComponants.fzDropdown !== 'undefined' && $('.dropdown').length > 0) {
                $('.dropdown').foundation('destroy');
                delete weAreWorldQuant.initFoundationComponants.fzDropdown;
            };
            if (typeof weAreWorldQuant.initFoundationComponants.fzOffcanvas === 'undefined') {
                weAreWorldQuant.initFoundationComponants.fzOffcanvas = $('.off-canvas').foundation();
            };
        });
    },
    initonload: function () {
        if (Þ.browser.is.IE9()) {
            weAreWorldQuant.equalheight.init('.eh-large:visible');
        }
        if (typeof weAreWorldQuant.initFoundationComponants.fzOrbit === 'undefined') {
            weAreWorldQuant.initFoundationComponants.fzOrbit = $('.orbit').foundation();
            $('.orbit').css('visibility', 'visible').css('height', 'auto');
        };
        ///768 breakpoint
        Þ.window.stack('breakpoint', { maxWidth: 767 }, function () {
            //  console.debug('< 768');
            if (typeof weAreWorldQuant.initFoundationComponants.fzOrbit !== 'undefined' && $('.orbit').length > 0) {
                $('.orbit').foundation('destroy');
                //    console.debug('orbit destroy');
                delete weAreWorldQuant.initFoundationComponants.fzOrbit;

            };
        }, function () {
            ///769 breakpoint
            if (Þ.browser.is.IE9()) {
                weAreWorldQuant.equalheight.init('.eh-large:visible');
            }
        });
    }
};
weAreWorldQuant.canvasSlider = {
    init: function () {
        var thisWidth = $('.mdl-slide-m1 .hero img').width();
        var thisHeight = $('.mdl-slide-m1 .hero img').height();

        console.log('Image width: ' + thisWidth);
        console.log('Image height: ' + thisHeight);

        $('.mdl-slide-m1').canvasSlider({
            arrowHeight: 480,
            animDuration: 1000,
            bgColour: 'rgba(225,255,255,0)',
            imgSize: 'contain',
            loop: false,
            slideWidth: thisWidth,
            slideHeight: thisHeight,
            childSelector: 'section'
        });

        weAreWorldQuant.pagePile.movePPNav('#pp-nav', -8);
    }
};
weAreWorldQuant.pagePile = {
    init: function () {
        if ($('.mdl-slide-m1').length < 1) {
            return;

        }
        Þ.window.stack('breakpoint', { minWidth: 1025 }, function () {
            if ($('html.pagePiling').length === 0) {
                // console.log('running page pile');
                weAreWorldQuant.pagePile.pagePilingSettings();
            }
        });
        Þ.window.stack('breakpoint', { maxWidth: 1024 }, function () {
            if ($('html.pagePiling').length === 1) {
                //     console.log('destroy page pile');
                weAreWorldQuant.pagePile.pagePilingDestry();

            }
            if ($('h1 .button').length === 0) {
                $('.titleCopy').each(function (eq, me) {
                    var addBtn = $(me).find('.button').clone();
                    $(me).find('h1').append(addBtn);
                });
                $('.mdl-slide-m1 section:not(.hero)').each(function (eq, me) {
                    var h1Clone = $(me).find('h1').clone();
                    $(me).find('.summaryCopy > div:first-child').append(h1Clone);

                });
                var heroClone = $('.section.hero .button').clone();
                // $('.section.hero').find('h1').append(heroClone);

            }

        });
        //clone for lager screen
        $('.mdl-slide-m1 .titleCopy-wrap').find('.title-inner:last-child').clone().appendTo();
    },
    movePPNav: function ($selector, offSetNumber) {
        $($selector).css({
            'right': $(window).width() > 1300 ? ($(window).width() - $('.mdl-slide-m1 .section .row').width()) / 2 - ($('#pp-nav').width()) : offSetNumber + 'px',
            'top': $('.section:first-child img').offset().top + $('.section:first-child img').height() - $('#pp-nav').height() + Math.ceil(parseFloat($('#pp-nav li:last-child').css('margin-bottom'))) + 'px' // LDB
        });
    },
    pagePilingDestry: function () {
        var $pagePiling = $('.mdl-slide-m1');
        $pagePiling.attr({
            'style': ''
        });
        $('html').removeClass('pagePiling');
        $pagePiling.find('.section').removeClass('pp-section active pp-table pp-easing');
        $pagePiling.find('section').css({
            'z-index': '',
            'transform': '',
            '-webkit-transform': '',
            '-moz-transform': '',
            '-ms-transform': ''
        });
        $pagePiling.find('.pp-tableCell').remove();
        $('#pp-nav').remove();
    },
    pagePilingSettings: function ($pagePiling) {
        var sectionLength = $('.mdl-slide-m1 section').length;
        var sectionName = [];
        //create the names of each sectin for ##pp-nav
        $('.mdl-slide-m1').find('.section').each(function (eq, me) {
            sectionName.push($(me).data('section-name'));
        });
        //    console.debug(sectionName);

        //console.log('section length:  ' + sectionLength);
        $('.mdl-slide-m1').pagepiling({
            menu: null,
            direction: 'vertical',
            verticalCentered: false,
            anchors: sectionName,
            scrollingSpeed: 500,
            easing: 'swing',
            loopBottom: false,
            loopTop: false,
            css3: true,
            normalScrollElements: null,
            normalScrollElementTouchThreshold: 5,
            touchSensitivity: 5,
            keyboardScrolling: true,
            sectionSelector: '.section',
            animateAnchor: true,
            navigation: {
                'position': 'right'
            },
            //events
            onLeave: function (index, nextIndex, direction) {
                //  console.log('on leave');
                if (index === 0) {
                    //    console.log(index);
                }
                //after leaving section 2
                if (direction === 'down') {
                    //nextIndex.addClass('animate');
                }
                if (index === sectionLength - 1 && direction === 'down') {
                    //  console.log('happening one', index, 'section length', sectionLength - 1);
                }
                else if (index === 2 && direction === 'up') {
                    //  console.log('index is 2 direction up', index);
                }
            },
            afterRender: function () {
                //console.log('after render');
                if ($('.mdl-canvasSlider').length > 0) {
                    $('.mdl-canvasSlider').remove();
                }
                $('html').addClass('pagePiling');

                Þ.window.stack('resize', function () {
                    // moveCopyNotice('.copynotice');
                    weAreWorldQuant.pagePile.movePPNav('#pp-nav', 0);
                });
                var vh = 1200 - (1200 * 30 / 100);

                if ($('.mdl-slide-m1 .hero img').width() > 0) {
                    console.log('Image already loaded, initialising canvas slider');
                    weAreWorldQuant.canvasSlider.init();
                    console.log('Canvas slider initialised');
                }
                else {
                    console.log('Waiting for image to load...');
                    $('.mdl-slide-m1 .hero img').on('load', function () {
                        console.log('Image loaded, initialising canvas slider');
                        weAreWorldQuant.canvasSlider.init();
                        console.log('Canvas slider initialised');
                    });
                }

            },
            afterLoad: function (anchorLink, index) {
                // console.debug('after load');
                if (index > 1) {
                    $('#pp-nav').removeClass('custom');
                } else {
                    $('#pp-nav').addClass('custom');
                }

            },
            onLeave: function (index, nextIndex, direction) {
                //$('.csDotsWrap li:eq(' + index + ')').click();
                $('.csDotsWrap').find('li').eq(nextIndex - 1).click();
                //   console.log(index, nextIndex);
            },
        });
    }
};
weAreWorldQuant.sectionListing = {
    init: function () {
        Þ.window.stack('breakpoint', { minWidth: 641 }, function () {
            //check to see if you have already appended an SVG
            if ($('.mdl-pageNav a svg').length == 0) {
                $('.mdl-pageNav a').each(function (eq, me) {
                    $(this).append(weAreWorldQuant.assets.svg.arrowDownRound);
                });
            }
        });
        Þ.window.stack('breakpoint', { maxWidth: 640 }, function () {
            if ($('.mdl-section-listing').length > 0) {
                $('.smlAccordion-content').hide(0);
                $('.smlAccordion-title').each(function (eq, me) {
                    //console.log('artivel');
                    if ($(me).find('svg').length === 0) {
                        $(me).find('.h2').append(weAreWorldQuant.assets.svg.arrowDown);
                        $(me).find('p').clone().appendTo($(me).closest('.smlAccordion-content'));
                        //console.info($(me).find('p').clone().appendTo('.smal);
                        $(me).click(function () {
                            //this is cheeky, but since I checkf or the SVG if it is not there, then I can use this as a maker to run this code only once on resise, so that the slides dont stack
                            if ($(me).hasClass('is-active')) {
                                $(me).removeClass('is-active').next('.smlAccordion-content').removeClass('showme').addClass('hideme').slideUp(750);
                            } else {
                                $(me).addClass('is-active').next('.smlAccordion-content').removeClass('hideme').addClass('showme').slideDown(750);
                            }
                        });
                    }
                });
            }
            if ($('.tel svg').length === 0) {
                $('.tel').each(function (eq, me) {
                    $(me).append(weAreWorldQuant.assets.svg.phone);
                });
                $('.directions').each(function (eq, me) {
                    $(me).append(weAreWorldQuant.assets.svg.mapPin);
                });
                $('.email').each(function (eq, me) {
                    $(me).append(weAreWorldQuant.assets.svg.mail);
                });
            }
        }, function () {
            $('.smlAccordion-content').show(0);

        });
        $('.accordion .accordion-title').each(function (eq, me) {
            if ($(me).find('svg').length === 0) {
                $(me).append(weAreWorldQuant.assets.svg.arrowDown);
            }
        });
    }
};
weAreWorldQuant.revealFooter = {
    init: function () {
        if ($('body').hasClass('HomeLandingPage')) {
            var $footer = $('footer');
            $footer.addClass('footerHide');
            $('.revealFooter').click(function () {
                //  console.log('clicked revealFooter');

                if ($footer.hasClass('footerHide')) {
                    $footer.addClass('footerShow').removeClass('footerHide');
                } else if ($footer.hasClass('footerShow')) {
                    $footer.addClass('footerHide').removeClass('footerShow');
                }
            });
        }
    }
};
weAreWorldQuant.moveCopyNotice = {
    init: function () {
        weAreWorldQuant.moveCopyNotice.mover('.copynotice');
    },
    mover: function (selector) {
        if ($('.hero').length > 0) {
            var offsetTop = $('.hero .callout').offset().top + $(selector).width() / 2
            // console.debug($('.mdl-slide-m1 section>.row:first-child').offset().top + $(selector).width());
            //console.debug($('.hero .callout').offset().top);
            //console.debug($('.hero .callout').offset().top + $(selector).width());
            $(selector).css({
                'top': offsetTop + 'px'
            });
            Þ.window.stack('resize', function () {
                $(selector).css({
                    'right': $(window).width() > 1300 ? ($(window).width() - $('.mdl-slide-m1 .section .row').width()) / 2 - ($(selector).height() + 8) : 10 + 'px',
                });
            });
        }
    }
};
weAreWorldQuant.accordionSml = {
    init: function () {
        Þ.window.stack('breakpoint', { maxWidth: 640 }, function () {
            if ($('.accordionSml').length > 0) {
                $('.media-object').each(function (eq, me) {
                    if ($(me).find('.icon-arrow').length === 0) {
                        $(this).append(weAreWorldQuant.assets.svg.arrowDown);
                    }
                });
                $('.accordion-item').each(function (eq, me) {
                    $(me).find('.accordion-content').append('<div class="img-wrap clearfix"></div>');
                    if ($(me).find('.img-wrap img').length === 0) {
                        $(me).find('.orbit img').clone().appendTo($(me).find('.img-wrap'));
                    }
                });
            }
        });
    }
};
weAreWorldQuant.dlAccordion = {
    init: function () {
        if ($('.dlAccordion').length > 0) {
            $('.dlAccordion-content').hide(0);
            $('.dlAccordion-title').each(function (eq, me) {
                $(me).append(weAreWorldQuant.assets.svg.arrowDown);
                $(me).click(function () {
                    if ($(me).hasClass('is-active')) {
                        $(me).removeClass('is-active').next('.dlAccordion-content').removeClass('showme').addClass('hideme').slideUp(750);
                    } else {
                        $(me).addClass('is-active').next('.dlAccordion-content').removeClass('hideme').addClass('showme').slideDown(750);
                    }
                });
            });
        }
    }
};
weAreWorldQuant.vidYard = {
    isLoaded: false,
    loadvidYard: function (callback) {
        callback = typeof callback === 'function' ? callback : function () { };
        if (!weAreWorldQuant.vidYard.isLoaded) {
            $.ajax({
                url: '//play.vidyard.com/v0/api.js',
                dataType: "script",
                success: function () {
                    callback.call();
                    weAreWorldQuant.vidYard.isLoaded = true;
                }
            });
        }
        else {
            callback.call();
        }
    },
    vidYardMobile: {
        init: function (selector) {
            //this needs to be fixed CSS in place.
            $('.vidYard-instance').each(function (eq, me) {
                var ƒcontent = $(me).data('content');
                //  console.log('ƒcontent', ƒcontent);
                var id = 'vidYard-id-' + ƒcontent.uuid;
                var uuid = ƒcontent.uuid;
                var marginOffset = $(me).find('.video-text-overlay').height() + 32; //equates tot 2 rem;

                $(me).css({ 'margin-bottom': marginOffset });
                $(me).attr('id', id);
                $(me).append(getEmbedCode(uuid));

                var video = new Vidyard.player(uuid);
                weAreWorldQuant.vidYard.bxVideos.videos[uuid] = video;
                weAreWorldQuant.vidYard.bxVideos.vidYardInstance.bindEvents($(me), video, id);
                if ($(me).find('.vidYardplayer').length < 1) {
                }
            });
            //console.log('weAreWorldQuant.vidYard.bxVideos.bxInstances.length', weAreWorldQuant.vidYard.bxVideos.bxInstances.length);
            function getEmbedCode(uuid) {
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.id = 'vidyard_embed_code_' + uuid;
                script.src = 'https://play.vidyard.com/' + uuid + '.js?v=3.1&type=inline';
                return script;
            }
        }
    },
    init: function () {
        //add the custom vidYard iFrame to fitVids
        if ($('.vidYard-single').length > 0 || $('.VidYard').length > 0) {
            //     console.log('vidYard');
            $('.vidYard-player').fitVids({ customSelector: "iframe[src^='//play.vidyard.com']" });

        }
        //load single video for hero
        if ($('.vidYard-single').length > 0) {
            weAreWorldQuant.vidYard.loadvidYard(function () {
                weAreWorldQuant.vidYard.singleVideo.init('.vidYard-single');
            });

        }
        if ($('.VidYard').length > 0) {
            Þ.window.stack('breakpoint', { minWidth: 768 }, function () { // large viewport
                weAreWorldQuant.vidYard.loadvidYard(function () {
                    if (weAreWorldQuant.vidYard.bxVideos.bxInstances.length < 1) {
                        weAreWorldQuant.vidYard.bxVideos.init('.video-bx');
                        $('.vidYard-player').fitVids();
                    }
                });
            });
            //          console.log('vidyard found');
            Þ.window.stack('breakpoint', { maxWidth: 767 }, function () { // small viewport
                for (var i = 0, iLoops = weAreWorldQuant.vidYard.bxVideos.bxInstances.length; i < iLoops; i++) {
                    weAreWorldQuant.vidYard.bxVideos.bxInstances[i].destroySlider();
                }
                weAreWorldQuant.vidYard.loadvidYard(function () {
                    if (weAreWorldQuant.vidYard.bxVideos.bxInstances.length === 0) {

                        weAreWorldQuant.vidYard.vidYardMobile.init('.video-bx li');
                        $('.vidYard-player').fitVids();
                    }
                });
                weAreWorldQuant.vidYard.bxVideos.bxInstances = [];
            });
        }
    },
    bxVideos: {
        videos: []
        ,
        bxInstances: []
        ,
        init: function (selector) {
            this.vidYardInstance.init(selector);
            this.bxInstances.push(
                $(selector).bxSlider({
                    infiniteLoop: false, // turn off clones -> causes thumb eq detection to go out of order
                    pagerCustom: '#custom-pager',
                    onSliderLoad: function (slideIndex) {
                        weAreWorldQuant.vidYard.bxVideos.bind.thumbnailsControl(selector, slideIndex);
                    },
                    onSlideNext: function () {
                    }
                })
            );
        }
        ,
        bind: {
            thumbnails: function (selector, videos, slideIndex) {
                $('.orbit-slide > .columns').click(function (eq, me) {
                    //var $instance = $(sprintf('%s .vidYard-instance:eq(%s)', selector, index));  //unsed variable????

                    //create an arrary with each uuid
                    var uuidArray = [];
                    $(sprintf('%s .vidYard-instance', selector)).each(function () {
                        var uuid = $(this).attr('id').replace('vidYard-id-', '');
                        uuidArray.push(uuid);
                        if ($(this).hasClass('playing')) {
                            $(this).addClass('parent-playing');
                            weAreWorldQuant.vidYard.bxVideos.videos[uuid].pause();
                        } else if ($(this).hasClass('paused')) {
                            weAreWorldQuant.vidYard.bxVideos.videos[uuid].play();
                        }
                    });

                });
            },
            thumbnailsControl: function (selector, videos, index) {
                var uuidArray = [];
                var $instance = $(sprintf('%s .vidYard-instance', selector));
                if ($('.video-cluster-bx .orbit svg').length === 0) {
                    $('.orbit-previous').append(weAreWorldQuant.assets.svg.arrowLeft);
                    $('.orbit-next').append(weAreWorldQuant.assets.svg.arrowRight);
                    $('.orbit .video-thumb').each(function (eq, me) {
                        $(me).append(weAreWorldQuant.assets.svg.iconPlay);
                    });
                }
                if (uuidArray.length < $instance.length) {
                    $instance.each(function (eq, me) {
                        var ƒcontent = $(me).data('content');
                        var uuid = ƒcontent.uuid;
                        uuidArray.push(uuid);
                    });
                }
                function pausePlayes(thisUuid) {
                    weAreWorldQuant.vidYard.bxVideos.videos[thisUuid].pause();
                }
                $('.bx-pager-item:not(active)').click(function () {
                    // console.log('not active')
                    uuidArray.forEach(pausePlayes);
                    //Puase all the players
                    var dataUuid = $(this).data('uuid');
                    weAreWorldQuant.vidYard.bxVideos.videos[dataUuid].play();
                });
            }
        }
        ,
        vidYardInstance: {
            init: function (selector) {
                $(sprintf('%s .vidYard-instance', selector)).each(function (eq, me) {
                    if ($(me).find('.vidYardplayer').length < 1) {
                        var ƒcontent = $(me).data('content');
                        var id = 'vidYard-id-' + ƒcontent.uuid;
                        var uuid = ƒcontent.uuid;

                        function getEmbedCode() {
                            var script = document.createElement('script');
                            script.type = 'text/javascript';
                            script.id = 'vidyard_embed_code_' + uuid;
                            script.src = 'https://play.vidyard.com/' + uuid + '.js?v=3.1&type=inline';
                            return script;
                        }

                        $(me).attr('id', id);
                        $(me).append(getEmbedCode(uuid));

                        var video = new Vidyard.player(uuid);
                        weAreWorldQuant.vidYard.bxVideos.videos[uuid] = video;
                        weAreWorldQuant.vidYard.bxVideos.vidYardInstance.bindEvents($(me), video, id);

                    }
                });
                // $('.vidYard-player').fitVids();

            },
            bindEvents: function ($element, video, id) {
                //Vidyard Method Play
                video.on('play', function () {
                    $element.removeClass('paused');
                    $element.addClass('playing');
                    weAreWorldQuant.vidYard.bxVideos.textOverlays.hide(id);
                });

                //Vidyard Method Pause
                video.on('pause', function () {
                    $element.removeClass('playing');
                    $element.addClass('paused');
                    weAreWorldQuant.vidYard.bxVideos.textOverlays.show(id);
                });
                // video.pause();

                //Use the mouse enter and leave to show vidYard functionality on pause. 
                $element.mouseenter(function () {
                    if ($element.hasClass('paused')) {
                        weAreWorldQuant.vidYard.bxVideos.textOverlays.hide(id);
                    }
                });
                $element.mouseleave(function () {
                    if ($element.hasClass('paused')) {
                        weAreWorldQuant.vidYard.bxVideos.textOverlays.show(id);
                    }
                });
                //On complete remove the class playing and show the copy. 
                video.on('playerComplete', function () {
                    weAreWorldQuant.vidYard.bxVideos.textOverlays.show(id);
                });

            }

        }
        ,
        textOverlays: {
            show: function (id) {
                //console.log('show', id);
                $('#' + id + '.vidYard-instance').find('.video-text-overlay').fadeIn(500, 'swing');
                $('.video-cluster-bx .copy').fadeIn(800, 'swing');
            }
            ,
            hide: function (id) {
                //console.log('hide', id);
                setTimeout(function () {
                    $('#' + id + '.vidYard-instance').find('.video-text-overlay').fadeOut(500, 'swing');
                    $('.video-cluster-bx .copy').fadeOut(800, 'swing');
                }, 250);
            }
        }
    },
    singleVideo: {
        init: function (thisVideo) {
            var ƒcontent = $(thisVideo).data('content');
            var id = 'vidYard-id-' + ƒcontent.uuid;
            var uuid = ƒcontent.uuid;
            //          console.debug(ƒcontent);

            function getEmbedCode(uuid) {
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.id = 'vidyard_embed_code_' + ƒcontent.uuid;
                script.src = 'https://play.vidyard.com/' + ƒcontent.uuid + '.js?v=3.1&type=inline';
                return script;
            }

            //$(thisVideo).attr('id', id);
            $(thisVideo).find('.vidyard_player').fadeOut(0);
            $(thisVideo).closest('.hero').addClass('hasVideo');
            $(thisVideo).append(getEmbedCode(uuid));

            var video = new Vidyard.player(uuid);
            weAreWorldQuant.vidYard.singleVideo.bindEvents($(thisVideo), video, id);
            $('.vidYard-player').fitVids();


        },
        bindEvents: function ($element, video, id) {
            //Vidyard Method Play
            video.on('play', function () {
                $element.removeClass('paused');
                $element.addClass('playing');
                weAreWorldQuant.vidYard.singleVideo.textOverlays.hide(id);
            });

            //Vidyard Method Pause
            video.on('pause', function () {
                $element.removeClass('playing');
                $element.addClass('paused');
                weAreWorldQuant.vidYard.singleVideo.textOverlays.show(id);
            });

            //Use the mouse enter and leave to show vidYard functionality on pause. 
            $element.mouseenter(function () {
                if ($element.hasClass('paused')) {
                    weAreWorldQuant.vidYard.singleVideo.textOverlays.hide(id);
                }
            });
            $element.mouseleave(function () {
                if ($element.hasClass('paused')) {
                    weAreWorldQuant.vidYard.singleVideo.textOverlays.show(id);
                }
            });

            //On complete remove the class playing and show the copy. 
            video.on('playerComplete', function () {
                weAreWorldQuant.vidYard.singleVideo.textOverlays.show(id);
            });


        },
        textOverlays: {
            show: function (id) {
                $('.vidYard-single').closest('.hero').find('.copy').fadeIn(500, 'swing');
                $('.vidYard-single .vidyard_player').fadeIn(500, 'swing');

                $('.vidYard-single').closest('img').find('.copy').fadeIn(500, 'swing');
            },
            hide: function (id) {
                setTimeout(function () {
                    $('.vidYard-single').closest('.hero').find('.copy').fadeOut(500, 'swing');
                    $('.vidYard-single').closest('img').find('.copy').fadeOut(500, 'swing');
                }, 250);
            }
        }
    }
};
weAreWorldQuant.init = function () {
    //this.moveCopyNotice.init();
    this.initFoundationComponants.init();
    this.pagePile.init();
    this.dlAccordion.init();
    this.counter.init();
    this.googleMaps.init();
    this.sectionListing.init();
    this.vidYard.init();
    this.revealFooter.init();
    this.calloutMargin.init();
    this.accordionSml.init();
    this.addIconToButton.init();
};
$(document).ready(function () {
    weAreWorldQuant.init();


});
$(window).load(function () {
    if (Þ.browser.is.IE()) {
        console.log('you are using IE');
        //must wait for the font to load before this can work... chosen font and web font have different glyf size IE only as this doe snot support FontFaceSet.ready 
        weAreWorldQuant.calloutMargin.offset();
    }
    weAreWorldQuant.fallbacks.ie9onLoad();
    weAreWorldQuant.initFoundationComponants.initonload();
});
