var greenBrier = greenBrier || {};
// Þ = custom Thorn library
greenBrier.specification = {
    init: function () {
        Þ.window.stack('breakpoint', { minWidth: 769 }, function () {
            if (!$('.spec-col').length > 0) {
                $('.mdl-specification').prepend('<div class="col-8 spec-col spec-col-1"></div><div class="col-8 spec-col spec-col-2"></div>');
                $('.mdl-product-detail dt').each(function (eq, me) {
                    var title = $(me).html() || '',
                        descrption = $(me).next('dd').html() || '';
                    if (!eq % 2) {
                        $('.spec-col-1').append(
                               sprintf('<div class="h3">%s</div><div></div>%s</div>', title, descrption)
                           );
                    } else {;
                        $('.spec-col-2').append(
                           sprintf('<div class="h3">%s</div><div></div>%s</div>', title, descrption)
                       );
                    }
                });
            }
        });
    }
};
greenBrier.tableFixes = {
    init: function () {
        $('.news-article-wrap').find('table').each(function (eq, me) {
            if (!$(me).parent().hasClass('table-responsive') && !$(me).parent().hasClass('divOverflow')) {
                $(me).wrap('<div class="divOverflow"></div>');
            }
        });
    }
};
greenBrier.notifications = {
    init: function () {
        if (!Modernizr.svg) { alertify.warning('This site requires a SVG capabile browser.'); }
        outdatedBrowser({
            color: '#000000',
            lowerThan: 'IE11',
            languagePath: '/scripts/libraries/outdatedbrowser/lang/en.html'
        });
    }
};

greenBrier.customerCommit = {
    init: function () {
        Þ.window.stack('breakpoint', { minWidth: 769 }, function () {
            var mdlCustomer = $('.mdl-cust-commit');
            if (mdlCustomer.length > 0 && !$('.inColumns').length > 0) {
                mdlCustomer.find('.content').prepend('<div class="clear"></div><div class="inColumns clearfix"><div/>').prepend(mdlCustomer.find('.sumary'));
                $('.mdl-cust-commit dt').each(function (eq, me) {
                    var title = $(me).html();
                    var descrption = $(me).next('dd').html();
                    $('.inColumns').append(
                        sprintf('<div class="col-8"><div class="h3">%s</div>%s</div>', title, descrption)
                    );
                });
            }
        });
    }
};
greenBrier.appendSVG = {
    init: function () {
        $('.icon-h').prepend('<svg version="1.1" baseProfile="tiny" class="icon-committee" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" width="98.3px" height="46.6px" viewBox="0 0 98.3 46.6" xml:space="preserve"> <path fill="#696969" d="M98.3,41.3v4.5c0,0.4-0.3,0.7-0.7,0.7H0.7c-0.4,0-0.7-0.3-0.7-0.7v-4.5c0-0.4,0.3-0.7,0.7-0.7h27.7 c0.1-2.1,0.1-5.7,0.1-6.3c0-1,0.3-2,1-2.8c0.6-0.7,1.5-1.1,2.3-1.2v0c0,0,8.5-3.2,11-4.2c0.3-0.8,1-1.5,1.8-2.2l3.7,7.1 c0.5,0.6,1.3,0.6,1.8,0l3.7-7.1c0.8,0.6,1.5,1.4,1.8,2.2c2.5,1.1,11.1,4.2,11.1,4.2v0c0.8,0.1,1.6,0.4,2.2,1.2c1,1.1,1,6.4,1.1,9.1 h27.7C98,40.6,98.3,40.9,98.3,41.3z M58,11c0-0.9-0.2-1.7-0.7-2.4c0,0,0,0,0,0C57.3,3,53.7,0,49.2,0C44.7,0,41,3,41,8.6c0,0,0,0,0,0 c-0.4,0.7-0.7,1.5-0.7,2.4c0,1.2,0.6,2.4,1.5,3.4c1.2,4.9,4.5,8.5,7.3,8.5s6.1-3.6,7.3-8.5C57.4,13.4,58,12.2,58,11z"/> </svg>');
        $('.icon-pdf').prepend('<svg version="1.1" class="icon-pdf" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"width="51px" height="70.3px" viewBox="-454 245.7 51 70.3" style="enable-background:new -454 245.7 51 70.3;"xml:space="preserve"> <style type="text/css"> .st0{fill:#4DAC9D;} .st1{fill:#FFFFFF;} </style> <path  class="st0" d="M-404.9,245.7H-440c-0.4,0-0.8,0.3-1,0.6l-12.3,13.8c-0.2,0.2-0.7,0.6-0.7,0.9v33.3 c0,0.8,1.3,1.4,2,1.4h47.1c0.7,0,1.9-0.6,1.9-1.3v-47C-403,246.6-404.2,245.7-404.9,245.7z M-441.2,250.3v9.4h-8.2L-441.2,250.3z M-406,292.7h-45v-30h11.1c0.7,0,1.3-0.6,1.3-1.3v-12.7h32.6V292.7z"/> <g id="XMLID_1_"> <rect id="XMLID_3_" x="-438.9" y="283.3" class="st1" width="24.5" height="32.8"/> <g id="XMLID_5_"> <path id="XMLID_50_" class="st0" d="M-427.5,307c0.5,0.5,1.4,0.5,1.9,0l6.9-6.7c0.5-0.5,0.5-1.4,0-1.9s-1.4-0.5-1.9,0l-4.6,4.5 v-14.7c0-0.7-0.6-1.3-1.4-1.3c-0.7,0-1.4,0.6-1.4,1.3v14.7l-4.6-4.5c-0.5-0.5-1.4-0.5-1.9,0c-0.3,0.3-0.4,0.6-0.4,0.9 s0.1,0.7,0.4,1L-427.5,307z M-419.7,308.9h-13.8c-0.7,0-1.4,0.6-1.4,1.3c0,0.7,0.6,1.3,1.4,1.3h13.9c0.7,0,1.4-0.6,1.4-1.3 C-418.3,309.5-418.9,308.9-419.7,308.9z"/> </g> </g> <g> <path class="st0" d="M-435.1,269.9v0.9l0,0c0.2-0.4,0.5-0.7,0.8-0.9s0.7-0.3,1.1-0.3c0.9,0,1.5,0.3,2,1c0.4,0.7,0.6,1.8,0.6,3.4 c0,1.6-0.2,2.7-0.6,3.4c-0.4,0.7-1.1,1-2,1c-0.4,0-0.8-0.1-1-0.2c-0.3-0.2-0.5-0.4-0.8-0.7l0,0v3.5h-2.2v-11.2h2.1V269.9z M-434.8,276.2c0.1,0.5,0.5,0.7,0.9,0.7c0.5,0,0.8-0.2,0.9-0.7s0.2-1.2,0.2-2.1c0-0.9-0.1-1.6-0.2-2.1s-0.4-0.7-0.9-0.7 s-0.8,0.2-0.9,0.7s-0.2,1.2-0.2,2.1C-435,275-435,275.7-434.8,276.2z"/> <path class="st0" d="M-425.1,277.4c-0.2,0.4-0.5,0.7-0.8,0.8c-0.3,0.2-0.7,0.2-1.1,0.2c-0.9,0-1.5-0.3-2-1 c-0.4-0.7-0.6-1.8-0.6-3.4c0-1.6,0.2-2.7,0.6-3.4c0.4-0.7,1.1-1,2-1c0.4,0,0.8,0.1,1,0.3c0.3,0.2,0.5,0.4,0.8,0.8l0,0v-3.5h2.2 v11.2h-2.1L-425.1,277.4L-425.1,277.4z M-427.2,276.2c0.1,0.5,0.4,0.7,0.9,0.7s0.8-0.2,0.9-0.7c0.1-0.5,0.2-1.2,0.2-2.1 c0-0.9-0.1-1.6-0.2-2.1c-0.1-0.5-0.5-0.7-0.9-0.7c-0.5,0-0.8,0.2-0.9,0.7c-0.1,0.5-0.2,1.2-0.2,2.1 C-427.4,275-427.3,275.7-427.2,276.2z"/> <path class="st0" d="M-422.1,271.3v-1.5h1.1v-1c0-0.4,0.1-0.7,0.2-1c0.1-0.3,0.3-0.5,0.4-0.6c0.2-0.2,0.4-0.3,0.7-0.3 c0.3-0.1,0.6-0.1,0.9-0.1c0.4,0,0.8,0,1.2,0v1.5c-0.1,0-0.1,0-0.2,0h-0.2c-0.3,0-0.5,0.1-0.6,0.2c-0.1,0.1-0.2,0.3-0.2,0.5v0.7h1.3 v1.5h-1.3v7h-2.2v-7L-422.1,271.3L-422.1,271.3z"/> </g> </svg>');
        $('.icon-email').prepend('<svg version="1.1" baseProfile="tiny" class="icon-email" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" width="45px" height="45px" viewBox="0 0 45 45" xml:space="preserve"> <path fill="#009662" d="M33.5,13h-22c-1.1,0-2,0.9-2,2v15c0,1.1,0.9,2,2,2h22c1.1,0,2-0.9,2-2V15C35.4,13.9,34.6,13,33.5,13z M28.9,16.2l-6.4,4.8l-6.4-4.8H28.9z M12.7,28.8V18.5l8.6,6.4c0.3,0.3,0.8,0.4,1.2,0.4c0.4,0,0.8-0.1,1.2-0.4l8.6-6.4v10.3H12.7z M22.5,3.5c10.5,0,19,8.5,19,19s-8.5,19-19,19s-19-8.5-19-19S12,3.5,22.5,3.5 M22.5,0C10.1,0,0,10.1,0,22.5C0,34.9,10.1,45,22.5,45 C34.9,45,45,34.9,45,22.5C45,10.1,34.9,0,22.5,0L22.5,0z"/> </svg>');
        $('.icon-print').prepend('<svg version="1.1" baseProfile="tiny" class="icon-print" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" width="45px" height="45px" viewBox="0 0 45 45" xml:space="preserve"> <path fill="#009662" d="M22,3.5c10.5,0,19,8.5,19,19s-8.5,19-19,19c-10.5,0-19-8.5-19-19S11.5,3.5,22,3.5 M22,0 C9.5,0-0.5,10.1-0.5,22.5C-0.5,34.9,9.5,45,22,45c12.4,0,22.5-10.1,22.5-22.5C44.5,10.1,34.4,0,22,0L22,0z M35.6,15.3h-3.7V12 c0-0.9-0.7-1.7-1.7-1.7H13.7c-0.9,0-1.7,0.7-1.7,1.7v3.3H8.3c-0.9,0-1.7,0.7-1.7,1.7v11.1c0,0.9,0.7,1.7,1.7,1.7H12V33 c0,0.9,0.7,1.7,1.7,1.7h16.6c0.9,0,1.7-0.7,1.7-1.7v-3.2h3.7c0.9,0,1.7-0.7,1.7-1.7V16.9C37.3,16,36.6,15.3,35.6,15.3z M28.6,31.3 H15.3v-1.6h13.3V31.3z M28.6,19.9H15.3v-6.2h13.3V19.9z M34.4,19.5c-0.7,0-1.3-0.6-1.3-1.3c0-0.7,0.6-1.3,1.3-1.3 c0.7,0,1.3,0.6,1.3,1.3C35.6,18.9,35.1,19.5,34.4,19.5z"/> </svg>');
    }
};
//Media dom manipulation
greenBrier.media = {
    init: function () {
        $('.mdl-media.mod-press li').each(function (eq, me) {
            var mediaDate = '';
            mediaDate += $(me).find('.date').html();
            $(me).find('div:first-child').append(sprintf('<p class="date">%s</p>', mediaDate));
        });
    }
};
greenBrier.products = {
    initHover: function () {
        //add a class on over for absolute positioned spacer imgage
        $('.mdl-products a').hover(
            function () {
                $(this).prev('img').addClass("active");
            }, function () {
                $(this).prev('img').removeClass("active");
            }
        );
    },
    init: function () {
        $('.mdl-double-accordion .title').click(function () {
            if ($(window).width() <= 768) {
                if ($(this).next('.wrap').hasClass('open')) {
                    $(this).addClass('more').removeClass('less').next('.wrap').removeClass('open');
                } else {
                    $(this).removeClass('more').addClass('less').next('.wrap').addClass('open');
                }
            }
        });
        if ($(window).width() > 769) {
            //only gets called once
            accordionToUlDesktop();
        }
        //DOM MANIPULATION(bad-practice - this type of design should never go through) - mangle the Mobile first double DL accordion into a tab layout
        function accordionToUlDesktop() {
            var accordionToTab = $('.mdl-accordionTab > li');
            accordionToTab.each(function (eq, me) {
                //take all the titles and add them to a list for the tab functionality
                var summaryHtml = '';
                var titleHtml = '';
                //summaryHtml += $(me).find('.title').html() || '';
                titleHtml += $(me).find('.title').find('h2').html() || '';
                var $summaryClone = $(me).find('.product-sumary').clone().unwrap();
                $summaryClone.find('.col-8:first').prepend('<h2 class="h1">' + titleHtml + '</h2>');
                //console.info(summaryClone.html());
                summaryHtml += $summaryClone.html() || '';
                var dtHtml = '';
                $(me).find('.dtTitle').each(function (ptEq, ptMe) {
                    dtHtml += sprintf('<li><a href="#tab-%s-%s">%s</a></li>', eq + 1, ptEq + 1, $(ptMe).html());
                });
                var ddHtml = '';
                $(me).find('.ddCopy').each(function (pdEq, pdMe) {
                    if ($(me).parent().hasClass('mod-products')) {
                        ddHtml += sprintf('<div class="pager mdl-products clearfix" id="tab-%s-%s">%s</div>', eq + 1, pdEq + 1, $(pdMe).html());
                    } else {
                        ddHtml += sprintf('<div class="pager clearfix" id="tab-%s-%s">%s</div>', eq + 1, pdEq + 1, $(pdMe).html());
                    }
                });
                $('.section').append(sprintf('<div class="sub-section">%s<div class="clearfix"></div><ul class="tabs no-list">%s</ul>%s</div>', summaryHtml, dtHtml, ddHtml));
                //add a class on over for absolute positioned spaver imgage
                $('.mdl-products a').hover(
                    function () {
                        $(this).prev('img').addClass("active");
                    }, function () {
                        $(this).prev('img').removeClass("active");
                    }
                );
                //greenBrier.tabs.init();
                if ($('.mdl-directors-officers').length > 0) {
                    greenBrier.equalheight.init('.eheight');
                    $('.tabs').addClass('col-3');
                }
            });
        }
        greenBrier.products.initHover();
    }
};
greenBrier.counter = {
    init: function () {
        if ($('.figure').length > 0) {
            $('.figure i').counterUp({
                delay: 10,
                time: 1500
            });
        }
    }
};
greenBrier.print = {
    init: function () {
        $('.print').click(function () {
            window.print();
        });
    }
};
greenBrier.anchorTagOffset = {
    init: function () {
        // get the fixed header height - this is the height of the sticky header which we know will alayes be 50px + 15 for measure
        var offsetSize = 52;
        if ($(".tabs").length > 0) {
            offsetSize = 52 + 55 + $('ul.tabs').outerHeight(true);
        }
        // if page has anchor tag offset top and is at the top of the page so that it dose not interfere with  *adds the # fragment to the url* 
        //this should only happen when the user is at the top of the page ie an new .hash* page reference
        if (location.hash) {
            //use the set timeout
            setTimeout(function () {
                $('html, body').animate({
                    scrollTop: $(location.hash).offset().top - offsetSize
                }, 750);
            }, 1000);
        }
        $('a[href*="#"]').click(function (e) {
            e.preventDefault();
            // if current a is active then do nothing at all
            if (!$(this).hasClass('active')) {
                var hrefHash = this.hash;
                var targetContainer = $(hrefHash);
                // Check to see if the targetContainer exists and this click event did not orginate inside a tab container
                if ((targetContainer.length > 0) && (!$(this).closest(".tabs").length)) {
                    // animate to hash element
                        $('html, body').animate({
                            scrollTop: targetContainer.offset().top - offsetSize
                        }, 750, function () {
                            // adds the # fragment to the url
                            // add history to the url 'silently' so that browser does not jump when the url is updated on the same page.
                            if (history.pushState) {
                                history.pushState(null, null, hrefHash);
                            }
                            else {
                                location.hash = hrefHash;
                            }
                        });
                }
                else if (window.location.hash !== $(this).attr('href')) {
                    // changing the window.location to the same as it is currently makes the page scroll up
                    // check to make sure the hash needs to change
                    // we have to use the href attribute in order to cater for direct inpage links (eg #tab1-1) as well as linked pages inpage links (eg /different-page#tab1-1)
                    window.location = $(this).attr('href');
                }
            }
        });
    }
};
greenBrier.selectric = {
    init: function () {
        if ($('select').length) {
            $('select').selectric();
        }
    }
};
greenBrier.filters = {
    init: function () {
        //hides filters when no media in page
        //https://merchantcantos.atlassian.net/browse/GCSR-343
        if (!$('.mdl-media ul li').length) {
            //$('.mdl-media-filter').addClass('display-none');
            $('.mdl-pagination').addClass('display-none');
            $('.mdl-media-filter').after('<div class="content noresults"><div class="col-16"><h2>Sorry, but your search returned no results.</h2></div></div>');
        }
        $('select[class*=filter-]').each(function (eq, me) {
            $(me).change(function () {
                greenBrier.filters.on.change();
            });
        });
    },
    on: {
        change: function () {
            var keyvalues = {};
            $('select[class*=filter-]').each(function (eq, me) {
                keyvalues[$(me).attr('data-query')] = $(me).find('option:selected').val();
            });
            var query = '';
            for (var filter in keyvalues) {
                query += Þ.url.get.toggleChar(query) + sprintf('%s=%s', filter, keyvalues[filter]);
            }
            window.location = window.location.protocol + '//' + window.location.host + window.location.pathname + query;
        }
    }
};

//fitvids
greenBrier.fitVids = {
    init: function () {
        $('.fitvid').fitVids();
    }
};
greenBrier.searchExpand = {
    setup: {
        animateDuration: 0.5e3
    },
    init: function () {
        $('.quicklinks').data({
            'right': parseInt($('.quicklinks').css('right'), 10)
        });
        $('.searchWrap .search').hide(0);
        $('.search-btn').click(function () {
            $('.search-btn').data('isOpen') ? greenBrier.searchExpand.contract(true) : greenBrier.searchExpand.expand();
        }).data('isOpen', false);
        Þ.window.stack('scroll', function () {
            greenBrier.searchExpand.contract(false);
        });
        Þ.window.stack('resize', function () {
            greenBrier.searchExpand.contract(true);
        });
    }
    ,
    expand: function () {
        var $searchWrap = $('.searchWrap');
        $searchWrap.find('.search').css({ 'margin-left': $searchWrap.addClass('open').find('.search').outerWidth() }).show(0).clearQueue().stop().animate({
            marginLeft: 0
        }, greenBrier.searchExpand.setup.animateDuration, function () {
            $searchWrap.find('.search').addClass('open').removeClass('close');
            $searchWrap.find('input.searchInput').focus();
            $('.search-btn').data('isOpen', true).addClass('close');
            if (Þ.window.get.viewportSize().width > 768) {
                $('.quicklinks').animate({
                    right: '+=' + ($searchWrap.find('.search').outerWidth() - $('.quicklinks').data('right'))
                }, greenBrier.searchExpand.setup.animateDuration);
            }
        });
    }
    ,
    contract: function (isClick) {
        if (isClick || Þ.window.get.viewportSize().width > 768) {
            var $searchWrap = $('.searchWrap');
            $searchWrap.removeClass('open').find('.search').clearQueue().stop().animate({
                marginLeft: $searchWrap.find('.search').outerWidth()
            }, greenBrier.searchExpand.setup.animateDuration, function () {
                $searchWrap.find('.search').removeClass('open').addClass('close').hide();
                $searchWrap.find('input.searchInput').blur();
                $('.search-btn').data('isOpen', false).removeClass('close');
                $('.quicklinks').animate({
                    right: ($('.quicklinks').data('right'))
                }, greenBrier.searchExpand.setup.animateDuration);
            });
        }
    }
};
//sticky header
greenBrier.stickyHeader = {
    init: function () {
        if (!Þ.browser.is.IE8()) { //Let's make things a little easier since we are using CSS3 transitions and not implement this for IE8
        }
        Þ.window.stack('scroll', function () {
            if ($(this).scrollTop() > 1 && !$('.nav').hasClass('open')) {
                if (!$('#nav-open-btn').hasClass('close-btn')) {
                    $('.outerwrap').addClass('sticky');
                }
            } else {
                if (!$('#nav-open-btn').hasClass('close-btn')) {
                    $('.outerwrap').removeClass('sticky');
                }
            }
        });
        Þ.window.stack('waypoint', {
            triggerOn: $('.header'),
            triggerDepth: 0,
            triggerOnce: true
        }, function () {
            if (Þ.window.get.viewportSize().width > 769) {
                $('.outerwrap').addClass("sticky");
            }
        });
        Þ.window.stack('breakpoint', { minWidth: 769 }, function () {
            //**TO DO** need to make sure that the navigation is sicky when the loadeing from down the page
            if ($('#nav .logo').length > 0) {
                $('.logo').clone().prependTo('.nav .content');
            }
        });
    }
};
//ensuring no widows
greenBrier.clearWidow = {
    init: function () {
        $('.clearwidow').each(function () {
            var wordArray = $(this).text().split(' ');
            if (wordArray.length > 1) {
                wordArray[wordArray.length - 2] += '&nbsp;' + wordArray[wordArray.length - 1];
                wordArray.pop();
                $(this).html(wordArray.join(' '));
            }
        });
    }
};
//Adds a title attribute to the table heading which is later used by CSS for mobile table displays. https://css-tricks.com/responsive-data-tables/
greenBrier.responsiveTableTitleAtr = {
    init: function () {
        if ($('main table thead').length > 0) {
            $('main table').each(function (eq, me) {
                var tableHeadings = [];
                $(me).addClass('table');
                $(me).find('th').each(function () {
                    tableHeadings.push($(this).text());
                    //console.log($(this).attr("title"));
                });
                $(me).find('tbody tr').each(function (tEq, tMe) {
                    for (var i = 0, iLoops = tableHeadings.length; i < iLoops; i++) {
                        $(tMe).find('td:eq(' + i + ')').attr({
                            'title': tableHeadings[i]
                        });
                    }
                });
            });
        }
    }
};
greenBrier.navMenu = {
    init: function () {
        greenBrier.navMenu.addIcon();
        greenBrier.navMenu.subMenuOpen();
    },
    addIcon: function () {
        //append markers to the menu for mobile
        if ($('.nav').find('i').length === 0) {
            $('.nav ul').each(function (eq, me) {
                if ($(me).parent('li').length > 0) {
                    $(this).before('<i class="more">open/close</i>');
                }
            });
        }
    },
    subMenuOpen: function () {
        $('.nav .more').click(function () {
            var $subUL = $(this).next('ul');
            if ($subUL.hasClass('ii') || $subUL.hasClass('iii')) {
                if ($(this).hasClass('more')) {
                    $(this).addClass('less').removeClass('more');
                    $(this).next('ul').addClass('open');
                } else {
                    $(this).addClass('more').removeClass('less').next('ul').removeClass('open');
                }
            }
            else if ($subUL.hasClass('iii')) {
                console.log('close that pie');
                $('.level-iii-mob').html($subUL.clone()).addClass('show').slideDown(500,
                    function () {
                        $('html, body').animate({
                            scrollTop: 0
                        }, 300);
                    });
                $('.nav ul.i').addClass('hide').slideUp(500);
                $('<div class="iiiTitle"><i class="back"></i>Back</div>').prependTo('.level-iii-mob').click(function () {
                    $('.level-iii-mob').removeClass('show').slideUp(500, function () {
                        $(this).empty();
                    });
                    $('.nav ul.i').removeClass('hide').slideDown(500);
                });
            }
        });
    },
    moveIndicator: function () {
        //add a caret element to primary navigation and position dynamically 
        //as due to design parent cannot be set to relative. 
        $('.nav ul.i>li').each(function (eq, me) {
            $(me).mouseenter(function () {
                //on enter
                if ($(this).find('.ii').length > 0 && $(this).find('.indicator').length === 0) {
                    $('<i class="indicator"></i>').appendTo(this).css({
                        'left': $(this).position().left + ($(this).width() / 2) + 7
                    });
                }
            });
        });
    },
    desktopNavigation: function () {
        //let's move the header so that we can make the whole wrapper easily peristant for desktop
        this.apendWrapper();
    }
};
greenBrier.offcanvas = {
    //menu show/hide functionality
    init: function () {
        if ($(window).width() < 768 && $('.innerwrap').height() < $('.nav > div').height()) {
            var navheight = $('.nav > div').height();
            $('.innerwrap').css({
                minHeight: navheight
            });
        }
        /*! * *  Copyright (c) David Bushell | http://dbushell.com/ * */
        (function (window, document, undefined) {
            // helper functions
            var trim = function (str) {
                return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, '');
            };
            var hasClass = function (el, cn) {
                return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
            };
            var addClass = function (el, cn) {
                if (!hasClass(el, cn)) {
                    el.className = (el.className === '') ? cn : el.className + ' ' + cn;
                }
            };
            var removeClass = function (el, cn) {
                el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
            };
            var hasParent = function (el, id) {
                if (el) {
                    do {
                        if (el.id === id) {
                            return true;
                        }
                        if (el.nodeType === 9) {
                            break;
                        }
                    }
                    while ((el = el.parentNode));
                }
                return false;
            };
            // normalize vendor prefixes
            var doc = document.documentElement;
            var transform_prop = window.Modernizr.prefixed('transform'),
                transition_prop = window.Modernizr.prefixed('transition'),
                transition_end = (function () {
                    var props = {
                        'WebkitTransition': 'webkitTransitionEnd',
                        'MozTransition': 'transitionend',
                        'OTransition': 'oTransitionEnd otransitionend',
                        'msTransition': 'MSTransitionEnd',
                        'transition': 'transitionend'
                    };
                    return props.hasOwnProperty(transition_prop) ? props[transition_prop] : false;
                })();
            window.App = (function () {
                var _init = false,
                    app = {};
                var inner = document.getElementById('innerwrap'),
                    nav_open = false,
                    nav_class = 'js-nav';
                app.init = function () {
                    if (_init) {
                        return;
                    }
                    _init = true;
                    var closeNavEnd = function (e) {
                        if (e && e.target === inner) {
                            document.removeEventListener(transition_end, closeNavEnd, false);
                        }
                        nav_open = false;
                    };
                    app.closeNav = function () {
                        if (nav_open) {
                            $('.nav-btn').removeClass('close-btn').addClass('open-btn');
                            // close navigation after transition or immediately
                            var duration = (transition_end && transition_prop) ? parseFloat(window.getComputedStyle(inner, '')[transition_prop + 'Duration']) : 0;
                            if (duration > 0) {
                                document.addEventListener(transition_end, closeNavEnd, false);
                                //console.log('transition end');
                            } else {
                                closeNavEnd(null);
                            }
                        }
                        removeClass(doc, nav_class);
                    };
                    app.openNav = function () {
                        if (nav_open) {
                            return;
                        }
                        //modification scroll to top
                        var timer = setTimeout(function () {
                            $('html, body').animate({
                                scrollTop: 0
                            }, 0.5e3);
                        }, 0.4e3);
                        $('.nav-btn').removeClass('open-btn').addClass('close-btn');
                        addClass(doc, nav_class);
                        nav_open = true;
                    };
                    app.toggleNav = function (e) {
                        if (nav_open && hasClass(doc, nav_class)) {
                            app.closeNav();
                            $('.outerwrap').addClass("sticky");
                        } else {
                            app.openNav();
                            $('.outerwrap').removeClass("sticky");
                        }
                        if (e) {
                            e.preventDefault();
                        }
                    };
                    // open nav with main "nav" button
                    document.getElementById('nav-open-btn').addEventListener('click', app.toggleNav, false);
                    // close nav with main "close" button - this button has been removed
                    //document.getElementById('nav-close-btn').addEventListener('click', app.toggleNav, false);
                    // close nav by touching the partial off-screen content
                    document.addEventListener('click', function (e) {
                        if (nav_open && !hasParent(e.target, 'nav')) {
                            e.preventDefault();
                            app.closeNav();
                            try {
                                if ($(e.srcElement.offsetParent).hasClass('logo')) {
                                    //with navigation open, logo should link to homepage
                                    //https://merchantcantos.atlassian.net/browse/GCSR-344
                                    document.location.href = '/';
                                }
                            }
                            catch (e) { };
                        }
                    },
                        true);
                    addClass(doc, 'js-ready');
                };
                return app;
            })();
            if (window.addEventListener) {
                window.addEventListener('DOMContentLoaded', window.App.init, false);
            }
            Þ.window.stack('resize', function () {
                App.closeNav();
            });
        })(window, window.document, Þ, $);
    }
};
//populate sub navigation under banner
greenBrier.cloneSubNav = {
    init: function () {
        if ($('.subNav').length > 0 && $('.nav li.active ul.ii').length > 0) {
            $('.nav li.active ul.ii').clone().appendTo('.subNav').addClass('col-16').wrap('<div class="content"></div>');
        }
        if ($('.subNav ul.iii').length > 0) {
            $('.subNav ul.iii').parent().addClass('more').append('<svg version="1.1" baseProfile="tiny" class="icon-subNav-more" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"x="0px" y="0px" width="10px" height="17px" viewBox="0 0 10 17" xml:space="preserve"> <path fill="#FFFFFF" d="M1.4,17c-0.4,0-0.7-0.1-1-0.4c-0.6-0.5-0.6-1.4,0-2l6.1-6.1L0.4,2.4c-0.6-0.6-0.5-1.4,0-2 c0.6-0.5,1.5-0.5,2,0l7.1,7.1c0.5,0.5,0.5,1.4,0,2l-7.1,7.1C2.2,16.9,1.8,17,1.4,17z"/> </svg>');
        }
        Þ.window.stack('breakpoint', { minWidth: 769 }, function () {
            //destroy slider and empty pager
            if ($('.subNav .bx-wrapper').length > 0) {
                greenBrier.bxInstance.destroySlider();
                $('.subNav .pager').remove();
            }
        });
    }
};
greenBrier.bxslider = {
    init: function () {
        if ($('.companyNews').length) {
            greenBrier.bxslider.companySlider = $('.companyNews').bxSlider({
                mode: 'horizontal',
                pager: true,
                adaptiveHeight: false,
                auto: true,
                pause: 5000,
                controls: false
            });
            Þ.window.stack('resize', function () {
                greenBrier.bxslider.companySlider.reloadSlider();
            });
        }
        if ($('.bx-slider').length) {
            greenBrier.bxslider.instances = [];
            $('.mdl-slider').each(function (eq, me) {
                greenBrier.bxslider.instances.push(
                    $(me).find('.bx-slider').bxSlider({
                        mode: 'horizontal',
                        pager: true,
                        adaptiveHeight: false,
                        pagerCustom: $(me).find('.bg-pager'),
                        onSliderLoad: function () {
                            if (Þ.window.get.viewportSize().width > 768) {
                                $('.mdl-slider').each(function (eqH2, meH2) {
                                    $(meH2).find('.copy .h2').css({
                                        marginTop: $(meH2).find('.title').height() + 10
                                    });
                                });
                            }
                            //initiate the iscroll for the navigation
                            if (Þ.window.get.viewportSize().width < 768) {
                                var scrollerInstance = $(me).find('.iscroller');
                                //calculate the width for each
                                var totalWidth = 0;
                                scrollerInstance.find('.scroller a').each(function () {
                                    totalWidth += $(this).outerWidth(true) + 7;
                                });
                                scrollerInstance.find('.scroller').css({
                                    width: totalWidth + 'px'
                                });
                            }
                        },
                        onSlideAfter: function () {
                        }
                    })
                );
                Þ.window.stack('resize', function () {
                    for (var i = 0, iLoops = greenBrier.bxslider.instances.length; i < iLoops; i++) {
                        greenBrier.bxslider.instances[i].reloadSlider();
                    }
                });
            });
            //swap out background style for the data-mobileBG one
            $('.bxslider img').each(function (eq, me) {
                $(me).removeAttr('style').css({
                    'background-image': sprintf('url(\'%s\')', $(window).width() < 768 ? $(me).data('mobileBg') : $(me).data('dtBg'))
                });
            });
        }
    }
};
greenBrier.searchboxText = {
    searchActive: function () {
        var $searchBox = $('.searchInput');
        $searchBox.addClass('idleField')
            .focus(function () {
                $(this).removeClass('idleField').addClass('focusField');
                $('.search button').addClass('focus');
                if (this.value === this.defaultValue) {
                    this.value = '';
                    if ($(this).parent().hasClass('search-media')) {
                        $(this).parent().addClass('active');
                    }
                }
                if (this.value !== this.defaultValue) {
                    this.select();
                }
            })
            .blur(function () {
                if ($(this).parent().hasClass('search-media')) {
                    $(this).parent().removeClass('active');
                }
                $(this).removeClass('focusField').addClass('idleField');
                $('.search button').removeClass('focus');
                if ($.trim(this.value) === '') {
                    this.value = (this.defaultValue ? this.defaultValue : '');
                }
            });
        if ($('.search-inpage .searchInput').length > 0) {
            Þ.window.stack('breakpoint', { maxWidth: 768 }, function () {
                $('.search-inpage .searchInput').css({
                    width: $(window).width() - ($(window).width() / 100 * 4) - 60
                });
            });
        }
        if ($('.filter-tool').length > 0) {
            Þ.window.stack('breakpoint', { maxWidth: 768 }, function () {
                $('#show-search').click(function () {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        $('.search-media').removeClass('show').slideUp(500);
                    } else {
                        $(this).addClass('active');
                        $('.search-media').addClass('show').slideDown(500);
                        if ($('#show-filter').hasClass('active')) {
                            $('#show-filter').removeClass('active');
                            $('.filter-tool').removeClass('show').slideUp(500);
                        }
                    }
                });
            });
            $('#show-filter').click(function () {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $('.filter-tool').removeClass('show').slideUp(500);
                } else {
                    $(this).addClass('active');
                    $('.filter-tool').addClass('show').slideDown(500);
                    if ($('#show-search').hasClass('active')) {
                        $('#show-search').removeClass('active');
                        $('.search-media').removeClass('show').slideUp(500);
                    }
                }
            });
        }
    }
};
greenBrier.equalheight = {
    init: function (selector) {
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = [],
            $el = 0,
            topPosition = 0,
            $elements = $(selector);
        for (var containerInLoop = 0, cLoops = $elements.length; containerInLoop < cLoops; containerInLoop++) {
            $elements.each(function () {
                $el = $(this);
                $($el).height('auto');
                var topPostion = $el.position().top;
                if (currentRowStart !== topPostion) {
                    rowDivs.length = 0; // empty the array
                    currentRowStart = topPostion;
                    currentTallest = $el.height();
                    rowDivs.push($el);
                } else {
                    rowDivs.push($el);
                    currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
                }
                for (var currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
            });
        }
    }
};
greenBrier.matchHeight = {
    init: function () {
        //Get height of mdl-products
        Þ.window.stack('breakpoint', { minWidth: 768 }, function () {
            $('.mdl-company-news, .mdl-work-with-us').height($('.mdl-products').height() / 2);
        });
        Þ.window.stack('breakpoint', { maxWidth: 769 }, function () {
            $('.mdl-company-news, .mdl-work-with-us').removeAttr('style');
        });
    }
};
greenBrier.medAccordion = {
    init: function () {
        Þ.window.stack('resize', function () {
            greenBrier.medAccordion.bind.click();
        });
    },
    bind: {
        click: function () {
            if ($(window).width() < 770) {
                var event = (Þ.browser.os() === 'iPhone' && Þ.browser.name() === 'Safari') ? 'touchend' : 'click';
                $('.med-accordion .icon').each(function (eq, me) {
                    if ($(me).data('hasRun') !== true) {
                        $(me).on(
                        event, function () {
                            $(this).parent().toggleClass('less').next('dd').toggleClass('open');
                        }).data('hasRun', true);
                    }
                });
            }
        }
    }
};
greenBrier.tabs = {
    init: function () {
        //http://www.jacklmoore.com/notes/jquery-tabs/
        $('ul.tabs').each(function (eq, me) {
            if ($(me).find('li').length > 0) {
                // For each set of tabs, we want to keep track of
                // which tab is active and it's associated content
                var $active, $content, $links = $(this).find('a');
                // If the location.hash matches one of the links, use that as the active tab.
                // If no match is found, use the first link as the initial active tab.
                $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                $active.addClass('active');
                $content = $($active[0].hash);
                // Hide the remaining content
                $links.not($active).each(function () {
                    $(this.hash).hide();
                });
                // Bind the click event handler
                $(this).on('click', 'a', function (e) {
                    if (!$(this).hasClass('active')) {
                        // Make the old tab inactive.
                        $active.removeClass('active');
                        $content.fadeOut(250);
                        // Update the variables with the new link and content
                        $active = $(this);
                        $content = $(this.hash);
                        // Make the tab active.
                        $active.addClass('active');
                        $content.fadeIn(250);
                        //custom for investors page
                        if ($('.mdl-directors-officers').length > 0) {
                            greenBrier.equalheight.init('.eheight');
                        }
                    }
                    // Prevent the anchor's default click action
                    e.preventDefault();
                    // if (Þ.storage.caniuse.sessionStorage()) {
                    //     sessionStorage['lahttps://featureshoot.submittable.com/submitstVistedTab'] = $(this).attr('href');
                    // }
                });
            }
        });
        if (Þ.storage.caniuse.sessionStorage()) {
            if (typeof sessionStorage['lastVistedTab'] !== 'undefined') {
                // alert(sessionStorage['lastVistedTab']);
                //$('ul.tabs a').removeClass('active');
                //$('a[href="' + sessionStorage['lastVistedTab'] + '"]').addClass('active');
            }
        }
    }
};

greenBrier.googleMaps = {
    debug: false,
    markerInstances: [],
    overlayInstances: {},
    preInit: function () {
        this.imageLayer = function (bounds, image, map, visible) {
            // Initialize all properties.
            this.bounds_ = bounds;
            this.image_ = image;
            this.map_ = map;
            this.visible_ = visible;
            // Define a property to hold the image's div. We'll
            // actually create this div upon receipt of the onAdd()
            // method so we'll leave it null for now.
            this.div_ = null;
            // Explicitly call setMap on this overlay.
            this.setMap(map);
        };
        this.imageLayer.prototype = new google.maps.OverlayView();
        this.imageLayer.prototype.draw = function () {
            // We use the south-west and north-east
            // coordinates of the overlay to peg it to the correct position and size.
            // To do this, we need to retrieve the projection from the overlay.
            var overlayProjection = this.getProjection();
            // Retrieve the south-west and north-east coordinates of this overlay
            // in LatLngs and convert them to pixel coordinates.
            // We'll use these coordinates to resize the div.
            var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
            var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());
            // Resize the image's div to fit the indicated dimensions.
            var div = this.div_;
            div.style.left = sw.x + 'px';
            div.style.top = ne.y + 'px';
            div.style.width = (ne.x - sw.x) + 'px';
            div.style.height = (sw.y - ne.y) + 'px';
        };
        this.imageLayer.prototype.onAdd = function () {
            var div = document.createElement('div');
            div.style.borderStyle = 'none';
            div.style.borderWidth = '0px';
            div.style.position = 'absolute';
            div.style.visibility = this.visible_ ? 'visible' : 'hidden';
            // Create the img element and attach it to the div.
            var img = document.createElement('img');
            img.src = this.image_;
            img.style.width = '100%';
            img.style.height = '100%';
            img.style.position = 'absolute';
            div.appendChild(img);
            this.div_ = div;
            // Add the element to the "overlayLayer" pane.
            var panes = this.getPanes();
            panes.overlayLayer.appendChild(div);
        };
        this.imageLayer.prototype.show = function () {
            if (this.div_) {
                this.div_.style.visibility = 'visible';
            }
        };
        this.imageLayer.prototype.onRemove = function () {
            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        };
        this.imageLayer.prototype.hide = function () {
            if (this.div_) {
                // The visibility property must be a string enclosed in quotes.
                this.div_.style.visibility = 'hidden';
            }
        };
        this.imageLayer.prototype.toggle = function () {
            if (this.div_) {
                if (this.div_.style.visibility === 'hidden') {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };
        this.imageLayer.prototype.setVisible = function (boolean) {
            if (this.div_) {
                if (boolean) {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };
        if (this.debug) {
            console.info('maps preInit done');
        }
    },
    init: function () {
        var svg = {
            path: {
                pinActive: 'M10.5,0C4.7,0,0,4.7,0,10.5c0,2.2,0.7,4.3,1.8,5.9l8.7,14.7l8.7-14.7c1.2-1.7,1.8-3.7,1.8-5.9 C21,4.7,16.3,0,10.5,0z M15.4,8.9l-7.7,7.7l-3.6-4c-0.5-0.5-0.5-1.3,0.1-1.7C4.7,10.4,5.5,10.5,6,11 l1.9,2.1l5.9-5.9c0.5-0.5,1.3-0.5,1.8,0S15.9,8.5,15.4,8.9z',
                pin: 'M10.5,0C4.7,0,0,4.7,0,10.5c0,2.2,0.7,4.3,1.8,5.9l8.7,14.7l8.7-14.7c1.2-1.7,1.8-3.7,1.8-5.9 C21,4.7,16.3,0,10.5,0z M10.5,16.2c-3.2,0-5.7-2.6-5.7-5.7s2.6-5.7,5.7-5.7s5.7,2.6,5.7,5.7S13.7,16.2,10.5,16.2z'
            }
        };
        //a   M6.7,13.5l-8.2,8.2l-3.6-4C-5.6,17.2-5.6,16.4-5,16c0.5-0.5,1.3-0.4,1.8,0.1l1.9,2.1l6.4-6.4,c0.5-0.5,1.3-0.5,1.8,0S7.2,13.1,6.7,13.5z
        //p   M1.5,22c-3.2,0-5.7-2.6-5.7-5.7s2.6-5.7,5.7-5.7s5.7,2.6,5.7,5.7S4.7,22,1.5,22z
        if ($('#map').length > 0) {
            //Þ.window.stack('resize', function () {
            //        alert('not on one line');
            //    if ($('.map-division-controls').height() > 51) {
            //        $('.map-division-controls').removeClass('col-8').addClass('col-16', 'mod-division-controls');
            //        $('.mdl-mapContact .h1').parent().removeClass('col-8').addClass('col-16');
            //        //console.info($('.mdl-mapContact .h1').parent());
            //    }// else {
            //    //    $('.map-division-controls').removeClass('col-16', 'mod-division-controls').addClass('col-8');
            //    //}
            //});
            // extend default Map functionality
            this.preInit();
            this.mcMap = new google.maps.Map($('#map').get(0), { //.get(0) returns the native DOM object
                center: {
                    lat: 40,
                    lng: 0
                },
                zoom: 2,
                scrollwheel: false,
                disableDefaultUI: false,
                styles:
                    [
                    { 'featureType': 'administrative.province', 'elementType': 'all', 'stylers': [{ 'visibility': 'off' }] },
                    { 'featureType': 'landscape', 'elementType': 'all', 'stylers': [{ 'saturation': -100 }, { 'lightness': 65 }, { 'visibility': 'on' }] },
                    { 'featureType': 'landscape', 'elementType': 'geometry.fill', 'stylers': [{ 'color': '#e3e3e3' }] },
                    { 'featureType': 'poi', 'elementType': 'all', 'stylers': [{ 'saturation': -100 }, { 'lightness': 51 }, { 'visibility': 'simplified' }] },
                    { 'featureType': 'road.highway', 'elementType': 'all', 'stylers': [{ 'saturation': -100 }, { 'visibility': 'simplified' }] },
                    { 'featureType': 'road.arterial', 'elementType': 'all', 'stylers': [{ 'saturation': -100 }, { 'lightness': 30 }, { 'visibility': 'on' }] },
                    { 'featureType': 'road.local', 'elementType': 'all', 'stylers': [{ 'saturation': -100 }, { 'lightness': 40 }, { 'visibility': 'on' }] },
                    { 'featureType': 'transit', 'elementType': 'all', 'stylers': [{ 'saturation': -100 }, { 'visibility': 'simplified' }] },
                    { 'featureType': 'water', 'elementType': 'geometry', 'stylers': [{ 'hue': '#ffff00' }, { 'lightness': -25 }, { 'saturation': -97 }] },
                    { 'featureType': 'water', 'elementType': 'geometry.fill', 'stylers': [{ 'color': '#7c7c7c' }] },
                    { 'featureType': 'water', 'elementType': 'labels', 'stylers': [{ 'visibility': 'on' }, { 'lightness': -25 }, { 'saturation': -100 }] }
                    ]
            });
            this.setMarkers();
            this.setOverlays();
            // Events for toggling pins/overlays
            $('.map-overlay-controls li.marker').each(function (eq, me) {
                if ($(this).hasClass('active')) {
                    $(this).find('.icon-pin path').attr('d', svg.path.pinActive);
                }
                $(me).click(function () {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        $(this).find('.icon-pin path').attr('d', svg.path.pin);
                        greenBrier.googleMaps.toggleMarkers($(this).data('type'), false);
                    } else {
                        $(this).addClass('active');
                        $(this).find('.icon-pin path').attr('d', svg.path.pinActive);
                        greenBrier.googleMaps.toggleMarkers($(this).data('type'), true);
                    }
                });
            });
            $('.map-overlay-controls li.overlay').each(function (eq, me) {
                $(me).click(function () {
                    if ($(this).hasClass('active')) {
                        greenBrier.googleMaps.toggleOverlays('hideAll');
                        $('.map-overlay-controls li.overlay').removeClass('active');
                    } else {
                        greenBrier.googleMaps.toggleOverlays('showAll');
                        $('.map-overlay-controls li.overlay').removeClass('active');
                        $(this).addClass('active');
                    }
                });
            });
            $('.map-division-controls li').each(function (eq, me) {
                $(me).click(function () {
                    if ($(this).hasClass('active')) {
                        greenBrier.googleMaps.setRegion('home');
                        $('.map-division-controls li').removeClass('active hidepsudeo');
                        greenBrier.googleMaps.toggleOverlays('showAll');
                    } else {
                        greenBrier.googleMaps.setRegion($(this).data('region'));
                        $('.map-division-controls li').removeClass('active hidepsudeo');
                        $(this).addClass('active').next('li').addClass('hidepsudeo');
                        greenBrier.googleMaps.toggleOverlays('hideAll');
                    }
                });
            });
        } // end if
    }, // end init
    setMarkers: function () {
        // Check we have the data
        if (typeof greenBrier.googleMaps.data === 'undefined' || greenBrier.googleMaps.data.length === 0) return;
        if (typeof greenBrier.googleMaps.data.mcPins === 'undefined' || greenBrier.googleMaps.data.mcPins.length === 0) return;
        function makeMarkup(pin) {
            var heading;
            if (pin.url > '') {
                heading = $('<a/>').attr('href', pin.url).attr('class', 'read-more').attr('title', pin.title).html(pin.title);
            } else {
                heading = $('<p/>').attr('class', 'popup-header').html(pin.title);
            }
            var location = $('<p/>').attr('class', location).html(pin.location);
            //var description = $('<p/>').html(pin.description);
            var address1 = $('<p/>').html(pin.address1);
            var address2 = $('<p/>').html(pin.address2);
            var contact1 = $('<p/>').html(sprintf('<a href="tel:%s">%s</a>', pin.contact1, pin.contact1));
            var contact2 = $('<p/>').html(sprintf('<a href="fax:%s">%s</a>', pin.contact2, pin.contact2));
            var webContact = $('<p/>').html(sprintf('<a href="%s" title="visit %s" target="_blank">%s</a>', pin.url, pin.sitename, pin.sitename));
            return heading.prop('outerHTML') + location.prop('outerHTML') + address1.prop('outerHTML') + address2.prop('outerHTML') + contact1.prop('outerHTML') + contact2.prop('outerHTML') + webContact.prop('outerHTML');
        }
        for (var pinTypeCtr in greenBrier.googleMaps.data.mcPins) {
            var pinType = greenBrier.googleMaps.data.mcPins[pinTypeCtr];
            for (var pinCtr in pinType.pins) {
                var pin = pinType.pins[pinCtr];
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pin.lat, pin.lng),
                    //anchor:  new google.maps.Marker({pinType.offsetX, pinType.offsetY}),
                    map: greenBrier.googleMaps.mcMap,
                    icon: pinType.icon,
                    title: pin.title,
                    zIndex: pinType.zindex,
                    type: pinType.type,
                    visible: pinType.visible
                });
                var markup = makeMarkup(pin);
                //open infowindow on mouseover
                google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                    //console.info(infowindow);
                    return function () {
                        if (typeof greenBrier.googleMaps.infowindow !== 'undefined') {
                            greenBrier.googleMaps.infowindow.close();
                        }
                        infowindow.setContent(content);
                        infowindow.open(greenBrier.googleMaps.mcMap, marker);
                        greenBrier.googleMaps.infowindow = infowindow;
                        google.maps.event.addListener(infowindow, 'domready', function () {
                            $('.gm-style-iw').parent().addClass('iw-styled');
                        });
                    };
                })(marker, markup, new google.maps.InfoWindow({
                    maxWidth: 600
                })));
                // closure for context scope.
                greenBrier.googleMaps.markerInstances.push(marker);
                if (greenBrier.googleMaps.debug) {
                    console.log('Marker data: ', pinType);
                }
            } // end for each (pin)
        } // end for each (pinType)
    },
    toggleMarkers: function (type, visible) {
        type = type || 'showAll';
        if (greenBrier.googleMaps.debug) {
            console.info('marker type: ', type);
        }
        for (var marker in greenBrier.googleMaps.markerInstances) {
            if (greenBrier.googleMaps.debug) {
                console.log('marker in loop: ', greenBrier.googleMaps.markerInstances[marker].title, marker);
                console.log('marker setVisible: ', greenBrier.googleMaps.markerInstances[marker].type === type);
            }
            if (type === 'showAll' || greenBrier.googleMaps.markerInstances[marker].type === type) {
                greenBrier.googleMaps.markerInstances[marker].setVisible(visible);
            }
        } // end for each
        if (typeof greenBrier.googleMaps.infowindow !== 'undefined') {
            greenBrier.googleMaps.infowindow.close();
        }
    },
    setOverlays: function () {
        // Check we have the data
        if (typeof greenBrier.googleMaps.data === 'undefined' || greenBrier.googleMaps.data.length === 0) {
            return;
        }
        if (typeof greenBrier.googleMaps.data.overlays === 'undefined' || greenBrier.googleMaps.data.overlays.length === 0) {
            return;
        }
        for (var overlay in greenBrier.googleMaps.data.overlays) {
            var overlaydata = greenBrier.googleMaps.data.overlays[overlay];
            this.overlayInstances[overlaydata.name] = new this.imageLayer(
                // overlay bounds
                new google.maps.LatLngBounds(
                    new google.maps.LatLng(overlaydata.bottom, overlaydata.left), // bottom left corner
                    new google.maps.LatLng(overlaydata.top, overlaydata.right) // top right corner
                ),
                // image src
                overlaydata.image,
                // map instance
                greenBrier.googleMaps.mcMap,
                greenBrier.googleMaps.data.overlaysVisible
            );
            greenBrier.googleMaps.overlayInstances[overlaydata.name].name = overlaydata.name || 'overlay_' + new Date().valueOf();
            greenBrier.googleMaps.overlayInstances[overlaydata.name].setMap(greenBrier.googleMaps.mcMap);
            if (greenBrier.googleMaps.debug) {
                console.log('Overlay data: ', overlaydata);
            }
        } // end for each
    },
    toggleOverlays: function (name) {
        name = name || 'showAll';
        if (greenBrier.googleMaps.debug) {
            console.info('overlay name: ', name);
        }
        for (var overlay in greenBrier.googleMaps.overlayInstances) {
            if (greenBrier.googleMaps.debug) {
                console.log('overlay in loop: ', overlay);
                console.log('overlay show/hide: ', greenBrier.googleMaps.overlayInstances[overlay].name === name);
            }
            greenBrier.googleMaps.overlayInstances[overlay].setVisible(name === 'showAll' ? true : false);
        }
        if (typeof greenBrier.googleMaps.infowindow !== 'undefined') {
            greenBrier.googleMaps.infowindow.close();
        }
    },
    setRegion: function (region) {
        region = region || 'home';
        // Find the region
        var regionToShow = {
            lat: 0,
            lng: 0,
            zoom: 2
        };
        for (var i = 0, len = greenBrier.googleMaps.data.regions.length; i < len; i++) {
            if (greenBrier.googleMaps.data.regions[i].title === region) {
                regionToShow = greenBrier.googleMaps.data.regions[i];
                break;
            }
        }
        greenBrier.googleMaps.mcMap.setZoom(regionToShow.zoom);
        greenBrier.googleMaps.mcMap.setCenter(new google.maps.LatLng(regionToShow.lat, regionToShow.lng));
        if (typeof greenBrier.googleMaps.infowindow !== 'undefined') {
            greenBrier.googleMaps.infowindow.close();
        }
    },
    mobileRedirect: function () {
        $('.map-overlay-controls li.active ').removeClass('active').addClass('mod');
    }
}; // end maps


greenBrier.init = function () {
    this.specification.init();
    this.appendSVG.init();
    this.tableFixes.init();
    this.media.init();
    this.navMenu.init();
    this.cloneSubNav.init();
    this.products.init();
    //run dom manipulation first
    this.offcanvas.init();
    this.theOwlCarousel.init();// ommited did not write myself
    this.stickyHeader.init();
    this.notifications.init();
    this.print.init();
    this.clearWidow.init();
    this.searchExpand.init();
    this.responsiveTableTitleAtr.init();
    this.bxslider.init();
    this.searchboxText.searchActive();
    this.matchHeight.init();
    this.medAccordion.init();
    this.selectric.init();
    this.inputEffects.init();
    this.filters.init();
    this.counter.init();
    this.googleMaps.init();//witten with by developer Francois DuPlessis - extending Gmaps for custom overlays
    this.tabs.init();
    this.anchorTagOffset.init();
    this.customerCommit.init();
    this.equalheight.init('.equalheight');
    //Þ.stats.GA.attach.contactLinks();//witten by lead developer Francois DuPlessis
};
$(document).ready(function () {
    greenBrier.init();
});
$(window).load(function () {

});