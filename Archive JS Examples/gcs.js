//'use strict';
var gcs = gcs || {};
$(document).foundation();


gcs.googleMaps = {
    init: function() {
        if ($('#gcs-map').length > 0) {
            var myMarker = {
                lat: -26.153000,
                lng: 28.090119
            };

            map = new google.maps.Map(document.getElementById('gcs-map'), {
                zoom: 15,
                center: myMarker,
                scrollwheel: false,
                disableDefaultUI: true,
                zoomControl: true,
                styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
            });
            var marker = new google.maps.Marker({
                position: myMarker,
                animation: google.maps.Animation.DROP,
                icon: 'http://localhost:3000/images/mapMarkerIcon.png',
                map: map
            });
            // google.maps.event.addDomListener(window, 'load', initialize);
        }
    }
}

gcs.productImage = {
    init: function() {
        var products = json.products;
        gcs.productImage.writeProducts(products);
        gcs.productImage.swap(products);


    },

    writeProducts: function(products) {
        var productWrapper = document.getElementById('productWrapper');


        // console.log(products.length);
        for (var i = 0; i < products.length; i++) {
            // console.info(productWrapper);
            //create each section for each product
            var newSection = document.createElement('section');
            newSection.className = 'row align-top align-center';
            productWrapper.appendChild(newSection);

            //create the colums for each product

            var columnFirst = document.createElement('div');
            var columnSecond = document.createElement('div');
            columnFirst.className = 'large-4 medium-6 small-12 columns prod-img';
            columnSecond.className = 'large-4 medium-6 small-12 columns prod-desc';
            newSection.appendChild(columnFirst);
            newSection.appendChild(columnSecond);
            // console.info(products[i].image[0].initial);

            //add images
            var imgLoaded = document.createElement('img');
            imgLoaded.setAttribute('src', products[i].image[i].initial);
            imgLoaded.setAttribute('data-magnify-src', products[i].image[i].large);
            imgLoaded.setAttribute('alt', products[i].altTxt);
            imgLoaded.className = 'zoom';
            columnFirst.appendChild(imgLoaded);

            //add description
            var productTitle = document.createElement('h2');
            var productDesc = document.createElement('p');
            productTitle.innerHTML = products[i].productName;
            columnSecond.appendChild(productTitle);
            productDesc.innerHTML = products[i].productDesc;
            columnSecond.appendChild(productDesc);


            //add the thumbs for each element
            var thumbWrap = document.createElement('div');
            thumbWrap.className = 'thumbnails';
            columnSecond.appendChild(thumbWrap);

            for (var j = 0; j < products[i].image.length; j++) {
                var thumbLoaded = document.createElement('img');
                thumbLoaded.setAttribute('src', products[i].image[j].thumb);
                thumbLoaded.setAttribute('data-magnify-src', products[i].image[j].large);
                thumbLoaded.setAttribute('data-magnify-initial', products[i].image[j].initial);
                thumbLoaded.setAttribute('alt', products[i].altTxt + ' ' + products[i].image[j].aspect + ' thumbnail');
                thumbWrap.appendChild(thumbLoaded);
            }

            $('.zoom').magnify();

        }
    },

    swap: function(products) {


        var $thumbnail = $('.thumbnails img');

        $('.mdl-product-list .row').each(function(eq, me) {
            $(me).find('.thumbnails img').on({
                'click': function() {
                    var $image = $(me).find('.prod-img img'),
                        imageInitial = $(this).data('magnifyInitial'),
                        magnifyInitial = $(this).data('magnifySrc');
                    
                    $image.attr({
                        'src': imageInitial,
                        'data-magnify-src': magnifyInitial
                    });

                    $('.zoom').magnify();
                }

            });
        });

    }


}
gcs.init = function() {
    this.productImage.init();
    // this.foundation.init();
}

$(window).load(function() {
    gcs.googleMaps.init(); //only works on load... need to wait for google api to work
});
$(document).ready(function() {
    gcs.init();
});
