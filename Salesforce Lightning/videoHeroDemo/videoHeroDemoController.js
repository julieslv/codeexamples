({
  doInit: function(component, event, helper) {
		// console.log(component.get("v.pageReference").state.testAttribute);
    var heroVideo = component.find("heroVideo");

    if (window.outerWidth >= 767) {
			component.set("v.videoAutoPlay", true);
    } else {
			component.set("v.videoAutoPlay", false);
		}
	},
	scriptsLoaded : function(component, event, helper) {
			// init controller
			var controller = new ScrollMagic.Controller({
				globalSceneOptions: {
					triggerHook: "onEnter",
					duration: "200%"
				}
			});
		/* 	var parallaxParent = component.find("parallaxParent").getElement();
			var parallaxChild = component.find("parallaxChild").getElement();
			// build scenes
			new ScrollMagic.Scene({
				triggerElement: parallaxParent
			}).setTween(parallaxChild, {y: "80%", ease: Linear.easeNone})
				// .addIndicators()
				.addTo(controller); */

				var parallaxVidParent = component.find("heroVideoParent").getElement();
				var parallaxVidChild = component.find("heroVideoChild").getElement();
				// build scenes2
			new ScrollMagic.Scene({
				triggerElement: parallaxVidParent
			}).setTween(parallaxVidChild, {
				y: "50%",
				ease: Linear.easeNone
			})
				.addIndicators()
				.addTo(controller);
		}
})