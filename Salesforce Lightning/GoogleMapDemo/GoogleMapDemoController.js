({
	init: function (cmp, event, helper) {
		cmp.set('v.mapMarkers', [
		{
			location: {
				Street: 'The Landmark @ One Market',
				City: 'San Francisco',
				PostalCode: '94105',
				State: 'CA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'Worldwide Corporate Headquarters',
			description: 'Sales: 1800-NO-SOFTWARE'
		},
		{
			location: {
				Street: '950 East Paces Ferry Road NE',
				City: 'Atlanta',
				PostalCode: '94105',
				State: 'GA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Atlanta',
		},
		{
			location: {
				Street: '929 108th Ave NE',
				City: 'Bellevue',
				PostalCode: '98004',
				State: 'WA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Bellevue',
		},
		{
			location: {
				Street: '500 Boylston Street 19th Floor',
				City: 'Boston',
				PostalCode: '02116',
				State: 'MA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Boston',
		},
		{
			location: {
				Street: '111 West Illinois Street',
				City: 'Chicago',
				PostalCode: '60654',
				State: 'IL',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Chicago',
		},
		{
			location: {
				Street: '2550 Wasser Terrace',
				City: 'Herndon',
				PostalCode: '20171',
				State: 'VA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Herndon',
		},
		{
			location: {
				Street: '2035 NE Cornelius Pass Road',
				City: 'Hillsboro',
				PostalCode: '97124',
				State: 'OR',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Hillsboro',
		},
		{
			location: {
				Street: '111 Monument Circle',
				City: 'Indianapolis',
				PostalCode: '46204',
				State: 'IN',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Indy',
		},
		{
			location: {
				Street: '300 Spectrum Center Drive',
				City: 'Irvine',
				PostalCode: '92618',
				State: 'CA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Irvine',
		},
		{
			location: {
				Street: '361 Centennial Parkway',
				City: 'Louisville',
				PostalCode: '80027',
				State: 'CO',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Louisville',
		},
		{
			location: {
				Street: '685 Third Ave',
				City: 'New York',
				PostalCode: '10017',
				State: 'NY',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc New York',
		},
		{
			location: {
				Street: '1442 2nd Street',
				City: 'Santa Monica',
				PostalCode: '90401',
				State: 'CA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Santa Monica',
		},
		{
			location: {
				Street: '12825 East Mirabeau Parkway',
				City: 'Spokane',
				PostalCode: '99216',
				State: 'WA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Spokane',
		},
		{
			location: {
				Street: '4301 West Boy Scout Blvd',
				City: 'Tampa',
				PostalCode: '33607',
				State: 'WA',
				Country: 'USA',
			},

			icon: 'utility:salesforce1',
			title: 'salesforce.com inc Tampa',
		},
		
		]);

		cmp.set('v.center', {
			location: {
				City: 'Denver'
			}
		});

		cmp.set('v.styles',
			[
			{
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#e9e9e9"
				},
				{
					"lightness": 17
				}
				]
			},
			{
				"featureType": "landscape",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#f5f5f5"
				},
				{
					"lightness": 20
				}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 17
				}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry.stroke",
				"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 29
				},
				{
					"weight": 0.2
				}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 18
				}
				]
			},
			{
				"featureType": "road.local",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#ffffff"
				},
				{
					"lightness": 16
				}
				]
			},
			{
				"featureType": "poi",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#f5f5f5"
				},
				{
					"lightness": 21
				}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#dedede"
				},
				{
					"lightness": 21
				}
				]
			},
			{
				"elementType": "labels.text.stroke",
				"stylers": [
				{
					"visibility": "on"
				},
				{
					"color": "#ffffff"
				},
				{
					"lightness": 16
				}
				]
			},
			{
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"saturation": 36
				},
				{
					"color": "#333333"
				},
				{
					"lightness": 40
				}
				]
			},
			{
				"elementType": "labels.icon",
				"stylers": [
				{
					"visibility": "off"
				}
				]
			},
			{
				"featureType": "transit",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#f2f2f2"
				},
				{
					"lightness": 19
				}
				]
			},
			{
				"featureType": "administrative",
				"elementType": "geometry.fill",
				"stylers": [
				{
					"color": "#fefefe"
				},
				{
					"lightness": 20
				}
				]
			},
			{
				"featureType": "administrative",
				"elementType": "geometry.stroke",
				"stylers": [
				{
					"color": "#fefefe"
				},
				{
					"lightness": 17
				},
				{
					"weight": 1.2
				}
				]
			}
			]
			);
		cmp.set('v.zoomLevel', 4);
		cmp.set('v.markersTitle', 'Salesforce locations in United States');
		cmp.set('v.showFooter', true);
	}
})