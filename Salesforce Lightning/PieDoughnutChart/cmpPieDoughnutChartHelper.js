({
  drawGraph: function (component) {
    if (!component.get("v.scriptsReady")) return;

    var data = component.get("v.values");
    var labels = component.get("v.labels");
    var target = component.get("v.target");
    var tint = component.get("v.tint");

    if (data == null) {
      // TODO:  show a message in the canvas element
      console.log('no suitable data for graphing');
      return;
    }

    var colours = component.get("v.colours");

    // console.log('component.get("v.colours"): ', colours);
    if (data.length > colours.length) {
      for (var i = colours.length - 1; i < data.length; i++) colours.push(this.getColourTint(tint));
      component.set("v.colours", colours);
    }
    var total = 0;
    for (var i in data) {
      total = total + data[i];
    }

    // data.splice(0, 0, target - total);

    // If we have a target and it isn't reached we insert
    // the remaining as the first entry in the table
    var hasTargetColumn = (target > total || total == 0);
    if (hasTargetColumn) {
      data.splice(0, 0, target - total);
    } else {
      // console.log('skip the first colour');
      // when reaching the target we want to skip the first colour
      colours.splice(0, 1);
    }


    var options = {
      responsive: true,
      defaultFontFamily: '"Maven Pro"',
      maintainAspectRatio: false,
      cutoutPercentage: 75,
      legend: {
        display: false,
      }
      // circumference: 2*Math.PI
    };

    if (!$A.util.isEmpty(component.get("v.centerText"))) {
      options["elements"] = {
        center: {
          text: component.get("v.centerText"),
          textSuper: component.get("v.superText"),
          textSub: component.get("v.subText"),
          fontStyle: 'normal',
          fontWeight: '600',
          fontWeightSub: '400',
          fontSize: '45px',
          fontSizeSub: '16px',
          fontFace: '"Maven Pro"',
        }
      };
    }

    if (!component.get("v.chartObj")) {
      var chartCanvas = component.find("chart").getElement();
      var chart = new Chart(chartCanvas, {
        type: 'doughnut',
        data: {
          datasets: [{
            data: data,
            borderWidth: 0,
            backgroundColor: colours,
            borderColor: colours,
          }],
          labels: labels,
        },
        options: options
      });
      component.set("v.chartObj", chart);
    } else {
      var chart = component.get("v.chartObj");
      var coData = chart.data;
      coData.datasets.pop();
      coData.datasets.push({
        data: data,
        backgroundColor: colours,
        borderColor: colours
      });
      chart.options = options;
      chart.update();
    }

    var t1 = component.get("v.centerText");
    var t2 = component.get("v.superText");
    if (!t1 && !t2) {
      $A.get('e.force:refreshView').fire();
    }
  },

  getColourTint: function (tint) {
    var p = 1,
      temp,
      random = Math.random(),
      colourTint = '#';
    while (p < tint.length) {
      temp = parseInt(tint.slice(p, p += 2), 16)
      temp += Math.floor((255 - temp) * random);
      colourTint += temp.toString(16).padStart(2, '0');
    }
    return colourTint;
  },

  getRandomColour: function (component) {
    var letters = '0123456789ABCDEF'.split('');
    var colour = '#';
    for (var i = 0; i < 6; i++) {
      colour += letters[Math.floor(Math.random() * 16)];
    }
    return colour;
  }
})
