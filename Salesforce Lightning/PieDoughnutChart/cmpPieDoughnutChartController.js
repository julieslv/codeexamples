({
    // Documentation for the library here: http://www.chartjs.org/docs/latest/charts/doughnut.html
    scriptsLoaded: function (component, event, helper) {
        component.set("v.scriptsReady", true);
        var colours = component.get("v.colours");
        helper.drawGraph(component);
    },

    drawGraph: function (component, event, helper) {
        var p = event.getParam('arguments');
        if (p) {
            component.set("v.values", p.values);
            component.set("v.target", p.target);
            component.set("v.centerText", p.centerText);
            component.set("v.superText", p.superText);
            component.set("v.subText", p.subText);
            helper.drawGraph(component);
        }
    },
})