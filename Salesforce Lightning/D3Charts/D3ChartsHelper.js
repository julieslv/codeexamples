({
	renderChartPie : function( component, data ) {
		var svg = d3.select("svg"),
		width = +svg.attr("width"),
		height = +svg.attr("height"),
		radius = Math.min(width, height) / 2,
		g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

		var color = d3.scaleOrdinal(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

		var pie = d3.pie()
		.sort(null)
		.value(function(d) { return d.candidates; });

		var path = d3.arc()
		.outerRadius(radius - 10)
		.innerRadius(0);

		var label = d3.arc()
		.outerRadius(radius - 80)
		.innerRadius(radius - 80);

		var arc = g.selectAll(".arc")
		.data(pie(data))
		.enter().append("g")
		.attr("class", "arc");

		arc.append("path")
		.attr("d", path)
		.attr("fill", function(d) { return color(d.data.age); });

		arc.append("text")
		.attr("transform", function(d) { return "translate(" + label.centroid(d) + ")"; })
		.attr("dy", "0.35em")
		.text(function(d) { return d.data.age; });
	},
	renderChart : function( component, data ) {
		console.info('Helper data: ', data);
        // define the contraints
        var width = 420,
        barHeight = 20;

        // define the x axis scale
        var x = d3.scale.linear()
        .range([0, width]);
        //set the width of the SVG element
        var chart = d3.select(".chart")
        .attr("width", width);

        console.info(width);
        console.log(x);
        console.info(chart);
            //var y = d3.scale.linear().domain([15,90]).range([250,0]); 

            //add to the scale function returned by function u(n){return o(n)}
            x.domain([0, d3.max(data, function(d) {
            	return d.value;
            })]);

        // set chart height
        chart.attr("height", barHeight * data.length);

        // bind data to SVG elements
        var bar = chart.selectAll("g")
        .data(data)
        .enter().append("g")
        .attr("transform", function(d, i) {
        	return "translate(0," + i * barHeight + ")";
        });

        bar.append("rect")
        .attr("width", function(d) {
        	return x(d.value);
        })
        .attr("height", barHeight - 1);

        bar.append("text")
        .attr("x", function(d) {
        	return x(d.value) - 3;
        })
        .attr("y", barHeight / 2)
        .attr("dy", ".35em")
        .text(function(d) {
        	return d.value;
        });
      }
    })