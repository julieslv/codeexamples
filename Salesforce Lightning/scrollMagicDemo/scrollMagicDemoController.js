({
	scriptsLoaded : function(component, event, helper) {
		// init controller
		var controller = new ScrollMagic.Controller();
		var lightningPin = component.find("sticky").getElement();

		console.info('lightningPin', lightningPin);

		// create a scene
		new ScrollMagic.Scene({
				triggerElement: lightningPin,
        duration: 1000,    // the scene should last for a scroll distance of 100px
        offset: 50    // start this scene after scrolling for 50px
      })
		.addIndicators() // requires plugin
    .setPin(lightningPin) // pins the element for the the scene's duration
    .addTo(controller); // assign the scene to the controller
  }







})