({
	scrollToTop: function(component,event, helper){
		var target = component.find('target');
		var element = target.getElement();
		/*The Element.getBoundingClientRect() method returns the size of an
		element and its position relative to the viewport.*/
		var rect = element.getBoundingClientRect();
		scrollTo({top: rect.top, behavior: 'smooth'});
		return rect;
	},
	scrollToBottom: function(component,event, helper){
		var target = component.find('target');
		var element = target.getElement();
		var rect = element.getBoundingClientRect();
		scrollTo({top: rect.bottom, behavior: 'smooth'});
	},
	scrollToElement: function(component,event, helper){
		var target = component.find('target');
		var destination = component.find('destination').getElement().offsetTop;
		var element = target.getElement();
		var newWindow = element.getBoundingClientRect();
		scrollTo({top: destination, behavior: 'smooth'});
	}
})